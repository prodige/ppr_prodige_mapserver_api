<?php

namespace Mapserver\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


$corrupted_data_color = "NotAColor";
$corrupted_data_stringofseveralvalues = "NotAStringOfSeveralValues";
$corrupted_data_numeric = "NotANumericValue";
$corrupted_data_constant = "NotAConstant";
$corrupted_data_boolean = "NotABoolean";


$good_file = "good_map"; // map file name
$good_layer = "good_layer"; // layer name
$good_class = "0"; // class index
$good_label = "0"; // label index
$good_style = "0"; // style index

$bad_file = "bad_map";
$bad_layer = "bad_layer";
$bad_class = "-1";
$bad_label = "-1";
$bad_style = "-1";

$array_keys_color = array(
    "map imagecolor",
    "layer offsite",
    "reference color",
    "reference outlinecolor",
    "label color",
    "label shadowcolor",
    "style color",
    "style outlinecolor"
);

$array_keys_stringofseveralvalues = array(
    "map extent",
    "reference extent",
    "reference size",
    "label shadowsize",
    "label offset",
    "style outlinewidth"
);

$array_keys_numeric = array(
    "map maxscaledenom",
    "map minscaledenom",
    "layer maxscaledenom",
    "layer minscaledenom",
    "layer symbolscaledenom",
    "layer tolerance",
    "layer toleranceunits",
    "label maxscaledenom",
    "label minscaledenom",
    "label size",
    "label angle",
    "label position",
    "label geomtransform",
    "style maxsize",
    "style minsize",
    "style size",
    "style symbol",
    "style width"
);

$array_keys_constant = array(
    //"map imagetype", is it a string or a constant ?
    "map units",
    "layer raster",
    "layer connectiontype",
    "layer status",
    "layer sizeunits",
    "class status"
);

$array_keys_boolean = array(
    "label force"
);


$data_successmap = array(
    "name" => "cecinestpasunecarte", // string
    "fontset" => "etc/fonts.txt", // string = fontset file path
    "symbolset" => "etc/symboles/symbols.sym", // string = symbolset file path
    "imagecolor" => "#00FF00", // color in HEX that we'll later convert into RGB then into colorObj
    "imagetype" => "PNG", // string I guess, but can't find in the doc
    "extent" => "-10.20 -0.5 10 1.6", // 4 decimal values (minx, miny, maxx, maxy) in a string we'll later convert into rectObj
    "units" => "METERS", // string that should match a map units type
    "projection" => "proj=utm,ellps=GRS80,zone=15,north,no_defs", // projection string (= some "PROJ.4" strings separated with "," to then create a projectionObj
    "maxscaledenom" => "500", // (for the web) decimal
    "minscaledenom" => "499.99", // (for the web) decimal
    "metadata" => json_encode(array("key1" => "value1", "key7" => "value7", "key2" => "value2", "value5")), // metadata for the web, that we'll later convert into a hashTableObj
    "template" => "template.map", // (for the web) string = template file path
);


$data_successlayer = array(
    "type" => "RASTER", // string matching sthing
    "connectiontype" => "TILED_SHAPEFILE",
    "connection" => "thisisaCONNECTIONstring", // string
    "data" => "thisisaDATAstring", // string
    "tileindex" => "thisisaTILEINDEXstring", //string
    "tileitem" => "thisisaTILEITEMstring", // string
    "projection" => "proj=utm,ellps=GRS80,zone=15,north,no_defs", // projection string (= some "PROJ.4" strings separated with "," to then create a projectionObj
    "status" => "MS_DEFAULT",
    "sizeunits" => "FEET",
    "maxscaledenom" => "22.03",
    "minscaledenom" => "03.22",
    "labelitem" => "thisisaLABELITEMstring", // string
    "offsite" => "#04B404", // string of color in HEX
    "processing" => "thisisaPROCESSINGstring",
    "symbolscaledenom" => "222.22", // decimal
    "tolerance" => "0.01", // decimal
    "toleranceunits" => "10", // decimal
    "classitem" => "thisisaCLASSITEMstring", // string
    "metadata" => json_encode(array("key1" => "value1", "key7" => "value7", "key2" => "value2", "value5")), // metadata for the web, that we'll later convert into a hashTableObj
);


$data_successreference = array(
    "color" => "#04B404", // string of color in HEX",
    "extent" => "0.2 0.5 20 50",
    "image" => "thisisaIMAGEfilepath",
    "outlinecolor" => "#04B404", // string of color in HEX
    "size" => "420 1024",
);

$data_successclass = array(
    "name" => "success_class", // string
    "expression" => "[POPULATION] > 50000 AND '[LANGUAGE]' eq 'FRENCH'", // string
    "status" => "MS_DEFAULT",
);

$data_successlabel = array(
    "color" => "#04B404", // string of color in HEX we'll need to convert to colorObj
    "shadowcolor" => "#04B404", // string of color in HEX we'll need to convert to colorObj
    "shadowsize" => "56 65", // integer
    "font" => "comic san MS", // string
    "maxscaledenom" => "22.03", // decimal
    "minscaledenom" => "03.22", // decimal
    "size" => "666", // integer
    "angle" => "22.222", // decimal
    "force" => TRUE, // boolean
    "offset" => "18 25", // two integers separated by a space
    "position" => "29", // integer
    "geomtransfom" => "1000",
);

$data_successstyle = array(
    "color" => "#04B404", // string of color in HEX",
    "geomtransform" => "thisisaGEOMTRANSFORMstring",
    "maxsize" => "17.89",
    "minsize" => "14.92",
    "outlinecolor" => "#04B404", // string of color in HEX
    "outlinewidth" => "0.2 1.5",
    "size" => "15.15",
    "symbol" => "99",
    "width" => "18.25"
);


class ApiControllerTest extends WebTestCase
{


// PUTTERS


    public function testErrorPutMap()
    {
        global $good_file; // map file name
        global $data_successmap;
        $data_errormap = $this->insertCorruptedDataIntoArray($data_successmap, "map");

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file, $data_errormap);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }


    public function testSuccessPutMap()
    {
        global $good_file; // map file name
        global $data_successmap;

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file, $data_successmap);
        $result = $client->getResponse();

//        dump('/api/map/' . $good_file);
//        dump($data_successmap);
//        dump($result);
//        exit();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success.');
    }


    public function testErrorPutReference()
    {
        global $good_file;
        global $data_successreference;
        $data_errorreference = $this->insertCorruptedDataIntoArray($data_successreference, "reference");

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file . '/reference', $data_errorreference);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    /*public function testSuccessPutReference() {
     $file = global $file;

     $client = static::createClient();
     $client->request('POST', '/api/map/'.$file.'/reference', $data_successreference);
     $result = $client->getResponse();

     $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
     $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success.');
     }

     */


    public function testErrorPutLayer()
    {
        global $good_file;
        global $good_layer;
        global $data_successlayer;
        $local_data_successlayer = $data_successlayer;
        $local_data_successlayer["name"] = $good_layer;
        $data_errorlayer = $this->insertCorruptedDataIntoArray($local_data_successlayer, "layer");

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file . '/layer/' . $good_layer, $data_errorlayer);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessPutLayer()
    {
        global $good_file;
        global $good_layer;
        global $data_successlayer;
        $local_data_successlayer = $data_successlayer;
        $local_data_successlayer["name"] = $good_layer;

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file . '/layer/' . $good_layer, $local_data_successlayer);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success.');
    }


    public function testErrorPutClass()
    {
        global $good_file;
        global $good_layer;
        global $good_class;
        global $data_successclass;
        $data_errorclass = $this->insertCorruptedDataIntoArray($data_successclass, "class");

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file . '/layer/' . $good_layer . '/class/' . $good_class, $data_errorclass);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessPutClass()
    {
        global $good_file;
        global $good_layer;
        global $good_class;
        global $data_successclass;

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file . '/layer/' . $good_layer . '/class/' . $good_class, $data_successclass);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success.');
    }


    public function testErrorPutLabel()
    {
        global $good_file;
        global $good_layer;
        global $good_class;
        global $good_label;
        global $data_successlabel;
        $data_errorlabel = $this->insertCorruptedDataIntoArray($data_successlabel, "label");

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file . '/layer/' . $good_layer . '/class/' . $good_class . '/label/' . $good_label, $data_errorlabel);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessPutLabel()
    {
        global $good_file;
        global $good_layer;
        global $good_class;
        global $good_label;
        global $data_successlabel;

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file . '/layer/' . $good_layer . '/class/' . $good_class . '/label/' . $good_label, $data_successlabel);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success.');
    }


    public function testErrorPutStyle()
    {
        global $good_file;
        global $good_layer;
        global $good_class;
        global $good_style;
        global $data_successstyle;
        $data_errorstyle = $this->insertCorruptedDataIntoArray($data_successstyle, "style");

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file . '/layer/' . $good_layer . '/class/' . $good_class . '/style/' . $good_style, $data_errorstyle);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessPutStyle()
    {
        global $good_file;
        global $good_layer;
        global $good_class;
        global $good_style;
        global $data_successstyle;

        $client = static::createClient();
        $client->request('POST', '/api/map/' . $good_file . '/layer/' . $good_layer . '/class/' . $good_class . '/style/' . $good_style, $data_successstyle);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success.');
    }


// GETTERS


    public function testErrorGetMap()
    {
        $data = $this->arrayWithABadValue(array("file"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessGetMap()
    {
        $data = $this->arrayOfGoodValues(array("file"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(201, $result->getStatusCode(), 'This Json string should have the status code 201, for HTTP_CREATED (meaning success in creation/update)');
    }


    public function testSuccessGetReference()
    {
        $data = $this->arrayWithABadValue(array("file"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"] . '/reference');
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }


    /*public function testSuccessGetReference()
     { 	$data = $this->arrayOfGoodValues(array("file"));

         $client = static::createClient();
         $client->request('GET', '/api/map/'.$data["file"].'/reference');
         $result = $client->getResponse();

         $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
         $this->assertEquals(201, $result->getStatusCode(), 'This Json string should have the status code 201, for HTTP_CREATED (meaning success in creation/update)');
     }*/


    public function testErrorGetLayer()
    {
        $data = $this->arrayWithABadValue(array("file", "layer"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"] . '/layer/' . $data["layer"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }


    public function testSuccessGetLayer()
    {
        $data = $this->arrayOfGoodValues(array("file", "layer"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"] . '/layer/' . $data["layer"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(201, $result->getStatusCode(), 'This Json string should have the status code 201, for HTTP_CREATED (meaning success in creation/update)');
    }


    public function testErrorGetClass()
    {
        $data = $this->arrayWithABadValue(array("file", "layer", "class"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessGetClass()
    {
        $data = $this->arrayOfGoodValues(array("file", "layer", "class"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(201, $result->getStatusCode(), 'This Json string should have the status code 201, for HTTP_CREATED (meaning success in creation/update)');
    }

    public function testErrorGetLabel()
    {
        $data = $this->arrayWithABadValue(array("file", "layer", "class", "label"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"] . '/label/' . $data["label"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessGetLabel()
    {
        $data = $this->arrayOfGoodValues(array("file", "layer", "class", "label"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"] . '/label/' . $data["label"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(201, $result->getStatusCode(), 'This Json string should have the status code 201, for HTTP_CREATED (meaning success in creation/update)');
    }

    public function testErrorGetStyle()
    {
        $data = $this->arrayWithABadValue(array("file", "layer", "class", "style"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"] . '/style/' . $data["style"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessGetStyle()
    {
        $data = $this->arrayOfGoodValues(array("file", "layer", "class", "style"));

        $client = static::createClient();
        $client->request('GET', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"] . '/style/' . $data["style"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(201, $result->getStatusCode(), 'This Json string should have the status code 201, for HTTP_CREATED (meaning success in creation/update)');
    }


    // SUCCESS DELETERS


    public function testErrorDeleteStyle()
    {
        $data = $this->arrayWithABadValue(array("file", "layer", "class", "style"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"] . '/style/' . $data["style"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }


    public function testSuccessDeleteStyle()
    {
        $data = $this->arrayOfGoodValues(array("file", "layer", "class", "style"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"] . '/style/' . $data["style"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success');
    }

    public function testErrorDeleteLabel()
    {
        $data = $this->arrayWithABadValue(array("file", "layer", "class", "label"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"] . '/label/' . $data["label"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }


    public function testSuccessDeleteLabel()
    {
        $data = $this->arrayOfGoodValues(array("file", "layer", "class", "label"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"] . '/label/' . $data["label"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success');
    }


    public function testErrorDeleteClass()
    {
        $data = $this->arrayWithABadValue(array("file", "layer", "class"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessDeleteClass()
    {
        $data = $this->arrayOfGoodValues(array("file", "layer", "class"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"] . '/layer/' . $data["layer"] . '/class/' . $data["class"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success');
    }


    public function testErrorDeleteLayer()
    {
        $data = $this->arrayWithABadValue(array("file", "layer"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"] . '/layer/' . $data["layer"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }

    public function testSuccessDeleteLayer()
    {
        $data = $this->arrayOfGoodValues(array("file", "layer"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"] . '/layer/' . $data["layer"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success');
    }

    public function testErrorDeleteMap()
    {
        $data = $this->arrayWithABadValue(array("file"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(500, $result->getStatusCode(), 'This Json string should have the status code 500, meaning an error occured.');
    }


    public function testSuccessDeleteMap()
    {
        $data = $this->arrayOfGoodValues(array("file"));

        $client = static::createClient();
        $client->request('DELETE', '/api/map/' . $data["file"]);
        $result = $client->getResponse();

        $this->assertJson($result->getContent(), 'This result should be a valid Json string.');
        $this->assertEquals(200, $result->getStatusCode(), 'This Json string should have the status code 200, meaning success');
    }


//// ERROR CASES


    protected function insertCorruptedDataIntoArray($array, $element_name)
    {

        global $array_keys_color, $array_keys_stringofseveralvalues, $array_keys_numeric, $array_keys_constant, $array_keys_boolean;
        global $corrupted_data_color, $corrupted_data_stringofseveralvalues, $corrupted_data_numeric, $corrupted_data_constant, $corrupted_data_boolean;

        $value_to_corrupt_is_found = FALSE;
        while (!$value_to_corrupt_is_found) { // we have to check because some values, like the simple string ones, don't have forbidden values
            // so we only corrupt values easily corrupted like color, numeric values, etc.
            $corrupted_data_key = array_rand($array);
            $string_key = $element_name . ' ' . $corrupted_data_key;

            if (in_array($string_key, $array_keys_color)) {
                $corrupted_data_value = $corrupted_data_color;
                $value_to_corrupt_is_found = TRUE;
            } else if (in_array($string_key, $array_keys_stringofseveralvalues)) {
                $corrupted_data_value = $corrupted_data_stringofseveralvalues;
                $value_to_corrupt_is_found = TRUE;
            } else if (in_array($string_key, $array_keys_numeric)) {
                $corrupted_data_value = $corrupted_data_numeric;
                $value_to_corrupt_is_found = TRUE;
            } else if (in_array($string_key, $array_keys_constant)) {
                $corrupted_data_value = $corrupted_data_constant;
                $value_to_corrupt_is_found = TRUE;
            } else if (in_array($string_key, $array_keys_boolean)) {
                $corrupted_data_value = $corrupted_data_boolean;
                $value_to_corrupt_is_found = TRUE;
            }

        }

        $array[$corrupted_data_key] = $corrupted_data_value;

        return $array;

    }


    protected function arrayOfGoodValues($array_of_keys)
    {
        global $good_file;
        global $good_layer;
        global $good_class;
        global $good_style;
        global $good_label;

        $return_array = array();

        foreach ($array_of_keys as $thiskey) {
            switch ($thiskey) {
                case "file":
                    $return_array["file"] = $good_file;
                    break;
                case "layer":
                    $return_array["layer"] = $good_layer;
                    break;
                case "class":
                    $return_array["class"] = $good_class;
                    break;
                case "style":
                    $return_array["style"] = $good_style;
                    break;
                case "label":
                    $return_array["label"] = $good_label;
                    break;
                default:
                    throw new \Exception();
            }
        }
        return $return_array;

    }


    protected function arrayWithABadValue($array_of_keys)
    {
        global $bad_file;
        global $bad_layer;
        global $bad_class;
        global $bad_style;
        global $bad_label;

        $return_array = $this->arrayOfGoodValues($array_of_keys);
        $random_key_to_corrupt = array_rand($return_array);

        switch ($random_key_to_corrupt) {
            case "file":
                $new_corrupted_value = $bad_file;
                break;
            case "layer":
                $new_corrupted_value = $bad_layer;
                break;
            case "class":
                $new_corrupted_value = $bad_class;
                break;
            case "style":
                $new_corrupted_value = $bad_style;
                break;
            case "label":
                $new_corrupted_value = $bad_label;
                break;
            default:
                throw new \Exception();
        }

        $return_array[$random_key_to_corrupt] = $new_corrupted_value;

        return $return_array;
    }


}
    
    
    



