<?php
namespace Mapserver\ApiBundle\Library;

/* gvSIG. Sistema de Informaci?n Geogr?fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib??ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
/** 
 * Calcula los intervalos naturales.
 *
 * @author Vicente Caballero Navarro
 */

class JenksGenerator {

  /**
   * tableau des données ordonnées
   */
  public $rVectorDatos = array ();
  /**
   * moyenne de la variable statistique
   */
  public $mediatotal;
  /**
   * tableau d'objets donnée
   */
  public $lVectorDatos = array ();
  /**
   * nombre d'intervalles souhaités
   */
  public $miNumIntervalosSolicitados; //int
  /**
   * nombre effectif d'intervalles
   */
  public $miNumIntervalosGenerados; //int
  /**
   * limites supérieures des classes
   */
  public $mdaValoresRuptura = array (); //double[]
  /**
   * lmimites inférieures des classes
   */
  public $mdaValInit = array (); //double[] 

  /**
   * Crea un nuevo IntervalGenerator.
   *
   * @param layer AlphanumericData
   * @param field Nombre del campo.
   * @param numIntervals N?mero de intervalos.
   */
  function __construct($rVectorDatos, $numIntervals) {
    $this->rVectorDatos = $rVectorDatos;
    $this->miNumIntervalosSolicitados = $numIntervals;
  }

  /**
   * Metodo para generar los intervalos.
  
   * @return true si se han generado correctamente.
   */
  function generarIntervalos() { //return boolean

    $ldUltimaGVF = array ();
    $ldNuevaGVF = array ();
    $ldNuevaGVF[0] = 0;
  
    //  'Calcular la suma de las desviaciones t?picas del total de los datos respecto de la media total
    $ldSDAM[0] = $this->mbGetSumSquaredDeviationArrayMean($this->lVectorDatos, $this->mediatotal);

    if (sizeof($this->lVectorDatos) == 0) {
      //      'S?lo se pueden generar dos intervalos -> hay un valor de ruptura
      $this->mdaValoresRuptura[0] = $this->lVectorDatos[0]->Valor;
      $this->mdaValInit[0] = $this->lVectorDatos[0]->Valor;
      $this->miNumIntervalosGenerados = 2;
      return true;
    }

    //  'Calculamos el n?mero m?ximo de clases reales que podemos generar.
    //  'Este n?mero ser?a igual al n? de elementos que tenga el vector de datos.
    if ($this->miNumIntervalosSolicitados > (sizeof($this->lVectorDatos))) {
      $liNumClasesReales = sizeof($this->lVectorDatos) + 1;
    } else {
      $liNumClasesReales = $this->miNumIntervalosSolicitados;
    }

    //  'Establecemos las clases iniciales especificando unos ?ndices de ruptura en ppo. equidistantes
    $llaIndicesRupturas = array (
      $liNumClasesReales -1
    );
    $llNumElementosPorClase = (int) ((sizeof($this->lVectorDatos)) / $liNumClasesReales);

    for ($i = 0; $i < ($liNumClasesReales -1); $i++) {
      if ($i == 0) {
        $llaIndicesRupturas[$i] = $llNumElementosPorClase -1;
      } else {
        $llaIndicesRupturas[$i] = $llaIndicesRupturas[$i -1] + $llNumElementosPorClase;
      }
    }
    $ldaSDCM_Parciales = array ();
    $ldaSDCM_Validos = array ();

    if (sizeof($llaIndicesRupturas) == 0) {
      return true;
    }
    //  'Calcular la bondad inicial

    if (!$this->mbCalcularGVF($this->lVectorDatos, $ldaSDCM_Parciales, $llaIndicesRupturas, $ldSDAM[0], $ldUltimaGVF, -1, false)) {

      $this->miNumIntervalosSolicitados = sizeof($this->rVectorDatos);
      $this->generarIntervalos();
      return false;
    }

    $ldaSDCM_Validos = $this->getArray($ldaSDCM_Parciales);

    $ldGVFentrepasadas = $ldUltimaGVF[0];

    //'liNumClasesReales no ser? muy grande (11 es el m?ximo)
    for ($k = 1; $k <= 100; $k++) {
      //      'Para cada ?ndice de ruptura...

      for ($i = 0; $i < count($llaIndicesRupturas); $i++) {
        $lbMoverADerecha = false;
        $lbMoverAIzquierda = false;

        $llIndiceRupturaOriginal = $llaIndicesRupturas[$i];
        //echo $llIndiceRupturaOriginal;
        $ldaSDCM_Validos = $this->getArray($ldaSDCM_Parciales);

        //'Hay que decidir hacia donde hay que desplazar el ?ndice de ruptura
        //'Probamos moviendo a derecha (si se puede)
        $lbIntentarDesplazamiento = false;

        if ($i == (count($llaIndicesRupturas) - 1)) {
          if (($llaIndicesRupturas[$i] + 1) < count($this->lVectorDatos)) {
            $lbIntentarDesplazamiento = true;
          }
        } else {
          if (($llaIndicesRupturas[$i] + 1) < $llaIndicesRupturas[$i +1]) {
            $lbIntentarDesplazamiento = true;
          } //'If (llaIndicesRupturas(i) + 1) < llaIndicesRupturas(i + 1) Then
        } //'If i = UBound(llaIndicesRupturas) Then

        if ($lbIntentarDesplazamiento) {
          $llaIndicesRupturas[$i] = $llaIndicesRupturas[$i] + 1;
          if (!$this->mbCalcularGVF($this->lVectorDatos, $ldaSDCM_Parciales, $llaIndicesRupturas, $ldSDAM[0], $ldNuevaGVF, $i, false)) {
            return false;
          }
          if ($ldNuevaGVF[0] > $ldUltimaGVF[0]) {
            $lbMoverADerecha = true;
            $ldaSDCM_Validos = $this->getArray($ldaSDCM_Parciales);
          } else {
            //'Dejamos el ?ndice de ruputura como estaba y probamos con un desplazamiento a izquierda
            $llaIndicesRupturas[$i] = $llIndiceRupturaOriginal;
            $ldaSDCM_Parciales = $this->getArray($ldaSDCM_Validos);
          }
        } //'If lbIntentarDesplazamiento Then

        $lbIntentarDesplazamiento = false;

        //'Probamos moviendo a izquierda (si se puede y no estamos moviendo ya a derechas)
        if (!$lbMoverADerecha) {
          if ($i == 0) { //LBound(llaIndicesRupturas) Then

            if (($llaIndicesRupturas[$i] - 1) >= 0) { //LBound(lVectorDatos()) Then
              $lbIntentarDesplazamiento = true;
            } //'If (llaIndicesRupturas(i) - 1) >= LBound(lVectorDatos()) Then
          } else {
            if (($llaIndicesRupturas[$i] - 1) > $llaIndicesRupturas[$i -1]) {
              $lbIntentarDesplazamiento = true;
            } //'If (llaIndicesRupturas(i) - 1) > llaIndicesRupturas(i - 1) Then
          } //'If i = LBound(llaIndicesRupturas) Then
        } //'If Not lbMoverADerecha Then

        if ($lbIntentarDesplazamiento) {
          $llaIndicesRupturas[$i] = $llaIndicesRupturas[$i] - 1;

          if (!$this->mbCalcularGVF($this->lVectorDatos, $ldaSDCM_Parciales, $llaIndicesRupturas, $ldSDAM[0], $ldNuevaGVF, $i, true)) {
            return false;
          }

          if ($ldNuevaGVF[0] > $ldUltimaGVF[0]) {
            $lbMoverAIzquierda = true;
            $ldaSDCM_Validos = $this->getArray($ldaSDCM_Parciales);
          } else {
            //'Dejamos el ?ndice de ruputura como estaba
            $llaIndicesRupturas[$i] = $llIndiceRupturaOriginal;
            $ldaSDCM_Parciales = $this->getArray($ldaSDCM_Validos);
          }
        } //'If lbIntentarDesplazamiento Then

        $lbIntentarDesplazamiento = false;

        //'Si se ha decidido desplazar el ?ndice ... continuamos hasta que no podamos mejorar la GVF
        if ($lbMoverAIzquierda || $lbMoverADerecha) {
          $ldUltimaGVF[0] = $ldNuevaGVF[0];

          $sortie = false;

          while (!$sortie) {
            $llIndiceRupturaOriginal = $llaIndicesRupturas[$i];

            if ($lbMoverADerecha) {
              if ($i == count($llaIndicesRupturas) - 1) {
                if (($llaIndicesRupturas[$i] + 1) >= count($this->lVectorDatos)) {
                  $sortie = true;
                }
              } else {
                if (($llaIndicesRupturas[$i] + 1) >= $llaIndicesRupturas[$i +1]) {
                  $sortie = true;
                }
              } //'If i = UBound(llaIndicesRupturas) Then

              $llaIndicesRupturas[$i] = $llaIndicesRupturas[$i] + 1;
            } else { //'If lbMoverAIzquierda Then

              if ($i == 0) { //LBound(llaIndicesRupturas) Then

                if (($llaIndicesRupturas[$i] - 1) < 0) { //LBound(lVectorDatos()) Then Exit Do
                  $sortie = true;
                }
              } else {
                if (($llaIndicesRupturas[$i] - 1) <= $llaIndicesRupturas[$i -1]) {
                  $sortie = true;
                }
              } //'If i = LBound(llaIndicesRupturas) Then

              $llaIndicesRupturas[$i] = $llaIndicesRupturas[$i] - 1; //////////////////
            } //'If lbMoverADerecha Then

            if (!$this->mbCalcularGVF($this->lVectorDatos, $ldaSDCM_Parciales, $llaIndicesRupturas, $ldSDAM[0], $ldNuevaGVF, $i, $lbMoverAIzquierda)) {
              return false;
            }

            if ($ldNuevaGVF[0] < $ldUltimaGVF[0]) {
              // 'Dejar el ?ndice donde estaba
              $llaIndicesRupturas[$i] = $llIndiceRupturaOriginal;
              $ldaSDCM_Parciales = $this->getArray($ldaSDCM_Validos);
              $sortie = true;
            } else {
              $ldUltimaGVF[0] = $ldNuevaGVF[0];
              $ldaSDCM_Validos = $this->getArray($ldaSDCM_Parciales);
            }

          }
        } //'If lbMoverAIzquierda Or lbMoverADerecha Then
      }

      if ($ldUltimaGVF[0] <= $ldGVFentrepasadas) {
        $i = 101;
      }

      $ldGVFentrepasadas = $ldUltimaGVF[0];
    }

    $this->mdaValoresRuptura = array ();
    $this->mdaValInit = array ();
    for ($i = 0; $i < count($llaIndicesRupturas); $i++) { // LBound(mdaValoresRuptura) To UBound(mdaValoresRuptura)
      if ($llaIndicesRupturas[$i] == -1)
        $llaIndicesRupturas[$i] = 1;
      if ($llaIndicesRupturas[$i] > (count($this->lVectorDatos) - 1))
        $llaIndicesRupturas[$i] = count($this->lVectorDatos) - 1;
      $this->mdaValoresRuptura[$i] = $this->lVectorDatos[$llaIndicesRupturas[$i]]->Valor;

      if (($llaIndicesRupturas[$i] + 1) < count($this->lVectorDatos)) {
        $this->mdaValInit[$i] = $this->lVectorDatos[$llaIndicesRupturas[$i] + 1]->Valor;
      } else {
        $mdaValInit[$i] = $this->lVectorDatos[$llaIndicesRupturas[$i]]->Valor;
      }
      //      'Hay que aplicar una peque?a correcci?n a los valores de ruptura para que los intervalos que genera
      //    'mapobjects se aprechen en su totalidad, o lo que es lo mismo, vengan todos representados en el mapa.
      //  'Con esto tambi?n se consigue hallar intervalos equivalentes a los de ArcView. Esta correcci?n consiste
      //'en sumar el m?nimo incremento a los valores de ruptura. No lo hago aqu? sino en la ventana de propiedades de capa.
    }
    $miNumIntervalosGenerados = sizeof($this->mdaValoresRuptura) + 2;

    $ldaSDCM_Validos = null;
    $ldaSDCM_Parciales = null;
    $this->lVectorDatos = null;

    return true;
  }

  /**
   * Devuelve la "SDAM" de un conjunto de datos que vienen almacenados en el
   * vector rVectorDatos
   *
   * @param rVectorDatos Datos
   * @param vdMedia Media
   *
   * @return suma de las desviaciones t?picas del total de los datos respecto
   *       de la media total
   */
  function mbGetSumSquaredDeviationArrayMean($rVectorDatos, $vdMedia) {
    //int i;

    $rdSDAM = 0;
    //echo sizeof($rVectorDatos);
    for ($i = 0; $i < sizeof($rVectorDatos); $i++) { // LBound(rVectorDatos) To UBound(rVectorDatos)
      $rdSDAM = $rdSDAM + (pow(($rVectorDatos[$i]->Valor - $vdMedia), 2) * ($rVectorDatos[$i]->Coincidencias));
    }
    return $rdSDAM;
  }

  /**
   * Esta funci?n obtiene los datos en los que queremos hallar las rupturas
   * naturales. Los datos se devuelven ordenados en un vector. Tambi?n
   * devuelve la media total
   *
   * @param rVectorDatos Datos
   * @param rdMediaTotal Media total
   *
   * @return True si se ha calculado correctamente.
   *
   * @throws com.iver.cit.gvsig.fmap.DriverException
   * @throws DriverException
   */
  function mbObtenerDatos($rVectorDatos) {
    
    $sumVectorDatos = 0;
    // 'Nos cuidamos de recorrer todos los registros sin consultar la propiedad EOF del recordset
    for ($i = 0; $i < sizeof($rVectorDatos); $i++) {
    
      $this->lVectorDatos[$i] = new udtDatosEstudio();
      $this->lVectorDatos[$i]->Valor = $rVectorDatos[$i];
      $this->lVectorDatos[$i]->Coincidencias = 1;

      $sumVectorDatos += $rVectorDatos[$i];
      
    }
    $this->mediatotal = $sumVectorDatos / sizeof($rVectorDatos);
    return true;
  }

    
  /**
   * Esta funci?n s?lo calcula las SDCM de las clases adyacentes al ?ndice de
   * ruptura actual. Si el ?ndice de ruptura actual es -1 entonces las
   * calcula todas! . En este caso no importa el valor de
   * vbDesplazAIzquierda
   *
   * @param rVectorDatos Datos
   * @param raClases Calses que representan a los intervalos.
   * @param rlaIndicesRuptura ?ndices de ruptura.
   * @param vdSDAM Desviaci?n standard.
   * @param rdGVF Desviaci?n standard de los intervalos.
   * @param vlIndiceRupturaActual ?ndice de ruptura actual.
   * @param vbDesplazAIzquierda Desplazamiento a la izquierda.
   *
   * @return True si se ha calculado correctamente.
   */
  function mbCalcularGVF($rVectorDatos, & $raClases, $rlaIndicesRuptura, $vdSDAM, & $rdGVF, $vlIndiceRupturaActual /* As Long = -1*/
  , $vbDesplazAIzquierda) {
  
    if ($vlIndiceRupturaActual == -1) {
      // 'Obtenemos los datos para todas las clases = intervalos
      for ($i = 0; $i < sizeof($rlaIndicesRuptura); $i++) {
        if ($i == 0) { // LBound(rlaIndicesRuptura) Then

          if (!$this->mbGetDatosClase($rVectorDatos, 0, $rlaIndicesRuptura[$i], $raClases, $i)) {
            return false;
          }
        } else {
          if (!$this->mbGetDatosClase($rVectorDatos, $rlaIndicesRuptura[$i -1] + 1, $rlaIndicesRuptura[$i], $raClases, $i)) {
            return false;
          }
        }
      }

      //'Falta la ?ltima clase
      if (!$this->mbGetDatosClase($rVectorDatos, $rlaIndicesRuptura[sizeof($rlaIndicesRuptura) - 1] + 1, sizeof($rVectorDatos) - 1, $raClases, sizeof($rlaIndicesRuptura))) {
        return false;
      }
    } else {
      $i = $vlIndiceRupturaActual;

      //'Hay que determinar para qu? clases debemos volver a recalcular los datos en funci?n del ?ndice de ruptura que estamos modificando
      if ($vbDesplazAIzquierda) {
        //  'Recalcular los datos de la clase izquierda
        if (!$this->mbRecalcularDatosClase($raClases, $i, $rVectorDatos, $rlaIndicesRuptura[$i] + 1, $vdSDAM, false)) {
          return false;
        }

        //  'Recalcular los datos de la clase derecha
        if (!$this->mbRecalcularDatosClase($raClases, $i +1, $rVectorDatos, $rlaIndicesRuptura[$i] + 1, $vdSDAM, true)) {

          return false;
        }
        
      } else {

        if (!$this->mbRecalcularDatosClase($raClases, $i, $rVectorDatos, $rlaIndicesRuptura[$i], $vdSDAM, true)) {
          return false;
        }
        //  'Recalcular los datos de la clase derecha
        if (!$this->mbRecalcularDatosClase($raClases, $i +1, $rVectorDatos, $rlaIndicesRuptura[$i], $vdSDAM, false)) {
          return false;
        }
      }
    
    }
    $ldSDCM_aux = 0;
    
    for ($j = 0; $j < sizeof($raClases); $j++) { //LBound(raClases) To UBound(raClases)
      $ldSDCM_aux = $ldSDCM_aux + $raClases[$j]->SDCM;
    }

    $rdGVF[0] = ($vdSDAM - $ldSDCM_aux) / $vdSDAM;

    return true;
  }

  /**
   * Recalcula los datos de las clases.
   *
   * @param rClase Clases.
   * @param i ?adir ?adir ?adir ?adir indica si a la clase se le a?ade un
   *      elemento (True) o se le quita (False)
   * @param rVectorDatos Datos.
   * @param vlIndiceElemento es el ?ndice del elemento que se le va a?adir o
   *      a quitar a la clase
   * @param vdSDAM desviaci?n standard.
   * @param vbA ?adir DOCUMENT ME!
   *
   * @return True si se ha calculado correctamente.
   */
  function mbRecalcularDatosClase(& $rClase, $i, $rVectorDatos, $vlIndiceElemento, $vdSDAM, $vbAnyadir) {

    if ($vlIndiceElemento > sizeof($rVectorDatos) - 1)
      return true;
    $ldValor = $rVectorDatos[$vlIndiceElemento]->Valor;
    $llNumCoincidencias = $rVectorDatos[$vlIndiceElemento]->Coincidencias;

    if ($vbAnyadir) {
      //'Actualizamos la suma total
      $rClase[$i]->SumaTotal = $rClase[$i]->SumaTotal + ($ldValor * $llNumCoincidencias);

      //'Actualizamos la suma de cuadrados total
      $rClase[$i]->SumaCuadradoTotal = $rClase[$i]->SumaCuadradoTotal + pow(($ldValor * $llNumCoincidencias), 2);

      //'Actualizamos el producto total
      //'rClase.ProductoTotal = rClase.ProductoTotal * (ldValor * llNumCoincidencias)
      //'Actualizamos el n? de elementos
      $rClase[$i]->NumElementos = $rClase[$i]->NumElementos + $llNumCoincidencias;
    } else {
      //'Actualizamos la suma total
      $rClase[$i]->SumaTotal = $rClase[$i]->SumaTotal - ($ldValor * $llNumCoincidencias);

    
      $rClase[$i]->SumaCuadradoTotal = $rClase[$i]->SumaCuadradoTotal - pow(($ldValor * $llNumCoincidencias), 2);
    
      $rClase[$i]->NumElementos = $rClase[$i]->NumElementos - $llNumCoincidencias;
    } // 'If vbA?adir Then
    if ($rClase[$i]->NumElementos <= 0)
      $rClase[$i]->NumElementos = 1;
    //'Obtenemos la nueva media
    $rClase[$i]->Media = $rClase[$i]->SumaTotal / $rClase[$i]->NumElementos;

    //'Actualizamos la SDCM
    $rClase[$i]->SDCM = ($rClase[$i]->SumaCuadradoTotal) - (2 * $rClase[$i]->Media * $rClase[$i]->SumaTotal) + ($rClase[$i]->NumElementos * pow($rClase[$i]->Media, 2));

    //}catch (Exception e) {
    //  return false;
    //}
    return true;
  }
  
  /**
     * Devuelve la "SDCM" de un conjunto de datos que vienen almacenados en el
     * vector rVectorDatos y que est?n delimitados por vlLimiteInf y
     * vlLimiteInf
     *
     * @param rVectorDatos Datos
     * @param vlLimiteInf L?mite inferior.
     * @param vlLimiteSup L?mite superior.
     * @param rClase Calses que representan a los intervalos.
     * @param numClas N?mero de calses.
     *
     * @return True si se ha calculado correctamente.
     */
  function mbGetDatosClase($rVectorDatos, $vlLimiteInf, $vlLimiteSup, & $rClase, $numClas) {
    //int i;

    if ($vlLimiteInf < 0) {
      return false;
    }

    if ($vlLimiteSup > sizeof($rVectorDatos)) {
      return false;
    }

    if ($vlLimiteSup < $vlLimiteInf) {
      return false;
    }

    // 'Inicializamos los datos de la clase
    $rClase[$numClas] = new udtDatosClase();

    //'Hallamos la media de la clase
    for ($i = $vlLimiteInf; $i < ($vlLimiteSup +1); $i++) {
      $rClase[$numClas]->NumElementos = $rClase[$numClas]->NumElementos + $rVectorDatos[$i]->Coincidencias;

      //'rClase.Media = rClase.Media + (rVectorDatos(i).Valor * rVectorDatos(i).Coincidencias)
      $rClase[$numClas]->SumaTotal = $rClase[$numClas]->SumaTotal + $rVectorDatos[$i]->Valor * $rVectorDatos[$i]->Coincidencias;

      //'rClase.ProductoTotal = rClase.ProductoTotal * (rVectorDatos(i).Valor * rVectorDatos(i).Coincidencias)
      $rClase[$numClas]->SumaCuadradoTotal = $rClase[$numClas]->SumaCuadradoTotal + (pow($rVectorDatos[$i]->Valor * $rVectorDatos[$i]->Coincidencias, 2));
    }

    //'rClase.Media = rClase.Media / llTotalElementos
    $rClase[$numClas]->Media = $rClase[$numClas]->SumaTotal / $rClase[$numClas]->NumElementos;
    $rClase[$numClas]->SDCM = ($rClase[$numClas]->SumaCuadradoTotal) - (2 * $rClase[$numClas]->Media * $rClase[$numClas]->SumaTotal) + ($rClase[$numClas]->NumElementos * pow($rClase[$numClas]->Media, 2));
    return true;
  }

  function getArray($tabudtDatos) {

    //$aux = new udtDatosClase();

    for ($i = 0; $i < sizeof($tabudtDatos); $i++) {
      $aux[$i] = new udtDatosClase();
      $aux[$i]->Media = $tabudtDatos[$i]->Media;
      $aux[$i]->NumElementos = $tabudtDatos[$i]->NumElementos;
      $aux[$i]->SDCM = $tabudtDatos[$i]->SDCM;
      $aux[$i]->SumaCuadradoTotal = $tabudtDatos[$i]->SumaCuadradoTotal;
      $aux[$i]->SumaTotal = $tabudtDatos[$i]->SumaTotal;
    }

    return $aux;
  }
  /**
   * Devuelve el valor de ruptura seg?n el ?ndice que se le pasa como
   * par?metro.
   *
   * @param viIndice ?nidice del valor de ruptura.
   *
   * @return Valor de ruptura.
   */
  function getValorRuptura($viIndice) {
    return $this->mdaValoresRuptura[$viIndice];
  }

  /**
   * Devuelve el valor inicial de cada intervalo
   *
   * @param index ?ndice del intervalo
   *
   * @return valor del intervalo.
   */
  function getValInit($index) {
    return $this->mdaValInit[$index];
  }

  /**
   * Devuelve el n?mero de intervalos que se pueden representar, no tiene
   * porque coincidir con el n?mero de intervalos que se piden.
   *
   * @return N?mero de intervalos calculados.
   */

  function getNumIntervals() {
    return $this->miNumIntervalosGenerados;
  }

  
}

class udtDatosEstudio {
  public $Valor; //double 
  public $Coincidencias; //long
}

/**
 * Clase para contener los atributos: N?mero de Elementos. Media.
 * SumaTotal. SumaCuadradoTotal. Desviaci?n standard.
 *
 * @author Vicente Caballero Navarro
 */
class udtDatosClase {
  public $NumElementos; //long              'N? total de elementos que hay en la clase
  public $Media; //double                  'Media de la clase
  public $SumaTotal; //double              'Suma total de los elementos
  public $SumaCuadradoTotal; //   double   'Suma del total de los cuadrados de los elementos
  public $SDCM; //    double              'Suma de la desviaci?n t?pica de los elementos de la clase respecto de la media de la clase

}
?>