<?php

namespace Mapserver\ApiBundle\Exception;

/**
 * Mapserver Exception.
 *
 * @author alkante <support@alkante.com>
 */
class MapserverException extends \Exception 
{
    protected $data;

    /**
     * Creates a new MapserverException.
     *
     * @param string $message Exception message
     * @param array  $data    Custom data
     * @param string $code
     * @param string $previous
     */
    public function __construct($message = null, array $data = array(), $code = null, $previous = null) 
    {
        $message .= "\nMapserver errors = {\n".implode("\n", self::getMapserverErrors())."\n}";
        parent::__construct($message, $code, $previous);
        $this->data = $data;
    }

    /**
     * Return all mapserver errors formatted
     * @return array
     */
    public static function getMapserverErrors()
    {
    	  $codes = array("MS_NOERR", "MS_IOERR", "MS_MEMERR", "MS_TYPEERR", "MS_SYMERR", "MS_REGEXERR", "MS_TTFERR", "MS_DBFERR", "MS_IDENTERR", "MS_EOFERR", "MS_PROJERR", "MS_MISCERR", "MS_CGIERR", "MS_WEBERR", "MS_IMGERR", "MS_HASHERR", "MS_JOINERR", "MS_NOTFOUND", "MS_SHPERR", "MS_PARSEERR", "MS_OGRERR", "MS_QUERYERR", "MS_WMSERR", "MS_WMSCONNERR", "MS_ORACLESPATIALERR", "MS_WFSERR", "MS_WFSCONNERR", "MS_MAPCONTEXTERR", "MS_HTTPERR", "MS_WCSERR");
    	  $vcodes = array_map("constant", $codes);
    	  $codes = array_combine($codes, $vcodes);
        $array = array();
        $error = ms_GetErrorObj();
        while($error)
        {
            $array[] = sprintf("[CODE=%s] Error in %s : %s", implode("|", array_keys($codes, $error->code)), $error->routine, $error->message);
            $error = $error->next();
        }
        ms_ResetErrorList();
        return $array;
    }
    
    /**
     * @return array The data.
     */
    public function getData()
    {
        return $this->data;
    }
}