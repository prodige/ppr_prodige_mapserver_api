<?php

namespace Mapserver\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Mapserver\ApiBundle\Exception\MapserverException;
use PhpCollection\AbstractMap;
use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\Process\Process;

/**
 * Base Controller that provides common helper functions.
 *
 * @author alkante <support@alkante.com>
 */
class BaseController extends AbstractController implements MapserverDefaults
{

    /**
     * Translate text.
     *
     * @param string $text Text to translate.
     * @param array $params Placeholder parameters (substitution).
     *
     * @return string Translated text
     * @see http://symfony.com/fr/doc/current/book/translation.html
     *
     * @example <code>$this->_t('Hello %name%', array('%name%'=>'World'))</code>
     */
    protected function _t($text, $params = array())
    {
        return $text;//$this->get('translator')->trans($text, $params);
    }

    /**
     * Returns the kernel env variable (debug/dev/prod).
     *
     * @return string Kernel environment.
     */
    protected function getEnv()
    {
        return $this->container->get('kernel')->getEnvironment();
    }

    /**
     * Returns true if app is in prod env.
     *
     * @return boolean True if app is in prod env, false otherwise.
     */
    protected function isProd()
    {
        return $this->getEnv() == 'prod';
    }

    /**
     * Returns the default logger.
     *
     * @return \Psr\Log\LoggerInterface The default logger
     */
    protected function getLogger()
    {
        return $this->container->get('logger');
    }

    /**
     * Shortcut to return the request service.
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->get('request_stack')->getCurrentRequest();
    }

    /**
     * Build a standard Json response error message.
     *
     * Json response is a json object with the following format: {status:<status>, error:<message>, description:<desc>, ...}
     *
     * @param number $status HTTP status of the response.
     * @param string $error Error message.
     * @param string $message Error detailled description.
     * @param array $extra Adds extra information to the response.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse The formatted Json response
     */
    protected function formatJsonError($status = 500, $error = "Internal Server Error", $description = "", array $extra = array())
    {
        $this->getLogger()->error($description, array("stacktrace" => (isset($extra["stacktrace"]) ? $extra["stacktrace"] : debug_backtrace(false))));
        unset($extra["stacktrace"]);
        $data = array(
            'server' => $this->container->get('kernel')->getLogDir()
        , 'status' => $status
        , 'success' => false
        , 'error' => $error
        , 'description' => $description
        , 'stacktrace' => (isset($extra["stacktrace"]) ? $extra["stacktrace"] : "not_set")
        , 'params' => $extra
        );
        return new JsonResponse($data, $status);
    }

    /**
     * Build a standard Json response success message.
     *
     * Json response is a json object with the following format: {status:<status>, ...}
     *
     * @param array $extra Adds extra information to the response.
     * @param number $status HTTP status of the response.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse The formatted Json response
     */
    protected function formatJsonSuccess(array $extra = array(), $status = 200)
    {
        $data = array_merge(array('status_code' => $status), $extra);
        return new JsonResponse($data, $status);
    }

    /**
     * Return the absolute path to user directory. Create it if not exists
     * @param string $directory
     * @return string Absolute path to user_directory
     */
    protected function getUserDirectory($directory)
    {
        $directory = $directory
            ? $directory
            : $this->container->getParameter("root_data_directory") . '/' . $this->container->getParameter("default_directory");

        if (!is_dir($directory)) {
            if (basename($directory) != "__template__") {
                $template_dir = $this->getUserDirectory("__template__");
                $replace_directory = preg_replace("!/$!", "", $directory);

                $process_args = array("cp", "-R", $template_dir, $replace_directory);
                $process = Process::fromShellCommandline($process_args);
                $process->run();

                // exec("cp -R ".$template_dir." ".$replace_directory);

                $process_args = array("chmod", "-R", "770", $replace_directory);
                $process = Process::fromShellCommandline($process_args);
                $process->run();

                // exec("chmod -R 770 ".$replace_directory);
            } else {
//                echo "par ici : '".$directory."'.</br>";
                throw new \Exception(sprintf($this->_t("The template directory for mapfiles : %s must exist"), $directory));

            }
        }
        return $directory;
    }

    const DEFAULTMSG_CALLSET = "On %s object, call of method set() on attibute %s";
    const DEFAULTMSG_CALLFUNCTION = "On %s object, call of method %s() with arguments";

    /**
     * Transform an hexadecimal color in RGB array value.
     * Transform an RGB string value in RGB array value.
     * @param unknown $color
     * @return string
     */
    protected function getRGB($color)
    {
        if (empty(trim($color))) {
            return array(-1, -1, -1);
        }
        // is it HEX ?(like #FFFFFF )

        if (preg_match("!^#?[[:xdigit:]]{6}$!", $color)) {
            $color = str_split(str_replace("#", "", $color), 2);
            $color = array_map("hexdec", $color);
            $color = implode(",", $color);
            return explode(",", $color); // this is an array

        } else throw new MapserverException($this->_t(sprintf("Could not add layer because %s is not a correct color variable", $color)));
    }

    /**
     * Convert a color obj in hexa code with prefix #
     * @param \colorObj $colorObj
     * @return NULL|string
     */
    protected function colorToHex(\colorObj $colorObj = null)
    {
        if (!$colorObj) return null;
        $colors = array_map("intval", array($colorObj->red, $colorObj->green, $colorObj->blue));
        if (min($colors) == -1) return null;
        $dechex = function ($color) {
            return sprintf("%'.02s", dechex($color));
        };
        return "#" . implode(array_map($dechex, $colors));
    }

    /**
     * Convert a rectObj mapserver object in array
     * @param \rectObj $rectObj
     * @return NULL|array
     */
    protected function rectObjToArray(\rectObj $rectObj = null)
    {
        if ($rectObj === null) return null;
        $array = array(
            "minx" => $rectObj->minx,
            "miny" => $rectObj->miny,
            "maxx" => $rectObj->maxx,
            "maxy" => $rectObj->maxy,
        );
        // @rule [MAPSCRIPT] : Note the members (minx, miny, maxx ,maxy) are initialized to -1;
        // don't return an all -1 value array
        if (min($array) == -1 && max($array) == -1) {
            return array();
        }
        return $array;
    }

    /**
     * Convert a hashTableObj mapserver object in array
     * @param \hashTableObj $hashtable
     * @return NULL|array
     */
    protected function hashTableObjToArray(\hashTableObj $hashtable = null)
    {
        if ($hashtable === null) return null;
        $key = null;
        $array = array();
        while ($key = $hashtable->nextkey($key)) {
            $array[$key] = $hashtable->get($key);
        }
        return $array;
    }

    /**
     * Call the set() method on Mapserver object on the attribute to set its value (The value must be not empty).
     * Throw an exception if the setting failed
     *
     * @param unknown $object
     * @param unknown $attribute
     * @param unknown $value
     * @param string $message
     * @param string $successValue MS_SUCCESS The success value as result of the call
     * @param boolean $required true       Check that is a not empty value
     * @throws MapserverException
     */
    protected function callSet($object, $attribute, $value, $message = "", $successValue = MS_SUCCESS, $required = true)
    {

        if (is_null($value)) {
            return;
        }

        // initialisation du $result
        $result = array();
        ob_start();
        var_dump(func_get_args(), $value);
        $r = ob_get_clean();
        $this->getLogger()->debug(__METHOD__, array("args" => $r));
        if (is_null($object)) {
            $message = $message ?: sprintf($this->_t(self::DEFAULTMSG_CALLSET), "[NULL Object]", $attribute);
            throw new MapserverException($message, array("value" => $value));
        }
        $message = $message ?: sprintf($this->_t(self::DEFAULTMSG_CALLSET), get_class($object), $attribute);

        if ($required) {
            if (!isset($value) || (!is_numeric($value) && empty($value))) {
                return MS_FALSE;
            }
        }
        if (!$required && is_null($value)) {
            if ($object->$attribute) {
                $result = $object->set($attribute, "");
            }

        } else {
            $result = $object->set($attribute, $value);
        }
        try {
            $this->getLogger()->debug(__METHOD__, array("args" => $r, "result" => array($result)));
        } catch (\Exception $e) {
            $this->getLogger()->debug(__METHOD__, array("args" => $r, "result" => array($result)));
        }
        if ($result !== null && $result !== $successValue) {
            throw new MapserverException($message, array("value" => $value));
        }
        return $result;
    }

    /**
     * Call the set() method on Mapserver object on the attribute to set its value (The value can be empty).
     * Throw an exception if the setting failed
     *
     * @param object $object The Mapserver object
     * @param unknown $attribute
     * @param unknown $value
     * @param string $message
     * @param string $successValue The success value as result of the call
     * @throws MapserverException
     */
    protected function callSetNotRequired($object, $attribute, $value, $message = "", $successValue = MS_SUCCESS)
    {
        return $this->callSet($object, $attribute, $value, $message, $successValue, false);
    }


    protected function callSetNumeric($object, $attribute, $value, $message = "", $successValue = MS_SUCCESS, $required = true)
    {
        ob_start();
        var_dump(func_get_args(), $value);
        $r = ob_get_clean();
        $this->getLogger()->debug(__METHOD__, array("args" => $r));
        if ($value === null) return $this->callSet($object, $attribute, $value, $message, $successValue, $required);
        if (is_numeric($value)) {
            $this->callSet($object, $attribute, $value, $message, $successValue, $required);
        } else {
            throw new MapserverException(sprintf($this->_t("Could not add element because [%s] is not a numeric variable for %s"), $value, $attribute));
        }
    }


    protected function callSetNumericOrFieldName($bindingConstantPrefix, $object, $attribute, $value, $message = "", $successValue = MS_SUCCESS, $required = true)
    {
        $value = preg_replace("!\[|\]!", '', $value);
        if (is_numeric($value)) {
            $this->callSetNumeric($object, $attribute, $value, $message, $successValue, $required);
        } else {
            $constant = strtoupper(preg_replace("!_$!", '', $bindingConstantPrefix) . '_' . $attribute);
            if (defined($constant) && method_exists($object, "setBinding")) {
                if (empty($value)) {
                    $object->removeBinding(constant($constant));
                } else {
                    $object->setBinding(constant($constant), /*utf8_decode*/ ($value));
                }
            } else {
                $this->callSet($object, $attribute, $value, $message, $successValue, $required);
            }
        }
    }


    protected function callSetBoolean($object, $attribute, $value, $message = "", $successValue = MS_SUCCESS, $required = true)
    {
        if ($value === null) return;
        if (is_bool($value)) {
            $this->callSet($object, $attribute, $value, $message, $successValue, $required);
        } else {
            throw new MapserverException(sprintf($this->_t("Could not add element because [%s] is not a boolean variable for %s"), $value, $attribute));
        }
    }

    // takes an object, attribute and string and will check if the string matches some existent constant (with a certain constant prefix)
    // the string in parameter can already have the constant prefix or not, that makes no difference
    // if the constant name exists, it is added to the object.
    // if not, return a MapServerException

    protected function callSetConstant($object, $attribute, $string, $constant_prefix = "", $message = "", $successValue = MS_SUCCESS, $required = true)
    {
        $string = strtoupper($string);
        $constant_prefix = strtoupper($constant_prefix);
        $constant_name = "MS_" . str_replace("MS_", "", $string); //adding "MS_" if needed
        if (!empty($constant_prefix)) {
            $constant_name = "MS_" . $constant_prefix . "_" . str_replace("MS_", "", $string); // adding "MS_(prefix)_" instead of "MS_" if needed
        }

        if (defined($constant_name)) {
            $constant_value = constant($constant_name);
            $this->getLogger()->debug(__METHOD__, array("attribute" => $attribute, "constante_name" => $constant_name));
            $this->callSet($object, $attribute, $constant_value, $message, $successValue, $required);
        } else {
            throw new MapserverException(sprintf($this->_t("Could not add element because %s is not an existent status constant name"), $constant_name));
        }

    }


    protected function callSetSeveralValuesFromString($object, $attributes_array, $value_string, $message = "", $successValue = MS_SUCCESS, $required = true)
    {
        $values_array = explode(" ", $value_string);
        $numberofvaluesexpected = count($attributes_array);

        if (count($values_array) == $numberofvaluesexpected) {
            $counter = 0;
            while ($counter < $numberofvaluesexpected) {
                $thisAttribute = $attributes_array[$counter];
                $thisValue = $values_array[$counter];
                $this->callSetNumeric($object, $thisAttribute, $thisValue, $message, $successValue, $required);
                $counter++;
            }
        } else {
            throw new MapserverException(sprintf($this->_t("Could not add element because a problem occured when parsing %s string value."), $value_string));
        }
    }

    /**
     * Call a method (=callable) on Mapserver object with the arguments.
     * Throw an exception if the setting failed
     *
     * @param object $object The Mapserver object
     * @param string $callable The method to call on the object
     * @param array $arguments The callable arguments
     * @param string $message The exception message
     * @param string $successValue The success value as result of the call
     * @throws MapserverException
     */
    protected function callFunction($object, $callable, array $arguments, $message = "", $successValue = MS_SUCCESS)
    {
        $message = $message ?: sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), get_class($object), $callable);

        $result = @call_user_func_array(array($object, $callable), $arguments);

        if ($result !== $successValue) {
            throw new MapserverException($message, $arguments);
        }
    }
}
