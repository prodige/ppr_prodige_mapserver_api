<?php

namespace Mapserver\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Mapserver\ApiBundle\Exception\MapserverException;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\RequestParam;

// POST params
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;

/**
 *
 * @author alkante <support@alkante.com>
 *
 */
class MapfileController extends BaseController
{
    const HATCHSYMBOL = "hatchsymbol";
    const NB_MAX_CLASSES = 40;
    const LEGEND_ICON_WIDTH = 8;
    const LEGEND_ICON_HEIGHT = 8;

    /**
     * Construct and returns the PhpOGR data path and name for a layer
     * @param array $directories
     * @param string $layerName
     * @param string $layerData
     * @param int $connectiontype
     * @param string $connection
     * @return array(string layerData, string layerName)
     */
    protected static function getDataForPhpOGR(array $directories, $layerName, $layerData, $connectiontype, $connection, $bForCmdOgr = false, $bFullPath = false)
    {
        switch ($connectiontype) {
            case MS_INLINE :
            case MS_SHAPEFILE :
                if (!file_exists($layerData)) {
                    $_layerData = $layerData;
                    foreach ($directories as $directory) {
                        if (file_exists($directory . "/" . $_layerData)) {
                            $layerData = $directory . "/" . $_layerData;
                            break;
                        }
                    }
                }
                if (!file_exists($layerData)) {
                    $layerData = null;
                }
                $layerName = pathinfo($layerData, PATHINFO_FILENAME);
                break;

            case MS_POSTGIS :

                $schema = preg_replace('!.+from\s+("?(\w+)"?\.)?"?(\w+)"?.+!i', "$2", $layerData);
                $table = preg_replace('!.+from\s+("?(\w+)"?\.)?"?(\w+)"?.+!i', "$3", $layerData);
                $layerName = $table;
                $echap = ($bForCmdOgr ? "'" : "");
                $layerData = "PG:" . $echap . $connection . ($schema ? " active_schema=" . $schema : "") . " connect_timeout=3" . $echap;
                if ($bFullPath) {
                    $echap = ($bForCmdOgr ? '"' : "");
                    $layerData .= " " . $echap . $layerName . $echap;
                }
                break;
            case MS_WFS :
                
                if ($bForCmdOgr) {
                    $echap = ($bForCmdOgr ? '"' : "");
                }
                $layerData = $echap ."WFS:" . $connection. $echap;
                $layerData .= " " . $echap . $layerName . $echap;
                
                break;
            case MS_OGR :
                foreach ($directories as $directory) {
                    if (file_exists($directory . "/" . $connection)) {
                        $layerData = $directory . "/" . $connection;
                        $layerName = pathinfo($layerData, PATHINFO_FILENAME);
                        break;
                    }
                }
                break;

            default :
            case MS_RASTER :
            case MS_WMS :
                break;

            case MS_ORACLESPATIAL :
            case MS_TILED_SHAPEFILE :
            case MS_SDE :

            case MS_TILED_OGR :
            case MS_GRATICULE :
            case MS_PLUGIN :
            case MS_UNION :
                $layerData = null;
                $layerName = null;
                break;
        }

        return array($layerData, $layerName);
    }

    /**
     * Construct and returns the PhpOGR data path and name for a layer
     * @param \mapObj $mapObj
     * @param \layerObj $layerObj
     * @return array(string layerData, string layerName)
     */
    protected static function _getDataForPhpOGR(\mapObj $mapObj, \layerObj $layerObj, $bForCmdOgr = false, $bFullPath = false)
    {
        $directories = array(
            $mapObj->mappath,
            $mapObj->shapepath,
            $mapObj->mappath . "/" . $mapObj->shapepath,
        );

        $layerName = $layerObj->name;
        switch ($layerObj->connectiontype) {
            case MS_WFS :
                $layerName = $layerObj->getMetaData("wfs_typename") ?: $layerName;
                break;
            case MS_WMS :
                $layerName = $layerObj->getMetaData("wms_name") ?: $layerName;
                break;
        }
        return self::getDataForPhpOGR($directories, $layerName, $layerObj->data, $layerObj->connectiontype, $layerObj->connection, $bForCmdOgr, $bFullPath);
    }

    /**
     * Extract from layerfile, the geometry type and returns its value
     *
     * @param string $layerfile complet path of filename
     */
    protected function _getGeometryType($layerType, $layerData, $layerName, $withDataEncoding = false)
    {
        $tempname = tempnam(sys_get_temp_dir(), "geometrytype_");
        @unlink($tempname);
        $csvfile = $tempname . ".csv";

        if (!($layerData && $layerName)) throw new \Exception("Unavailable parameters for data '{$layerData}' and layer named '{$layerName}'");
        $tempname = $this->container->getParameter("path_image_directory") . "/getGeometryType_" . uniqid();//tempnam(sys_get_temp_dir(), "getGeometryType_".$layerName."_");
        $txtfile = $tempname . ".txt";
        $geometryColumn = "GEOMETRY";
        $cmd = null;
        switch ($layerType) {
            case "VECTOR" :
            case "TABULAIRE" :
                $layerData = '"' . $layerData . '"';
                $cmd = 'ogrinfo -ro -fields=NO -geom=SUMMARY -dialect SQLite -sql "select GEOMETRY from \"' . $layerName . '\" limit 1" ' . $layerData . ' 1> "' . $txtfile . '" 2> ' . $tempname . '.log';
                break;
            case "POSTGIS" :
                $cmd = 'ogrinfo -ro -fields=NO -geom=SUMMARY -dialect SQLite -sql "select GEOMETRY from \"' . $layerName . '\" limit 1" ' . $layerData . ' 1> "' . $txtfile . '" 2> ' . $tempname . '.log';
                break;
            case "WFS" :
                $layerData .= ' "' . $layerName . '"';
                $geometryColumn = "GEOMETRY";

                $cmd = 'ogrinfo -noextent -nocount -so -ro ' . $layerData . ' | grep "Geometry Column"';
                $process = Process::fromShellCommandline($cmd);
                $process->run();
                $result = $process->getOutput();

                //$result = exec($cmd);
                $this->getLogger()->debug(__METHOD__, compact("cmd", "result"));
                $result = explode("=", $result);
                $result = array_map("trim", $result);
                $result = end($result);
                if ($result) $geometryColumn = $result;

                $cmd = 'ogrinfo -ro -fields=NO -geom=SUMMARY -dialect SQLite -sql "select ' . $geometryColumn . ' as GEOMETRY from \"' . $layerName . '\" limit 1" ' . $layerData . ' 1> "' . $txtfile . '" 2> ' . $tempname . '.log';
                break;
        }

        if (!$cmd) {
            return array("layerType" => MS_LAYER_POINT, "msGeometryType" => "POINT");
        }
        $this->getLogger()->debug(__METHOD__, compact("layerType", "cmd", "geometryColumn"));

        //$cmd = 'ogrinfo -ro -fields=NO -geom=SUMMARY -dialect SQLite -sql "select GEOMETRY from \"'.$layerName.'\" limit 1" '.$layerData.' 1> "'.$txtfile.'" 2> '.$tempname.'.log';
        //$this->getLogger()->debug(__METHOD__, array("cmd"=>$cmd));
        $errors = array();
        $return = null;

//        $process = new Process(["rm -f {$tempname}.*"]);
        $process = Process::fromShellCommandline("rm -f {$tempname}.*");
        $process->run();
        $errors = explode(" ", $process->getOutput());
        $return = $process->getExitCode();
        // $result = exec("rm -f {$tempname}.*", $errors, $return);


//        $process = Process::fromShellCommandline($cmd);
        $process = Process::fromShellCommandline($cmd);
        $process->setTimeout(null);
        $process->run();

        $errors = explode(" ", $process->getOutput());
        $return = $process->getExitCode();
        // $result = exec($cmd, $errors, $return);

        $errors = array_merge($errors, array(file_get_contents("{$tempname}.log")));

        $file = fopen($txtfile, "r");
        $start = false;
        $geometrytype = null;
        while ($line = fgets($file)) {
            if (!$start) $start = stripos($line, "OGRFeature") !== false;
            else {
                $geometrytype = preg_replace("!^\s*(\w+)\W.+$!", "$1", $line);
                break;
            }
        }
        fclose($file);
        @unlink($txtfile);
        @unlink("{$tempname}.log");
        if ($geometrytype && stripos($geometrytype, 'LINESTRING') !== false) {
            $results = array("layerType" => MS_LAYER_LINE, "msGeometryType" => "LINE");
        } elseif ($geometrytype && stripos($geometrytype, 'POLYGON') !== false) {
            $results = array("layerType" => MS_LAYER_POLYGON, "msGeometryType" => "POLYGON");
        } elseif ($geometrytype && stripos($geometrytype, 'POINT') !== false) {
            $results = array("layerType" => MS_LAYER_POINT, "msGeometryType" => "POINT");
        } else {
            $results = array("layerType" => MS_LAYER_POINT, "msGeometryType" => "POINT");
        }

        if ($withDataEncoding) {
            $csvfile = $tempname . '.csv';
            $cmd = 'ogr2ogr --config SHAPE_ENCODING "" -f "CSV" "' . $csvfile . '" ' . $layerData . ' 2> ' . $tempname . '.log';
            $errors = array();
            $return = null;
            $process = Process::fromShellCommandline("rm -f {$tempname}.*");
            $process->setTimeout(null);
            $process->run();
            $errors = explode(" ", $process->getOutput());
            $return = $process->getExitCode();
            // $result = exec("rm -f {$tempname}.*", $errors, $return);

            $process = Process::fromShellCommandline($cmd);
            $process->setTimeout(null);
            $process->run();
            $errors = explode(" ", $process->getOutput());
            $return = $process->getExitCode();
            // $result = exec($cmd, $errors, $return);
            $errors = array_merge($errors, array(file_get_contents("{$tempname}.log")));
            $this->getLogger()->debug(__METHOD__, array("action" => "Détection encodage des données", "cmd" => $cmd));

            $cmd = "file -b --mime-encoding {$csvfile}";
            $process = Process::fromShellCommandline($cmd);
            $process->run();
            $errors = explode(" ", $process->getOutput());
            $return = $process->getExitCode();

            //$resultEncoding = exec($cmd, $errors, $return);
            $resultEncoding = trim($resultEncoding, " \n");
            $resultEncoding = strtoupper($resultEncoding);
            $this->getLogger()->debug(__METHOD__, array("RESULT ENCODING initial" => $resultEncoding, "cmd" => $cmd));
            $current_iconv = iconv_get_encoding("output_encoding");
            if (iconv($resultEncoding, $current_iconv, "a") === false) $resultEncoding = null;

            $this->getLogger()->debug(__METHOD__, array("RESULT ENCODING final" => $resultEncoding, "cmd" => $cmd));
            $results["layerDataEncoding"] = $resultEncoding;
        }
        @unlink($logfile);
        @unlink($csvfile);
        return $results;
    }

    /**
     * Extract from layerfields
     *
     * @param string $layerfile complet path of filename
     */
    public function _getLayerFields($layerData, $layerName, $layerSheet = null, $bGetRealEncoding = false)
    {
        $tempname = $this->container->getParameter("path_image_directory") . "/layerfields_" . uniqid();
        $csvfile = $tempname . ".csv";
        $destCsvfile = $tempname . "_UTF8.csv";
        $logfile = $tempname . ".log";

        $toEncoding = "UTF-8";
        $cmd = "export OGR_XLS_HEADERS='FORCE';export OGR_ODS_HEADERS='FORCE';ogrinfo -ro -al -so -nocount -noextent -nomd {$layerData} > \"{$csvfile}\" 2> \"{$logfile}\"";

        $this->getLogger()->debug(__METHOD__, array("cmd" => $cmd));
        $errors = array();
        $return = null;

        $process = Process::fromShellCommandline($cmd);
        $process->run();
        $errors = explode(" ", $process->getOutput());
        $return = $process->getExitCode();


        //$result = exec($cmd, $errors, $return);        
        $errors = array_merge($errors, array(file_get_contents("{$logfile}")));
        $errors = array_map("trim", $errors);
        $errors = array_diff($errors, array(""));
        $errors = array_filter($errors, function ($error) {
            return stripos($error, "Warning") === false;
        });
        if (!empty($errors)) {
            error_log(implode("\n", $errors));
            //throw new \Exception(implode("<br/>", $errors));
        }

        $cmd = "file -b --mime-encoding {$csvfile}";
        $resultEncoding = strtoupper(str_replace("\n", '', `{$cmd}`));//!! execution linux par les `
        $this->getLogger()->debug(__METHOD__, array("RESULT ENCODING " => $resultEncoding, "cmd" => $cmd));
        $process = Process::fromShellCommandline($cmd);
        $process->run();

        $cmd = "iconv -f " . $resultEncoding . " -t " . $toEncoding . " " . $csvfile . " > " . $destCsvfile;
        //$process_args = array("iconv", "-f", $resultEncoding, "-t", $toEncoding, $csvfile, ">", $destCsvfile); 
        $process = Process::fromShellCommandline($cmd);
        $process->run();


        $errors = explode(" ", $process->getOutput());
        $return = $process->getExitCode();

        //$cmd = "iconv -f ".$resultEncoding. " -t ".$toEncoding ." ". $csvfile. " > ".$destCsvfile;
        //$result = exec($cmd, $errors, $return);
        $errors = array_map("trim", $errors);
        $errors = array_diff($errors, array(""));
        if (!empty($errors)) {
            error_log(implode("\n", $errors));
            //throw new \Exception(implode("<br/>", $errors));
        }

        $startfield_pattern = "!^Layer SRS WKT!is";
        $field_pattern = "!^([^:]+)\s*:\s+([^\s:]+)!i";
        $items = array();
        $fieldListStart = false;
        $projectionCount = 0;
        $fidPattern = "/FID Column = /";
        $fid = "";

        if (file_exists($destCsvfile)) {
            $file = fopen($destCsvfile, "r");
            while ($line = fgets($file)) {
                if (preg_match($fidPattern, $line)) {
                    $fid = str_replace("FID Column = ", "", $line);
                }
                if (preg_match($startfield_pattern, $line) || $fieldListStart || $projectionCount > 0) {
                    $fieldListStart = true;
                    $projectionCount += substr_count($line, "[");
                    $projectionCount -= substr_count($line, "]");
                }
                if ($fieldListStart && $projectionCount == 0 && preg_match($field_pattern, $line)) {
                    preg_replace_callback($field_pattern, function ($matches) use (&$items) {
                        $items[$matches[1]] = $matches[2];
                    }, $line);
                }
            }
        }
        @unlink($logfile);
        @unlink($csvfile);
        @unlink($destCsvfile);

        $items_utf8 = array();
        foreach ($items as $field => $type) {
            $encoding = mb_detect_encoding($field);
            $items_utf8[] = array("field_name" => $field, "field_utf8" => utf8_encode(($field)), "field_datatype" => $type);
        }

        // GET REAL ENCODING FOR HEADERS AND DATA
        if ($bGetRealEncoding) {
            $cmd = "ogr2ogr -f CSV \"{$csvfile}\" {$layerData} " . ($layerSheet ? "\"{$layerSheet}\" " : "") . " 2> \"{$tempname}.log\"";

            $errors = array();
            $return = null;
            $process = Process::fromShellCommandline($cmd);
            $process->setTimeout(null);
            $process->run();
            $errors = explode(" ", $process->getOutput());
            $return = $process->getExitCode();
            //$result = exec($cmd, $errors, $return);
            $this->getLogger()->debug(__METHOD__, array("cmd" => $cmd, "csv" => $csvfile, "exists" => file_exists($csvfile)));
            $errors = array_merge($errors, array(file_get_contents("{$tempname}.log")));
            $errors = array_map("trim", $errors);
            $errors = array_diff($errors, array(""));
            $errors = array_filter($errors, function ($error) {
                return stripos($error, "Warning") === false;
            });
            if (!empty($errors)) {
                error_log(implode("\n", $errors));
                //throw new \Exception(implode("<br/>", $errors));
            }
            if (file_exists($csvfile)) {
                $cmd = "file -b --mime-encoding {$csvfile}";
                $this->getLogger()->debug(__METHOD__, array("cmd" => $cmd));
                $resultEncoding = strtoupper(str_replace("\n", '', `{$cmd}`));//!! execution linux par les `
                @unlink($csvfile);
            }
        }
        @unlink($logfile);

        return array("fields" => $items, "fields_encoded" => $items_utf8, "encoding" => $resultEncoding, "fid" => $fid);
    }

    /**
     *
     * @param \mapObj $mapObj mapserver mapObj
     * @param string $mapfilePath full mapfile path
     * @param string $event For exception messages : Event source of the call
     * @return boolean
     * @throws MapserverException
     */
    protected function saveMap(\mapObj $mapObj, $mapfilePath, $event = null, $bFinalSave = true, $bReturnObj = false)
    {

//        dump($this->container->getParameters()); exit();
        $directory = dirname($mapfilePath);

        $message = $this->_t(sprintf("%s : Could not save map file at : %s", $event ?: "Save map", $mapfilePath));
        if (!$mapObj) return false;
        // Updating and saving the new map object
        $tempPath = tempnam($directory, "save_mapfile_");
        @unlink($tempPath);
        $tempPath .= ".map";

        /**
         * 1. Save in a temporary file to validate the new definition
         */
        if (-1 === @$mapObj->save($tempPath)) {
            throw new MapserverException($message);
        } else {
            try {
                /**
                 * 2. Try to reopen temporary file created :
                 * - If the file can be reopened, this is because the new definition is VALID, and so the final mapfile will be created
                 * - If the file cannot be reopened, this is because the new definition is INVALID and so the old mapfile will be restored
                 */
                $mapObj = ms_newMapObj($tempPath);
                @unlink($tempPath);
            } catch (\Exception $exception) {
                /**
                 * Cannot reopen : don't edit the final mapfile
                 */
                //@unlink($tempPath);
                throw new MapserverException($message . " " . $exception->getMessage());
            }
            /**
             * Can reopen : save in final mapfile path
             */
            if ($bFinalSave && -1 === @$mapObj->save($mapfilePath)) {
                $mapObject = $mapObj;
                $mapObj->free();
                unset($mapObj);
                throw new MapserverException($message);
            }
            if (!$bFinalSave || $bReturnObj) {
                $mapObject = $mapObj;
                $mapObj->free();
                unset($mapObj);
                return $mapObject;
            }

        }
        return true;
    }

    /**
     * @Post("/map/floating/{file}")
     *
     * @param string $file mapfile name without extension
     *
     * @RequestParam(name="directory" , description="MAP FILE DIRECTORY", nullable=true, default=ApiController::DEFAULT_DIRECTORY)
     *
     */
    public function floatingPutMap(Request $request, ParamFetcher $paramFetcher, $file)
    {
        //reset mapserver error (to avoid warnings due to precedent errors)
        ms_ResetErrorList();


        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            /*
             * Request values
             */
            $directory = $paramFetcher->get('directory'); // should be absolute path to directory
            $mapfile = $file;
            $mapObj = $this->getMapObj($directory, $file, false);

            $mapData = $this->convertMapObj($mapObj);
            unset($mapData["reference"]);
            $parameters = array_merge($mapData["web"], $mapData, $request->request->all());
            $this->getLogger()->debug(__METHOD__, array($request->request->all(), $mapData, $parameters));

            $request->request->replace($parameters);
            $msData = self::putMapInFile($request->request, $mapObj, $directory, $mapfile);

            $this->saveMap($mapObj, $file, "When edit floating map definition");

            return $this->formatJsonSuccess($msData);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage(), $paramFetcher->all(false));
        }

    }

    /**
     * Put a mapserver layer in an existing mapObj
     * @param ParameterBag $paramFetcher
     * @param \mapObj $mapObj
     * @param string $layer layer index
     */
    protected function putMapInFile(ParameterBag $paramFetcher, \mapObj $mapObj, $directory, $mapfile)
    {
        $name = $paramFetcher->get('name');
        $fontset = $paramFetcher->get('fontset') ?: $this->container->getParameter("default_fontset");
        $symbolset = $paramFetcher->get('symbolset') ?: $this->container->getParameter("default_symbolset");
        $imagecolor = $paramFetcher->get('imagecolor');
        $imagetype = $paramFetcher->get('imagetype');
        //$transparent   = $paramFetcher->get('transparent');
        $extent = $paramFetcher->get('extent');
        $units = $paramFetcher->get('units');
        $projection = $paramFetcher->get('projection');
        $maxscaledenom = $paramFetcher->get('maxscaledenom');
        $minscaledenom = $paramFetcher->get('minscaledenom');
        $metadata = $paramFetcher->get('metadata');
        $template = $paramFetcher->get('template', 'GML');
        $reference = $paramFetcher->get('reference', null);

        $originalMap = ms_newMapObj($directory . "/" . $mapfile . ".map");
        /*
         * Computed/Transformed values
         */
        if (!is_array($extent) && preg_match("!^[\d.+e-]+ [\d.+e-]+ [\d.+e-]+ [\d.+e-]+$!i", $extent)) {
            $extent = explode(" ", $extent);
        }

        // UNITS : get a mapscript constant (in Map units category) or MS_FALSE if not exists
        if (defined(strtoupper("MS_" . $units)))
            $units = constant(strtoupper("MS_" . $units));
        else {
            $units = MS_FALSE;
        }

        // METADATA : get an array(key=>value) by json_decode
        if (!is_array($metadata)) {
            $metadata = json_decode($metadata, true) ?: array();
        }
        $projection = preg_replace("!.+(epsg)!", "$1", $projection);

        // IMAGECOLOR : get an array(red, green, blue) of RGB colors
        $imagecolor = $this->getRGB($imagecolor);

        $this->callSet($mapObj, "name", $name);
        $this->callSet($mapObj, "units", $units);

        $this->callSet($mapObj->web, "maxscaledenom", $maxscaledenom, sprintf($this->_t(self::DEFAULTMSG_CALLSET), "mapObj->web", "%s"));
        $this->callSet($mapObj->web, "minscaledenom", $minscaledenom, sprintf($this->_t(self::DEFAULTMSG_CALLSET), "mapObj->web", "%s"));
        $this->callSet($mapObj->web, "template", $template, sprintf($this->_t(self::DEFAULTMSG_CALLSET), "mapObj->web", "%s"));

        foreach ($metadata as $key => $value) {
            $this->callSet($mapObj->web->metadata, $key, $value, sprintf($this->_t(self::DEFAULTMSG_CALLSET), "mapObj->web->metadata", "%s"));
        }

        // special for arrays :
        $this->callFunction($mapObj, "setFontSet", array($fontset));
        $this->callFunction($mapObj, "setSymbolSet", array($symbolset));
        $this->callFunction($mapObj, "setProjection", array($projection));

        //report missing (in map file) symbols
        for ($iSymbol = 0; $iSymbol < $originalMap->getNumSymbols(); $iSymbol++) {
            $originalSymbol = $originalMap->getSymbolObjectById($iSymbol);
            if (($newSymbol = $mapObj->getSymbolByName($originalSymbol->name)) != $iSymbol) {
                $newSymbol = ms_newSymbolObj($mapObj, $originalSymbol->name);
                $newSymbol = $mapObj->getSymbolObjectById($newSymbol);

                $fields = array("type", "character", "filled", "font", "sizex", "sizey", "transparent", "transparentcolor", "inmapfile");
                foreach ($fields as $field) {
                    $newSymbol->set($field, $originalSymbol->$field);
                }
                $newSymbol->setImagePath($originalSymbol->imagepath);
                $originalSymbol->numpoints > 0 && $newSymbol->setPoints($originalSymbol->getPointsArray());
            }
        }

        $this->callFunction($mapObj->imagecolor, "setRGB", $imagecolor, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "mapObj->imagecolor", "%s"));
        $this->callFunction($mapObj->outputformat, "setOption", array(
            "OUTPUT_TYPE",
            $imagetype
        ), sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "mapObj->outputformat", "%s"));

        //$this->callSet($mapObj->getOutputFormat(0), "transparent", ($transparent=="true" ? MS_TRUE : MS_FALSE), sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION ), "mapObj->outputformat", "%s"));

        $this->callFunction($mapObj->extent, "setExtent", $extent, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "mapObj->extent", "%s"));


        $msData = array();
        // reference map, if any
        if ($reference) {
            $reference_mapfile = "reference_" . $mapfile . ".map";
            @unlink($directory . "/" . $reference_mapfile);
            if ($this->saveMap($mapObj, $directory . "/" . $reference_mapfile, "before save referenceMap")) {
                $reference = (!is_array($reference) ? json_decode($reference, true) : $reference) ?: array();
                $msData = $this->putReferenceInMap(new ParameterBag(array_merge(array('directory' => $directory), $reference)), $mapObj, basename($reference_mapfile));
            }
            @unlink($directory . "/" . $reference_mapfile);
        }

        return $msData;
    }

    /**
     * Catch the mapserver exception in symbolset loading on mapObj
     * @param \mapObj $mapObj
     * @param string $symbolset
     * @throws \Exception
     */
    function setSymbolSet(\mapObj $mapObj, $symbolset)
    {
        try {
            if (@$mapObj->setSymbolSet($symbolset) === MS_FAILURE)
                throw new \Exception("Impossible de charger le fichier symbole " . str_replace($mapObj->mappath, "", $symbolset) . "");
        } catch (\Exception $exception) {
            throw new \Exception("Impossible de charger le fichier symbole " . str_replace($mapObj->mappath, "", $symbolset) . "");
        }
    }

    /**
     * Put a mapserver layer in an existing mapObj
     * @param ParameterBag $paramFetcher
     * @param \mapObj $mapObj
     * @param string $layer layer index
     */
    protected function putLayerInMap(ParameterBag $paramFetcher, \mapObj $mapObj, $layer)
    {
        $this->getLogger()->debug(__METHOD__, $paramFetcher->all());
        $name = $paramFetcher->get('name');
        $type = $paramFetcher->get('type');
        $projection = $paramFetcher->get('projection');
        $status = $paramFetcher->get('status', "ON");
        $sizeunits = $paramFetcher->get('sizeunits');
        $connectiontype = $paramFetcher->get('connectiontype', "INLINE");
        $connection = $paramFetcher->get('connection');
        $data = $paramFetcher->get('data');
        $tileindex = $paramFetcher->get('tileindex');
        $tileitem = $paramFetcher->get('tileitem');
        $maxscaledenom = $paramFetcher->get('maxscaledenom');
        $minscaledenom = $paramFetcher->get('minscaledenom');
        $offsite = $paramFetcher->get('offsite');
        $processing = $paramFetcher->get('processing');
        $symbolscaledenom = $paramFetcher->get('symbolscaledenom');
        $symbolscaledenompixels = $paramFetcher->get('symbolscaledenompixels', null);
        $tolerance = $paramFetcher->get('tolerance');
        $template = $paramFetcher->get('template');
        $toleranceunits = $paramFetcher->get('toleranceunits', MS_PIXELS);
        $classitem = utf8_decode($paramFetcher->get('classitem'));
        $this->getLogger()->debug(__METHOD__, array("classitem" => $classitem));
        $labelitem = utf8_decode($paramFetcher->get('labelitem'));
        $metadata = $paramFetcher->get('metadata');
        $encoding = $paramFetcher->get('encoding', null);

        // 2. Creation/recuperation of a layer
        $bNewLayer = false;
        try {
            $this->getLayerObj($mapObj, $layer, true);
        } catch (\Exception $ex) {
            $bNewLayer = true;
        }
        $layerObj = $this->getLayerObj($mapObj, $layer, false);

        /**Create a copy with no properties sets but with subobjects */
        if (!$bNewLayer) {
            $index = $layerObj->index;
            $nIndex = $mapObj->numlayers;

            $nLayerObj = ms_newLayerObj($mapObj);
            $nLayerObj->updateFromString("LAYER
                                                TYPE POINT
                                                METADATA
                                                END # METADATA
                                                VALIDATION
                                                'labelrequires' '[01]'
                                                END
                                                END # LAYER");
            for ($i = 0; $i < $layerObj->numclasses; $i++) {
                ms_newClassObj($nLayerObj, $layerObj->getClass($i));
            }
            $mapObj->removeLayer($index);
            while ($nIndex != $index + 1) {
                $mapObj->moveLayerUp($nIndex--);
            }
            $layerObj = $nLayerObj;
        }
        // Data writing in the layer object using functions we created

        // // Name (string)
        $this->callSet($layerObj, "name", $name);

        // // Type (string that must match a constant name)
        $this->callSetConstant($layerObj, "type", $type, "LAYER");

        // // Projection (string = some "PROJ.4" strings separated with "," to then create a projectionObj)
        //$projection = "epsg:".str_ireplace("epsg:", "", $projection);
        $layerObj->setProjection($projection);

        // // Status (string that must match a constant name, with or without the "MS_" at the beginning))
        $this->callSetConstant($layerObj, "status", $status);

        // // Sizeunits (string that must match a constant name)
        $this->callSetConstant($layerObj, "sizeunits", $sizeunits);

        // // Connection (string)
        $this->callSet($layerObj, "connection", $connection);

        // // Data (string)
        $this->callSet($layerObj, "data", $data);

        if ($encoding) {
            $layerObj->updateFromString('LAYER ENCODING "' . $encoding . '" END');
        }

        // // Tile index (string = filename)
        if (!empty($tileindex))
            $this->callSet($layerObj, "tileindex", $tileindex);
        if (!empty($tileitem))
            $this->callSet($layerObj, "tileitem", $tileitem);

        // // Max scale denom (decimal)
        $this->callSetNumeric($layerObj, "maxscaledenom", $maxscaledenom);

        // // Min scale denom (decimal)
        $this->callSetNumeric($layerObj, "minscaledenom", $minscaledenom);

        // // Offsite (hex color -> colorObj)
        $offsite = $this->getRGB($offsite);
        $this->callFunction($layerObj->offsite, "setRGB", $offsite, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "layerObj->offsite", "%s"));

        // // Processing (string)
        $layerObj->clearProcessing();
        if (isset($processing) && !empty($processing)) {
            foreach ($processing as $process) {
                if (is_array($process)) {
                    $process = implode("=", $process);
                }
                $this->callFunction($layerObj, "setProcessing", array(utf8_decode($process)));
            }
        }
        //SPECIFIC PRODIGE FAST CGI

        $this->callFunction($layerObj, "setProcessing", array("CLOSE_CONNECTION=DEFER"));

        // // Symbol scale denom (decimal)
        if ($symbolscaledenompixels && !$symbolscaledenom) {
            $extent = $mapObj->extent;
            $scale = ($extent->maxx - $extent->minx) / ($mapObj->width * 0.0254 / 72);
            $symbolscaledenom = $scale * $symbolscaledenompixels;
        }
        $this->callSetNumeric($layerObj, "symbolscaledenom", $symbolscaledenom);


        // Default Tolerance Specified
        if ($tolerance === null) {
            if ($layerObj->type == MS_LAYER_POLYGON) {
                $tolerance = 0;
            } else {
                $tolerance = 5;
            }

        }
        // Default ToleranceUnits Specified
        if ($toleranceunits === null) {
            $toleranceunits = MS_PIXELS;
        }

        // Default Template
        if ($template === null) {
            $template = 'GML';
        }


        // // Tolerance (decimal)
        $this->callSetNumeric($layerObj, "tolerance", $tolerance);

        // // Template (string)
        $this->callSet($layerObj, "template", $template);

        // // Tolerance units (integer)
        $this->callSetNumeric($layerObj, "toleranceunits", $toleranceunits);

        // // Class item (string)
        $classitem = (empty($classitem) ? null : $classitem);
        $this->callSetNotRequired($layerObj, "classitem", $classitem);

        // // Label Item (string)
        $labelitem = (empty($labelitem) ? null : $labelitem);
        if (is_null($labelitem)) {
            for ($i = 0; $i < $layerObj->numclasses; $i++) {
                $classObj = $layerObj->getClass($i);
                while ($classObj->numlabels > 0) $classObj->removeLabel(0);
            }
        } else {
            $this->callSet($layerObj, "labelitem", $labelitem, "", MS_SUCCESS, false);
        }

        // // Metadata for the web (Json -> hashTableObj)
        if (!is_array($metadata)) {
            $metadata = json_decode($metadata, true) ?: array();
        }
        foreach ($metadata as $key => $value) {
            $this->callSet($layerObj->metadata, $key, $value, sprintf($this->_t(self::DEFAULTMSG_CALLSET), "layerObj->metadata", "%s"));
        }
        $layerObj->setMetadata('LAST_DEFINITION', date('Y-m-d H:i:s'));

        // // Default Metadata Specified
        /* en attente de précisions sur ce point
        $layerObj->setMetaData("wfs_namespace_uri", "http://www.opengis.net/wfs");
        $layerObj->setMetaData("wfs_namespace_prefix", "wfs");
            
        if ( in_array($layerObj->connectiontype, array(MS_WMS, MS_WFS)) ){
            $prefix = ($layerObj->connectiontype==MS_WMS ? "wms" : "wfs");
              
            $layerObj->setMetaData($prefix."_bbox_extended",    "true");
            $layerObj->setMetaData($prefix."_inspire_capabilities", "url");
            $layerObj->setMetaData($prefix."_accessconstraints","Aucun obstacle technique");
            $layerObj->setMetaData($prefix."_addresstype",      "postal");
            $layerObj->setMetaData($prefix."_identifier_authority", "");
            $layerObj->setMetaData($prefix."_identifier_value", "[identifiant]");
            $layerObj->setMetaData($prefix."_metadataurl_format",   "text/html");
            $layerObj->setMetaData($prefix."_metadataurl_type", "TC211");
            $layerObj->setMetaData($prefix."_inspire_capabilities", "url");
 //"wfs_featureid"
        }*/

        // // Connection Type (constant)
        $connectiontype_constantname = "MS_" . strtoupper($connectiontype);
        if (defined($connectiontype_constantname)) {
            $connectiontype_value = constant($connectiontype_constantname);
            $layerObj->setConnectionType($connectiontype_value);
        } else {
            throw new MapserverException($this->_t(sprintf("Could not add layer because '%s' is not an existent connection type", $connectiontype_constantname)));
        }

        /*Save classes definition if they exist*/
        $request = $this->getRequest();
        $save = $request->request->all();
        $classes = $request->request->get('classes', array($request->request->get('classe', null)));
        $this->getLogger()->debug(__METHOD__, $classes);
        if (array_key_exists(0, $classes) && !isset($classes[0])) $classes = array();
        if ($classes && !empty($classes)) {
            $classes = (array)$classes;
            foreach ($classes as $classIndex => $classDefinition) {
                $request->request->replace($classDefinition);
                $this->putClassInLayer($request->request, $mapObj, $layerObj, $classIndex);
            }
            $this->getLogger()->debug("removeClass " . $layerObj->numclasses . " " . count($classes));
            while ($layerObj->numclasses > count($classes)) {
                $this->getLogger()->debug("removeClass " . $layerObj->numclasses . " " . count($classes));
                $layerObj->removeClass($layerObj->numclasses - 1);
            }
        }

        if ($layerObj->numclasses == 0 && !in_array($layerObj->type, array(MS_LAYER_RASTER))) {
            $classDefinition = array(
                "directory" => $mapObj->mappath,
                "name" => $name,
                "title" => $name,
                "styles" => array(
                    "color" => "#000000",
                    "size" => 8,
                    "width" => 8,
                    "symbol" => ($layerObj->type == MS_LAYER_POINT ? "cercle" : ($layerObj->type == MS_LAYER_LINE ? null : "carre"))
                )
            );
            $request->request->replace($classDefinition);
            $this->putClassInLayer($request->request, $mapObj, $layerObj, 0);
        }
        $request->request->replace($save);
    }

    /**
     * Put a mapserver class in an existing layerObj
     *
     * @param ParameterBag $paramFetcher
     * @param \layerObj $layerObj
     * @param string $class class index
     */
    protected function putClassInLayer(ParameterBag $paramFetcher, \mapObj $mapObj, \layerObj $layerObj, $class)
    {
        $this->getLogger()->debug(__METHOD__, $paramFetcher->all());
        $expression = $paramFetcher->get('expression');
        $name = $paramFetcher->get('name');
        $title = $paramFetcher->get('title');
        $status = $paramFetcher->get('status', "ON");

        // 3. Initialization/creation of the class
        $classObj = $this->getClassObj($mapObj, $layerObj, $class, false);

        /**Create a copy with no properties sets but with subobjects */
        $nClass = $layerObj->numclasses;
        $nClassObj = ms_newClassObj($layerObj);
        for ($i = 0; $i < $classObj->numstyles; $i++) {
            ms_newStyleObj($nClassObj, $classObj->getStyle($i));
        }
        while ($nClass != $class) {
            $layerObj->moveclassup($nClass--);
        }
        $layerObj->removeClass($class + 1);
        $classObj = $nClassObj;


        // Data writing in the layer object using functions we created

        // // Name (string)
        $this->callSetNotRequired($classObj, "name", $name);
        $this->callSetNotRequired($classObj, "title", $title);


        if ($layerObj->type !== MS_LAYER_RASTER) {
            $classObj->setExpression($expression);
            // verif 
            if ($classObj->getExpressionString() !== $expression) {
                if (strpos($expression, "'") > 0 || strpos($expression, '"') > 0) {
                    $classObj->setExpression(strval($expression));
                } else {
                    $classObj->setExpression("'" . $expression . "'");
                }
            }
        }

        // // Status (string that must match a constant name, with or without the "MS_" at the beginning))
        $this->callSetConstant($classObj, "status", $status, "");

        $request = $this->getRequest();
        $save = $request->request->all();


        /*Save labels definition if they exist*/
        $labels = $request->request->get('labels', array($request->request->get('label', null)));
        if (!empty($labels) && !array_key_exists(0, $labels)) $labels = array($labels);
        if (array_key_exists(0, $labels) && !isset($labels[0])) $labels = array();
        if ($labels && !empty($labels) && $layerObj->labelitem) {
            $labels = (array)$labels;
            $save = $request->request->all();
            foreach ($labels as $labelIndex => $labelDefinition) {
                $request->request->replace($labelDefinition);
                $this->putLabelInClass($request->request, $mapObj, $layerObj, $classObj, $labelIndex);
            }
            $request->request->replace($save);
        }

        /*Save styles definition if they exist*/
        $styles = $request->request->get('styles', array($request->request->get('style', null)));
        if (!empty($styles) && !array_key_exists(0, $styles)) $styles = array($styles);
        if (array_key_exists(0, $styles) && !isset($styles[0])) $styles = array();

        if ($styles && !empty($styles)) {
            $styles = (array)$styles;
            while ($classObj->numstyles > 0) {
                $classObj->deleteStyle(0);
            }
            foreach ($styles as $styleIndex => $styleDefinition) {
                $request->request->replace($styleDefinition);
                $this->getStyleObj($mapObj, $layerObj, $classObj, $styleIndex, false);
                $this->putStyleInClass($request->request, $mapObj, $layerObj, $classObj, $styleIndex);
            }
            $request->request->replace($save);
        }
    }

    /**
     * Put a mapserver class style in an existing classObj
     * @param ParameterBag $paramFetcher
     * @param \classObj $classObj
     * @param string $style style index
     */
    protected function putLabelInClass(ParameterBag $paramFetcher, \mapObj $mapObj, \layerObj $layerObj, \classObj $classObj, $label)
    {
        $this->getLogger()->debug(__METHOD__, $paramFetcher->all());
        $angle = $paramFetcher->get('angle');
        $color = $paramFetcher->get('color');
        $geomtransform = $paramFetcher->get('geomtransform');
        $font = $paramFetcher->get('font');
        $force = $paramFetcher->get('force');
        $maxscaledenom = $paramFetcher->get('maxscaledenom');
        $minscaledenom = $paramFetcher->get('minscaledenom');
        $offsetx = $paramFetcher->get('offsetx');
        $offsety = $paramFetcher->get('offsety');
        $position = $paramFetcher->get('position');
        $shadowcolor = $paramFetcher->get('shadowcolor');
        $shadowsizex = $paramFetcher->get('shadowsizex');
        $shadowsizey = $paramFetcher->get('shadowsizey');
        $size = $paramFetcher->get('size');
        $encoding = $paramFetcher->get('encoding');

        $labelObj = $this->getLabelObj($mapObj, $layerObj, $classObj, $label, false);
        while ($labelObj->numstyles) {
            $labelObj->deleteStyle(0);
        }

        // Data writing in the label object using functions we created

        // // Angle (decimal)
        $this->getLogger()->debug(__METHOD__, array("angle" => $angle));
        if (!empty($angle)) {
            $this->callSetNumericOrFieldName('MS_LABEL_BINDING', $labelObj, "angle", $angle);
        }

        // // Color (hex color -> colorObj)
        $color = $this->getRGB($color);
        $this->callFunction($labelObj->color, "setRGB", $color, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "labelObj->color", "%s"));

        // // Geomtransform (string)
        if ($geomtransform) {
            $styleObj = null;
            $this->getLogger()->debug(__METHOD__, array("STYLE " . $labelObj->numstyles));
            for ($i = 0; $i < $labelObj->numstyles; $i++) {
                $_style = $labelObj->getStyle($i);
                $this->getLogger()->debug(__METHOD__, array("STYLE", $_style->getGeomTransform()));
                if ($_style->getGeomTransform() == $geomtransform) {
                    $styleObj = $_style;
                    break;
                }
            }
            if (!$styleObj) {
                $this->getLogger()->debug(__METHOD__, array("STYLE create"));
                $labelObj->updateFromString(implode(" ", array(
                    "LABEL",
                    "STYLE",
                    "GEOMTRANSFORM '{$geomtransform}'",
                    "SIZE 5",
                    "SYMBOL 'CARRE'",
                    "OFFSET 0 10",
                    "END",
                    "END")));
                $styleObj = $labelObj->getStyle(0);
            }
        }


        // // Font (string = name of the font as defined in the fontset)
        $this->callSet($labelObj, "font", pathinfo($font, PATHINFO_FILENAME));

        // // Force (boolean)
        $this->callSetConstant($labelObj, "force", (is_numeric($force) ? ($force ? "ON" : "OFF") : $force));

        // // Encoding
        $this->callSet($labelObj, "encoding", $encoding);

        // // Max scale denom (decimal)
        $this->callSetNumeric($labelObj, "maxscaledenom", $maxscaledenom);

        // // Max scale denom (decimal)
        $this->callSetNumeric($labelObj, "maxscaledenom", $maxscaledenom);

        // // Min scale denom (decimal)
        $this->callSetNumeric($labelObj, "minscaledenom", $minscaledenom);

        // // Offset (one string of two integers separated by a space-> two integers)
        $this->callSetNumeric($labelObj, "offsetx", $offsetx);
        $this->callSetNumeric($labelObj, "offsety", $offsety);

        // // Position (integer)
        $this->getLogger()->debug(__METHOD__, array("position" => $position));
        $this->callSetConstant($labelObj, "position", $position ?: "AUTO");


        // // Size (integer or a string key word amongst : MS_TINY , MS_SMALL, MS_MEDIUM, MS_LARGE, MS_GIANT)
        if (is_numeric($size)) {
            $this->callSetNumeric($labelObj, "size", $size);
        } else if (!empty($size)) {
            $this->callSetConstant($labelObj, "size", $size);
        }

        if (!empty($shadowcolor)) {
            $shadowcolor = $this->getRGB($shadowcolor);
            $labelObj->updateFromString(implode(" ", array(
                "LABEL",
                ($labelObj->numstyles ? "STYLE END" : ""),
                "STYLE",
                "GEOMTRANSFORM 'labelpoly'",
                "OFFSET {$shadowsizex} {$shadowsizey}",
                "END",
                "STYLE",
                "GEOMTRANSFORM 'labelpoly'",
                "OPACITY 20",
                "OFFSET 0 0",
                "END",
                "END")));

            $styleObj = $labelObj->getStyle($labelObj->numstyles - 2);
            $this->callFunction($styleObj->color, "setRGB", $shadowcolor, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "labelObj->styleObj->color", "%s"));

            $styleObj = $labelObj->getStyle($labelObj->numstyles - 1);
            $color = array(($color[0] ^ 255), ($color[1] ^ 255), ($color[2] ^ 255));
            //$color = array(255,255,255);
            $this->getLogger()->info(__METHOD__, $color);
            $this->callFunction($styleObj->color, "setRGB", $color, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "labelObj->styleObj->color", "%s"));

// 	        // // Shadowsize (one string of two integers separated by a space-> two integers)
// 	        $this->callSetNumeric($labelObj, "shadowsizex", $shadowsizex);
// 	        $this->callSetNumeric($labelObj, "shadowsizey", $shadowsizey);
// 	        // // Shadowshadowcolor (hex shadowcolor -> shadowcolorObj)
// 	        $this->callFunction($styleObj->shadowcolor, "setRGB", $shadowcolor, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION ), "labelObj->shadowcolor", "%s"));
        }

    }

    /**
     * Put a mapserver class style in an existing classObj
     * @param ParameterBag $paramFetcher
     * @param \classObj $classObj
     * @param string $style style index
     */
    protected function putStyleInClass(ParameterBag $paramFetcher, \mapObj $mapObj, \layerObj $layerObj, \classObj $classObj, $style)
    {
        $this->getLogger()->debug(__METHOD__, $paramFetcher->all());
        $color = $paramFetcher->get('color');
        $geomtransform = $paramFetcher->get('geomtransform');
        $maxsize = $paramFetcher->get('maxsize');
        $minsize = $paramFetcher->get('minsize');
        $outlinecolor = $paramFetcher->get('outlinecolor');
        $outlinewidth = $paramFetcher->get('outlinewidth');
        $size = $paramFetcher->get('size');
        $angle = $paramFetcher->get('angle');
        $symbol = $paramFetcher->get('symbol');
        $pattern = $paramFetcher->get('pattern');
        $width = $paramFetcher->get('width');

        // / 4. Creation/recuperation of a style from its index
        while ($classObj->numstyles) {
            $classObj->removeStyle($classObj->numstyles - 1);
        }
        $styleObj = ms_newStyleObj($classObj);

        // // Symbol (integer)
        if (!is_numeric($symbol) && $symbol == self::HATCHSYMBOL) {
            if ($mapObj) {
                $nId = ms_newSymbolObj($mapObj, $symbol);
                $oSymbol = $mapObj->getSymbolObjectById($nId);
                $oSymbol->set("type", MS_SYMBOL_HATCH);
                $oSymbol->set("inmapfile", MS_TRUE);
            }
        }

        // // Symbol (integer)
        if (is_numeric($symbol)) {
            $this->callSetNotRequired($styleObj, "symbol", intval($symbol));
        } else {
            $this->callSetNotRequired($styleObj, "symbolname", $symbol);
        }


        // Data writing in the label object using functions we created
        $geomtransform && $styleObj->setGeomtransform($geomtransform);

        // // Color (hex color -> colorObj)
        $color = $this->getRGB($color);
        $this->callFunction($styleObj->color, "setRGB", $color, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "styleObj->color", "%s"));

        // // Geomtransform (string)
        //Provoque une erreur si vide et non utile : $styleObj->setGeomTransform($geomtransform);

        // // angle (decimal)
        $this->callSetNumeric($styleObj, "angle", $angle);

        // // Max size (decimal)
        $this->callSetNumeric($styleObj, "maxsize", $maxsize);

        // // Min size (decimal)
        $this->callSetNumeric($styleObj, "minsize", $minsize);


        // // Size (decimal)
        $this->callSetNumericOrFieldName('MS_STYLE_BINDING', $styleObj, "size", $size, MS_SUCCESS, false);

        if ($pattern) {
            //$pattern = explode(" ", trim($pattern));
            //$styleObj->setPattern($pattern);
            // since php/Mapscript has a bug with setPattern in this version
            $strPattern = 'STYLE
                          PATTERN 
                          ' . $pattern . '
                          END
                          END';
            $styleObj->updateFromString($strPattern);

        }

        // // Width (decimal)
        //when set to 1, with not put in mapfile (bug Mapserver???)
        //
        $this->callSetNumeric($styleObj, "width", $width ?: 1);


        if ($outlinecolor && $outlinewidth) {
            $styleObj2 = $this->getStyleObj($mapObj, $layerObj, $classObj, $style + 1, false);
            $geomtransform && $styleObj2->setGeomtransform($geomtransform);
            $this->callSetNumericOrFieldName('MS_STYLE_BINDING', $styleObj2, "size", $size, MS_SUCCESS, false);
            // // Outline color (hex color -> colorObj)
            $outlinecolor = $this->getRGB($outlinecolor);
            $this->callFunction($styleObj2->outlinecolor, "setRGB", $outlinecolor, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "styleObj->outlinecolor", "%s"));

            // // Outlinewidth (one string of two decimals separated by a space-> two decimals)
            //TODO         $this->callSetSeveralValuesFromString($styleObj, array("maxwidth", "minwidth"), $outlinewidth);
            $this->callSetNumeric($styleObj2, "width", $outlinewidth);
        }
        if (preg_replace("!\s!", "", $styleObj->convertToString()) == "STYLEEND#STYLE") {
            $classObj->removeStyle($style);
        }
        $this->getLogger()->debug(__METHOD__, array(preg_replace("!\s!", "", $styleObj->convertToString())));
    }

    /**
     * Put a mapserver class style in an existing classObj
     * @param ParameterBag $paramFetcher
     * @param \classObj $classObj
     * @param string $style style index
     */
    protected function putReferenceInMap(ParameterBag $paramFetcher, \mapObj $mapObj, $mapfile)
    {
        $this->getLogger()->debug(__METHOD__, $paramFetcher->all());
        $directory = $paramFetcher->get('directory');
        $color = $paramFetcher->get('color');
        $extent = $paramFetcher->get('extent');
        $image = $paramFetcher->get('image');
        $outlinecolor = $paramFetcher->get('outlinecolor');
        $size = $paramFetcher->get('size');
        $status = $paramFetcher->get('status', 'ON');

        // 2. Creation/recuperation of a referenceMapObj
        try {
            $referenceMapObj = $mapObj->reference;
        } catch (\Exception $e) {
            throw new MapserverException($this->_t(sprintf("An error occured when trying to edit the referenceMapObj in the map file : '%s'.", $file)));
        }
        $this->callSetConstant($referenceMapObj, "status", $status);

        // Data writing in the layer object using functions we created

        // // Color (hex color -> colorObj)
        $color = $this->getRGB($color);
        $this->callFunction($referenceMapObj->color, "setRGB", $color, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "referenceMapObj->color", "%s"));

        // // Extent (one string of 4 decimal values separated by a space -> 4 decimal values -> rectObj)
        $extent_array = (is_array($extent) ? array_values($extent) : explode(" ", $extent));

        $tempfile = uniqid('tmp_') . $mapfile;
        $mapReferenceObj = $this->saveMap($mapObj, $directory . '/' . $tempfile, 'reference map generation', true, true);
        file_exists($directory . '/' . $tempfile) && unlink($directory . '/' . $tempfile);
        if (count($extent_array) == 4) {
            $extent_minx = $extent_array [0];
            $extent_miny = $extent_array [1];
            $extent_maxx = $extent_array [2];
            $extent_maxy = $extent_array [3];
            if (is_numeric($extent_minx) && is_numeric($extent_miny) && is_numeric($extent_maxx) && is_numeric($extent_maxy)) {
                $mapReferenceObj->extent->setExtent($extent_minx, $extent_miny, $extent_maxx, $extent_maxy); // creates itself a rectObj object
            } else {
                throw new MapserverException($this->_t(sprintf("Could not add reference because at least one of four values of extent is not a numeric value.")));
            }
        } else {
            throw new MapserverException($this->_t(sprintf("Could not add reference because a problem occured when parsing extent string value.")));
        }

        // // Outlinecolor (hex color -> colorObj)
        $outlinecolor = $this->getRGB($outlinecolor);
        $this->callFunction($referenceMapObj->outlinecolor, "setRGB", $outlinecolor, sprintf($this->_t(self::DEFAULTMSG_CALLFUNCTION), "referenceMapObj->outlinecolor", "%s"));

        // // Size (one string of two integers separated by a space -> ??)
        $size_array = explode(" ", $size);
        if (count($size_array) == 2) {
            $size_x = $size_array [0] ?: 200;
            $size_y = $size_array [1] ?: 200;

            if (is_numeric($size_x) && is_numeric($size_y)) {
                $mapReferenceObj->set("width", $size_x);
                $mapReferenceObj->set("height", $size_y);
                //echo "Size not added : '" . $size_x . " " . $size_y . "'";
                // TODO1
                // $referenceMapObj->setSize($size_x, $size_y);// does not work ; setSize() does not exist for referenceMapObjs
                // $mapObj->reference->setSize($size_x,$size_y); // does not work ; setSize() does not exist for referenceMapObjs
                // $this->callSet($referenceMapObj, "size", $size); // does not work ; property "size" does not exist for referenceMapObjs
                // $this->callSet($mapObj->reference, "size", $size); // does not work ; property "size" does not exist for referenceMapObjs
                // $this->callSet($referenceMapObj, "sizex", $size_x); // does not work ; property "sizex" does not exist for referenceMapObjs
                // $referenceMapObj->setSizex($size_x);// does not work ; setSizex() does not exist for referenceMapObjs

                // TODO needs to be tested...
                // // Image (string = filepath)
                if (null !== $image && strlen($image) > 0) {
                    $new_image = $directory . $image;
                    if (file_exists($directory . $image)) {
                        $new_image = str_replace(pathinfo($directory . $image, PATHINFO_FILENAME), str_replace(".map", "", $mapfile), $image);
                        if ($new_image != $image) @copy($directory . $image, $directory . $new_image);
                    }
                    $this->callSet($referenceMapObj, "image", $new_image);
                } else {
                    for ($iLayer = 0; $iLayer < $mapReferenceObj->numlayers; $iLayer++) {
                        $oLayer = $mapReferenceObj->getLayer($iLayer);
                        try {
                            $oLayer->open();
                            $oLayer->close();
                        } catch (\Exception $exception) {
                            $mapReferenceObj->removeLayer($oLayer->index);
                            continue;
                        }

                        if ($oLayer->isVisible()) {
                            $oLayer->set('maxscaledenom', null);
                            $oLayer->set('minscaledenom', null);
                        } else {
                            $mapReferenceObj->removeLayer($oLayer->index);
                        }
                    }
                    $mapReferenceObj = $this->saveMap($mapReferenceObj, $directory . '/' . $tempfile, 'reference map generation', true, true);
                    // generate and save image on disk
                    $ext = $mapReferenceObj->outputformat->extension;

                    $filename = $directory . '/reference/reference_' . (microtime(true) * 10000) . '.' . $ext;
                    $this->getLogger()->info('IMAGE FILENAME:' . $filename, array($size_x, $size_y));

                    list($imageObj, $extent) = array_values($this->getReferenceImageObj($directory, $tempfile, $extent_array, array($size_x, $size_y)));
                    $imageObj && $imageObj->saveImage($filename);

                    @unlink($directory . '/' . $tempfile);

                    $size = getimagesize($filename);
                    $relativePath = str_replace($directory, '.', $filename);
                    $this->callSet($referenceMapObj, 'image', $relativePath);
                }

                $this->callSet($referenceMapObj, "width", $size_x);
                $this->callSet($referenceMapObj, "height", $size_y);
                $this->callFunction($referenceMapObj->extent, "setExtent", $extent_array);//array($mapReferenceObj->extent->minx, $mapReferenceObj->extent->miny, $mapReferenceObj->extent->maxx, $mapReferenceObj->extent->maxy,)); // creates itself a rectObj object
                $this->callSet($referenceMapObj, "status", MS_ON);
                $mapReferenceObj->free();
                unset($mapReferenceObj);
            } else {
                throw new MapserverException($this->_t(sprintf("Could not add reference because at least one of two values of size is not a numeric value.")));
            }
        } else {
            throw new MapserverException($this->_t(sprintf("Could not add reference because a problem occured when parsing size string value.")));
        }


        // always return reference map
        return $this->convertReferenceMapObj($mapObj, $mapObj->reference);
    }


    /**
     *
     * @param \mapObj $mapObj
     * @param array|string|null $extent
     * @param array|string|null $size
     * @return \imageObj|null
     */
    public function getReferenceImageObj($directory, $mapfile, $extent, $size)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $mapObj = $this->getMapObj($directory, $mapfile);
            // set reference map extent
            if (is_string($extent) && ($extent = urldecode($extent)) && preg_match("!^[\d.+e-]+ [\d.+e-]+ [\d.+e-]+ [\d.+e-]+$!i", $extent)) {
                $extent = explode(' ', $extent);
            }

            // set reference map size
            if (is_string($size) && ($size = urldecode($size)) && preg_match("!^\d+ \d+$!i", $size)) {
                $size = explode(' ', $size);
                $size = array_map("intval", $size);
            }

            if (is_array($extent) && count($extent) == 4) {
                $i = 0;
                $mapObj->extent->set("minx", $extent[$i++]);
                $mapObj->extent->set("miny", $extent[$i++]);
                $mapObj->extent->set("maxx", $extent[$i++]);
                $mapObj->extent->set("maxy", $extent[$i++]);
            }

            if (is_array($size) && count($size) == 2) {
                $mapObj->set("width", $size[0]);
                $mapObj->set("height", $size[1]);
            }
            //$mapObj->save($directory."reference.map");
            return array("image" => $mapObj->draw(), "extent" => array($mapObj->extent->minx, $mapObj->extent->miny, $mapObj->extent->maxx, $mapObj->extent->maxy,));

        } catch (\Exception $ex) {
            throw $ex;
            return array("image" => null, "extent" => array_fill(0, 4, -1));;
        }
    }

    /**
     * Get a mapObj. If must exist and doesn't do then we throw an exception else we create it.
     * @param string $directory the mapfile directory
     * @param string $file the mapfile file name
     * @param boolean $mustExist If true, the requested object must already exists. If false and the requested object does not exist then throws an exception
     * @return \mapObj
     */
    protected function getMapObj($directory, &$file, $mustExist = true, $removeRelativeDirectory = true)
    {

        $directory = $this->getUserDirectory($directory);

        if ($removeRelativeDirectory)
            $file = pathinfo($file, PATHINFO_FILENAME);
        $file = $directory . str_replace(".map", "", $file) . ".map";

        /*
         * Write values
         */
        if ($mustExist) {
            if (!file_exists($file)) {
                throw new MapserverException($this->_t(sprintf("When loading map object, could not find mapfile at : %s", $file)));
            }
        } else {
            if (!file_exists($file)) {
                if (file_exists($directory . "template.map")) {
                    copy($directory . "template.map", $file);
                } else {
                    throw new MapserverException($this->_t(sprintf("This API needs a template mapfile [path=%s]", $directory . "template.map")));
                }
            }
        }

        // 1. Initialization of mapObj
        try {
            $mapObj = ms_newMapObj($file);
        } catch (\Exception $ex) {
            throw new MapserverException($this->_t(sprintf("When loading map object, could not open mapfile at : %s. ", $file)));
        }
        try {
            $imagepath = $this->container->getParameter("path_image_directory");
            $this->callSet($mapObj->web, "imagepath", $imagepath, sprintf($this->_t(self::DEFAULTMSG_CALLSET), "mapObj->web", "%s"));
        } catch (\Exception $ex) {
            throw new MapserverException($this->_t(sprintf("When loading map object, could not initiate imagepath in mapfile at : %s. ", $file)));
        }
        try {
            $imageurl = $this->container->getParameter("url_image_directory");
            $this->callSet($mapObj->web, "imageurl", $imageurl, sprintf($this->_t(self::DEFAULTMSG_CALLSET), "mapObj->web", "%s"));
        } catch (\Exception $ex) {
            throw new MapserverException($this->_t(sprintf("When loading map object, could not initiate imageurl in mapfile at : %s. ", $file)));
        }
        $mapObject = $mapObj;
        $mapObj->free();
        unset($mapObj);
        return $mapObject;
    }


    /**
     * Get a layerObj. If must exist and doesn't do then we throw an exception else we create it.
     * @param \mapObj $mapObj
     * @param integer|string $layer
     * @return \layerObj
     */
    protected function getLayerObj($mapObj, $layer, $mustExist = true, $duplicate = false)
    {
        if (is_numeric($layer) && ($layer < 0 || ($mustExist ? $layer >= $mapObj->numlayers : $layer > $mapObj->numlayers)))
            throw new MapserverException($this->_t(sprintf('The layer index %s is out of bounds in the mapfile', $layer)));
        $layerObj = null;
        try {

//            dump($layer);
            $layerObj = (is_numeric($layer) ? $mapObj->getLayer($layer) : $mapObj->getLayerByName($layer));
//            dump($layerObj->connection); exit();
            if (!$layerObj) {
                if ($mustExist) {
                    throw new \Exception();
                } else {
                    $layerObj = ms_newLayerObj($mapObj);
                    //SPECIFIC PRODIGE DUE TO FRONTCARTO LABEL MANAGEMENT
                    $layerObj->updateFromString("LAYER
                                                TYPE POINT
                                                METADATA
                                                END # METADATA
                                                VALIDATION
                                                'labelrequires' '[01]'
                                                END
                                                END # LAYER");
                }
            }
        } catch (\Exception $ex) {
            if (!$layerObj) {
                if ($mustExist) {
                    throw new MapserverException($this->_t(sprintf('Could not find layer named %s in mapfile %s', $layer , $mapObj->name)));
                } else {
                    $layerObj = ms_newLayerObj($mapObj);
                    //SPECIFIC PRODIGE DUE TO FRONTCARTO LABEL MANAGEMENT
                    $layerObj->updateFromString("LAYER
                                                TYPE POINT
                                                METADATA
                                                END # METADATA
                                                VALIDATION
                                                'labelrequires' '[01]'
                                                END
                                                END # LAYER");
                }
            }
        }
        return $layerObj;
    }


    /**
     * Get a classObj. If must exist and doesn't do then we throw an exception else we create it.
     * @param \mapObj $mapObj
     * @param \layerObj $layerObj
     * @param integer $style Class index in layerObj
     * @return \classObj
     */
    protected function getClassObj(\mapObj $mapObj, \layerObj $layerObj, $class, $mustExist = true)
    {
        if (is_numeric($class) && ($class < 0 || ($mustExist ? $class >= $layerObj->numclasses : $class > $layerObj->numclasses)))
            throw new MapserverException($this->_t(sprintf("The class index %d is out of bounds in the layer named %s in mapfile %s", $class, $layerObj->name, $mapObj->mappath)));
        $classObj = null;
        try {
            $classObj = $layerObj->getClass($class);
            if (!$classObj) {
                if ($mustExist) {
                    throw new \Exception();
                } else {
                    $classObj = ms_newClassObj($layerObj);
                    $classObj->updateFromString('CLASS LABEL END STYLE COLOR 0 0 0 SYMBOL 0 END END'); // TODO2 Il y a mieux à faire
                }
            }
        } catch (\Exception $ex) {
            if (!$classObj) {
                if ($mustExist) {
                    throw new MapserverException($this->_t(sprintf("Could not find class named %s in layer named %s in mapfile %s", $class, $layerObj->name, $mapObj->mappath)));
                } else {
                    $classObj = ms_newClassObj($layerObj);
                    $classObj->updateFromString('CLASS LABEL END STYLE COLOR 0 0 0 SYMBOL 0 END END'); // TODO2 Il y a mieux à faire
                }
            }
        }
        return $classObj;
    }

    /**
     * Get a styleObj. If must exist and doesn't do then we throw an exception else we create it.
     * @param \mapObj $mapObj
     * @param \layerObj $layerObj
     * @param \classObj $classObj
     * @param integer $style Style index in classObj
     * @return \styleObj
     */
    protected function getStyleObj(\mapObj $mapObj, \layerObj $layerObj, \classObj $classObj, $style, $mustExist = true)
    {
        if (is_numeric($style) && ($style < 0 || ($mustExist ? $style >= $classObj->numstyles : $style > $classObj->numstyles)))
            throw new MapserverException($this->_t(sprintf("The style index %d is out of bounds in the class named %s in mapfile %s", $style, $classObj->name, $file)));

        $styleObj = null;
        try {
            $styleObj = $classObj->getStyle($style);
            if (!$styleObj) {
                if ($mustExist) {
                    throw new \Exception();
                } else {
                    try {
                        $styleObj = $classObj->getStyle($style);
                    } catch (\Exception $ex) {
                        $classObj->updateFromString("CLASS STYLE END END");
                        $styleObj = $classObj->getStyle($style);
                    }
                }
            }
        } catch (\Exception $ex) {
            if (!$styleObj) {
                if ($mustExist) {
                    throw new MapserverException($this->_t(sprintf("Could not find style named %s in class named %s in mapfile %s", $style, $classObj->name, $file)));
                } else {
                    try {
                        $styleObj = $classObj->getStyle($style);
                    } catch (\Exception $ex) {
                        $classObj->updateFromString("CLASS STYLE END END");
                        $styleObj = $classObj->getStyle($style);
                    }
                }
            }
        }
        return $styleObj;
    }


    /**
     * Get a labelObj. If must exist and doesn't do then we throw an exception else we create it.
     * @param \mapObj $mapObj
     * @param \layerObj $layerObj
     * @param \classObj $classObj
     * @param integer $label Label index in classObj
     * @return \labelObj
     */
    protected function getLabelObj(\mapObj $mapObj, \layerObj $layerObj, \classObj $classObj, $label, $mustExist = true)
    {
        if (is_numeric($label) && ($label < 0 || ($mustExist ? $label >= $classObj->numlabels : $label > $classObj->numlabels))) {
            throw new MapserverException($this->_t(sprintf("The label index %d is out of bounds in the class named %s in mapfile %s", $label, $classObj->name, $file)));
        }
        $labelObj = null;
        try {
            $labelObj = $classObj->getLabel($label);
            if (!$labelObj) {
                if ($mustExist) {
                    throw new \Exception();
                } else {
                    if ($classObj->numlabels == 0) {
                        $classObj->updateFromString("CLASS LABEL END END");
                    }
                    $labelObj = $classObj->getLabel(0);
                }
            }
        } catch (\Exception $ex) {
            if (!$labelObj) {
                if ($mustExist) {
                    throw new MapserverException($this->_t(sprintf("Could not find label named %s in class named %s in mapfile %s", $label, $class, $file)));
                } else {
                    if ($classObj->numlabels == 0) {
                        $classObj->updateFromString("CLASS LABEL END END");
                    }
                    $labelObj = $classObj->getLabel(0);
                }
            }
        }
        return $labelObj;
    }


    /**
     * Get a layer in mapObj
     * @param \mapObj $mapObj
     * @param integer|string $layer
     * @return array
     */
    protected function convertMapObj(\mapObj $mapObj)
    {

        $layers = array();
        for ($layer = 0; $layer < $mapObj->numlayers; $layer++) {
            $layers[] = $this->convertLayerObj($mapObj, $mapObj->getLayer($layer));
        }

        $result = array(
            "mappath" => $mapObj->mappath,
            "shapepath" => $mapObj->shapepath,
            "fontset" => $mapObj->fontsetfilename,
            "symbolset" => $mapObj->symbolsetfilename,//str_replace($mapObj->mappath, "", $mapObj->symbolsetfilename),
            "name" => $mapObj->name,

            "status" => $mapObj->status,
            "units" => $mapObj->units,
            "mapUnits" => $this->convertUnits($mapObj->units),
            "imagetype" => $mapObj->imagetype,
            //"transparent"        => ($mapObj->getOutputFormat(0)->transparent==MS_TRUE ? "true" : "false"),
//           "template"         => $mapObj->template,
//           "maxscaledenom"    => $mapObj->maxscaledenom,
//           "minscaledenom"    => $mapObj->minscaledenom,

            "numlayers" => $mapObj->numlayers,

            "projection" => $mapObj->getProjection(),
            "imagecolor" => $this->colorToHex($mapObj->imagecolor),
            "extent" => $this->rectObjToArray($mapObj->extent),

            "metadata" => $this->hashTableObjToArray($mapObj->metadata),
            "web" => $this->convertWebObj($mapObj->web),
            "reference" => $this->convertReferenceMapObj($mapObj, $mapObj->reference),
            "layers" => $layers
        );
        $mapObj->free();
        unset($mapObj);
        return $result;
    }

    /**
     * Convert referenceMapObj to array
     * @param \referenceMapObj $referenceMapObj
     * @return array
     */
    protected function convertReferenceMapObj(\mapObj $mapObj, \referenceMapObj $referenceMapObj)
    {
        $result = array(
            "color" => $this->colorToHex($referenceMapObj->color),
            "height" => $referenceMapObj->height,
            "width" => $referenceMapObj->width,
            "extent" => $this->rectObjToArray($referenceMapObj->extent) ?: $this->rectObjToArray($mapObj->extent),
            "image" => $referenceMapObj->image,
            "marker" => $referenceMapObj->marker,
            "markername" => $referenceMapObj->markername,
            "markersize" => $referenceMapObj->markersize,
            "maxboxsize" => $referenceMapObj->maxboxsize,
            "minboxsize" => $referenceMapObj->minboxsize,
            "outlinecolor" => $this->colorToHex($referenceMapObj->outlinecolor),
            "status" => ($referenceMapObj->status == MS_ON ? "ON" : "OFF"),
        );
        $referenceMapObj->free();
        unset($referenceMapObj);
        return $result;
    }

    /**
     * Convert webObj to array
     * @param \webObj $webObj
     * @return array
     */
    protected function convertWebObj(\webObj $webObj)
    {
        $result = array(
            "browseformat" => $webObj->browseformat,
            "empty" => $webObj->empty,
            "error" => $webObj->error,
            "extent" => $this->rectObjToArray($webObj->extent),
            "footer" => $webObj->footer,
            "header" => $webObj->header,
            "imagepath" => $webObj->imagepath,
            "imageurl" => $webObj->imageurl,
            "legendformat" => $webObj->legendformat,
            "log" => $webObj->log,
            "maxtemplate" => $webObj->maxtemplate,
            "metadata" => $this->hashTableObjToArray($webObj->metadata),
            "maxscaledenom" => $webObj->maxscaledenom,
            "minscaledenom" => $webObj->minscaledenom,
            "mintemplate" => $webObj->mintemplate,
            "queryformat" => $webObj->queryformat,
            "template" => $webObj->template,
            "temppath" => $webObj->temppath,
        );
        $webObj->free();
        unset($webObj);
        return $result;
    }

    /**
     * return text according to unit mapserv constant
     * @param unit (mapserv constant)
     * @return text
     */
    protected function convertUnits($unit)
    {
        //MS_INCHES, MS_FEET, MS_MILES, MS_METERS, MS_KILOMETERS, MS_DD, MS_PIXELS, MS_NAUTICALMILES
        switch ($unit) {
            case MS_INCHES :
                return "INCHES";
                break;
            case MS_FEET :
                return "FEET";
                break;
            case MS_MILES :
                return "MILES";
                break;
            case MS_METERS :
                return "METERS";
                break;
            case MS_KILOMETERS :
                return "KILOMETERS";
                break;
            case MS_DD :
                return "DD";
                break;
            case MS_PIXELS :
                return "PIXELS";
                break;
            case MS_NAUTICALMILES :
                return "NAUTICALMILES";
                break;
            default :
                return "METERS";
                break;
        }
    }

    /**
     * Convert layerObj to array
     * @param \mapObj mapObj
     * @param \layerObj layerObj
     * @return array
     */
    protected function convertLayerObj(\mapObj $mapObj, \layerObj $layerObj)
    {
        $connectionsTypeName = array(
            MS_INLINE => "MS_INLINE",
            MS_SHAPEFILE => "MS_SHAPEFILE",
            MS_TILED_SHAPEFILE => "MS_TILED_SHAPEFILE",
//          MS_SDE             => "MS_SDE", 
            MS_OGR => "MS_OGR",
//          MS_TILED_OGR       => "MS_TILED_OGR", 
            MS_POSTGIS => "MS_POSTGIS",
            MS_WMS => "MS_WMS",
            MS_ORACLESPATIAL => "MS_ORACLESPATIAL",
            MS_WFS => "MS_WFS",
            MS_GRATICULE => "MS_GRATICULE",
            MS_RASTER => "MS_RASTER",
            MS_RASTER => "MS_PLUGIN",
            MS_RASTER => "MS_UNION"
        );

        $geometrytype = null;
        //TODO MARIE Pourquoi faire cet appel surchargé ensuite?
        if (in_array($layerObj->connectiontype, array(MS_SHAPEFILE, MS_OGR, MS_POSTGIS, MS_WFS))) {
            $layerType = ($layerObj->connectiontype == MS_WFS ? "WFS" : $layerObj->connectiontype == MS_POSTGIS ? "POSTGIS" : "RASTER");
            list($layerData, $layerName) = self::_getDataForPhpOGR($mapObj, $layerObj, true);
            if ($layerData) {
                $geometrytype = $this->_getGeometryType($layerType, $layerData, $layerName);
                $geometrytype = $geometrytype["msGeometryType"] ?: $geometrytype["layerType"];
            }
        }
        $layerType = $layerObj->type;
        $is_raster = in_array($layerType, array(MS_LAYER_RASTER, MS_LAYER_TILEINDEX));

        switch ($layerType) {
            case MS_LAYER_POINT     :
                $geometrytype = "POINT";
                break;
            case MS_LAYER_LINE      :
                $geometrytype = "LINE";
                break;
            case MS_LAYER_POLYGON   :
                $geometrytype = "POLYGON";
                break;
            case MS_LAYER_RASTER    :
                if ($layerObj->tileindex != "")
                    $geometrytype = "TILEINDEX";
                else
                    $geometrytype = "RASTER";
                break;
        }

        $MS_LAYER_TYPES = explode("|", self::MS_LAYER_TYPES);
        foreach ($MS_LAYER_TYPES as $MS_LAYER_TYPE) {
            if (defined($MS_LAYER_TYPE) && constant($MS_LAYER_TYPE) == $layerType) {
                $layerType = str_replace("MS_LAYER_", "", $MS_LAYER_TYPE);
                break;
            }
        }

        $hasLabel = false;
        $classes = array();
        for ($classe = 0; $classe < $layerObj->numclasses; $classe++) {
            $classes[$classe] = $this->convertClassObj($layerObj->getClass($classe), $classe);
            $hasLabel = $hasLabel || (isset($classes[$classe]["labels"]) && !empty($classes[$classe]["labels"]));
        }

        $processingString = $layerObj->getProcessing();
        $processing = array();
        foreach ($processingString as $process) {
            $process = explode('=', $process);
            $processing[trim($process[0])] = utf8_encode(trim($process[1]));
        }
        $symbolscaledenompixels = null;
        if ($layerObj->symbolscaledenom && $layerObj->symbolscaledenom != -1) {
            $extent = $mapObj->extent;
            $scale = ($extent->maxx - $extent->minx) / ($mapObj->width * 0.0254 / 72);
            $symbolscaledenompixels = $layerObj->symbolscaledenom / $scale;
        }

        $result = array(
            "index" => $layerObj->index,
            "name" => $layerObj->name,

            "tileindex" => $layerObj->tileindex,
            "tileitem" => $layerObj->tileitem,
            "data" => $layerObj->data,
            "type" => $layerType,
            "geometrytype" => $geometrytype,
            "is_raster" => $is_raster,

            "projection" => $layerObj->getProjection(),
            "status" => ($layerObj->status == MS_ON ? "ON" : "OFF"),
            "sizeunits" => $layerObj->sizeunits,
            "connectiontypename" => (array_key_exists($layerObj->connectiontype, $connectionsTypeName) ? $connectionsTypeName[$layerObj->connectiontype] : ""),
            "connectiontype" => $layerObj->connectiontype,
            "connection" => $layerObj->connection,
            "maxscaledenom" => $layerObj->maxscaledenom,
            "minscaledenom" => $layerObj->minscaledenom,
            "offsite" => $this->colorToHex($layerObj->offsite),
            "processing" => $processing,
            "symbolscaledenom" => ($layerObj->symbolscaledenom && $layerObj->symbolscaledenom == -1 ? 0 : $layerObj->symbolscaledenom),
            "symbolscaledenompixels" => $symbolscaledenompixels,
            "tolerance" => $layerObj->tolerance,
            "classitem" => $this->encode($layerObj->classitem),
            "labelitem" => ($hasLabel ? $this->encode($layerObj->labelitem) : null),
            "numclasses" => $layerObj->numclasses,

            "metadata" => $this->hashTableObjToArray($layerObj->metadata),
            "classes" => $classes
        );
        $this->getLogger()->debug(__METHOD__, $result);
        $layerObj->free();
        unset($layerObj);
        return $result;
    }


    /**
     * Convert classObj to array
     * @param \classObj classObj
     * @param int $index the current index of this object in its owner object
     * @return array
     */
    protected function convertClassObj(\classObj $classObj, $index, $bGenerateIcon = true)
    {
        $styles = array();
        for ($style = 0; $style < $classObj->numstyles; $style++) {
            $styles[] = $this->convertStyleObj($classObj->getStyle($style), $style);
        }

        $labels = array();
        for ($label = 0; $label < $classObj->numlabels; $label++) {
            $labels[] = $this->convertLabelObj($classObj->getLabel($label), $label);
        }

        $result = array(
            "index" => $index,
            "name" => $classObj->name,
            "title" => $classObj->title,
            "group" => $classObj->group,
            "keyimage" => $classObj->keyimage,
            "maxscaledenom" => $classObj->maxscaledenom,
            "minscaledenom" => $classObj->minscaledenom,
            "numlabels" => $classObj->numlabels,
            "numstyles" => $classObj->numstyles,
            "status" => ($classObj->status == MS_ON ? "ON" : "OFF"),
            "template" => $classObj->template,
            "metadata" => $this->hashTableObjToArray($classObj->metadata),
            "expression" => preg_replace('!^"|"$!', "", $classObj->getExpressionString()),
            "expressionstring" => $classObj->getExpressionString(),

            "visible" => $classObj->status == MS_ON,
            "legend" => $classObj->name != "",
            "styles" => $styles,
            "labels" => $labels,
        );
        if ($bGenerateIcon) {
            $result["icon"] = $this->generateClassIcon($classObj);
        }
        $classObj->free();
        unset($classObj);
        return $result;
    }

    /**
     * Generate the class Legend icon. Returns the json array for the generated icon
     * Optional : Rename it.
     * Optional : Set a transparent color
     * @param \classObj $classObj The classObj
     * @param string $rename The name of the icon file (filename without extension and directory)
     * @param string $transparent_color Hexa color
     * @return array
     */
    protected function generateClassIcon(\classObj $classObj, $rename = null, $transparent_color = null)
    {
        try {
            $minsize = 0;
            for ($i = 0; $i < $classObj->numstyles; $i++) {
                $style = $classObj->getStyle($i);
                $minsize = max($minsize, $style->size ?: $style->width);
            }
            $minsize = max($minsize, self::LEGEND_ICON_WIDTH);
            $size = sqrt(2 * pow($minsize, 2));
            //$this->getLogger()->debug(__METHOD__, array($minsize, $size, $style->size, $style->width, $style->symbol, $style->symbolname, $style->color->toHex()));
            $imageObj = $classObj->createLegendIcon($size, $size);

            $image_name = str_replace($imageObj->imageurl, "", $imageObj->saveWebImage());
            $file = $imageObj->imagepath . "/" . $image_name;
            if (!file_exists($file)) return array();
            $extension = pathinfo($file, PATHINFO_EXTENSION);
            if ($transparent_color) {
                $function_open = "imagecreatefrom" . strtolower($extension);
                $function_save = "image" . strtolower($extension);

                $gdimg = $function_open($file);
                $transparent_color = call_user_func_array("imagecolorallocate", array_merge(array($gdimg), $this->getRGB($transparent_color)));
                imagecolortransparent($gdimg, $transparent_color);
                $function_save($gdimg, $file);
                imagedestroy($gdimg);
            }

            if ($rename) {
                rename($file, $imageObj->imagepath . "/" . $rename . "." . $extension);
                $image_name = $rename . "." . $extension;
            }


            $type = pathinfo($image_name, PATHINFO_EXTENSION);
            $data = file_get_contents($imageObj->imagepath . "/" . $image_name);
            $image = 'data:image/' . $type . ';base64,' . base64_encode($data);

            $result = array(
                "image" => $image,
                "imagepath" => $imageObj->imagepath,
                "imageurl" => $imageObj->imageurl
            );

            $imageObj = null;
            @unlink($file);
            @unlink($image_name);

            return $result;
        } catch (\MapScriptException $ex) {
            return array();
        }

    }

    /**
     * Convert labelObj to array
     * @param \labelObj $labelObj
     * @param int $index the current index of this object in its owner object
     * @return array
     */
    protected function convertLabelObj(\labelObj $labelObj, $index)
    {
        $positions = array("MS_UL", "MS_UC", "MS_UR", "MS_CL", "MS_CC", "MS_CR", "MS_LL", "MS_LC", "MS_LR", "MS_AUTO");
        $labelposition = "AUTO";
        foreach ($positions as $position) {
            if ($labelObj->position == constant($position)) {
                $labelposition = str_replace("MS_", "", $position);
            }
        }
        $styles = array();
        for ($style = 0; $style < $labelObj->numstyles; $style++) {
            $styles[] = $this->convertStyleObj($labelObj->getStyle($style), $style);
        }
        $result = array(
            "index" => $index,
            "align" => $labelObj->align,
            "angle" => $this->encode($labelObj->getBinding(MS_LABEL_BINDING_ANGLE)) ?: $labelObj->angle,
            "anglemode" => $labelObj->anglemode,
            //"antialias" => $labelObj->antialias,
            "autominfeaturesize" => $labelObj->autominfeaturesize,
            "buffer" => $labelObj->buffer,
            "encoding" => $labelObj->encoding,
            "font" => $labelObj->font,
            "force" => ($labelObj->force == MS_ON ? true : false),
            "maxlength" => $labelObj->maxlength,
            "maxsize" => $labelObj->maxsize,
            "mindistance" => $labelObj->mindistance,
            "minfeaturesize" => $labelObj->minfeaturesize,
            "minlength" => $labelObj->minlength,
            "minsize" => $labelObj->minsize,
            "numstyles" => $labelObj->numstyles,
            "offsetx" => $labelObj->offsetx,
            "offsety" => $labelObj->offsety,
            "outlinewidth" => $labelObj->outlinewidth,
            "partials" => $labelObj->partials,
            "position" => strtolower($labelposition),
            "priority" => $labelObj->priority,
            "repeatdistance" => $labelObj->repeatdistance,
            "shadowsizex" => $labelObj->shadowsizex,
            "shadowsizey" => $labelObj->shadowsizey,
            "size" => $labelObj->size,
            "type" => $labelObj->type,
            "wrap" => $labelObj->wrap,
            "styles" => $styles,

            "color" => $this->colorToHex($labelObj->color),
            "outlinecolor" => $this->colorToHex($labelObj->outlinecolor),
            "shadowcolor" => $this->colorToHex($labelObj->shadowcolor),
            "maxscaledenom" => ($labelObj->maxscaledenom == -1 ? null : $labelObj->maxscaledenom),
            "minscaledenom" => ($labelObj->minscaledenom == -1 ? null : $labelObj->minscaledenom)
        );
        $labelObj->free();
        unset($labelObj);
        return $result;
    }

    /**
     * Convert styleObj to array
     * @param \styleObj $styleObj
     * @param int $index the current index of this object in its owner object
     * @return array
     */
    protected function convertStyleObj(\styleObj $styleObj, $index = null)
    {
        $result = array(
            "index" => $index,
            "color" => $this->colorToHex($styleObj->color),
            "symbol" => $styleObj->symbolname ?: $styleObj->symbol,
            "width" => $styleObj->width,
            "angle" => $styleObj->angle,
            "size" => $this->encode($styleObj->getBinding(MS_STYLE_BINDING_SIZE)) ?: $styleObj->size,
            "minsize" => $styleObj->minsize,
            "maxsize" => $styleObj->maxsize,
            "outlinecolor" => $this->colorToHex($styleObj->outlinecolor),
            "outlinewidth" => $styleObj->outlinewidth,
            "geomtransform" => $styleObj->getGeomTransform(),
            "pattern" => ($styleObj && $styleObj->patternlength ? implode(" ", $styleObj->getPatternArray()) : null),
        );
        $styleObj->free();
        unset($styleObj);
        return $result;
    }


    protected function encode($value)
    {
        $nothing_to_do = @json_encode(array($value)) !== null ? $value : null;
        $utf8_encode = @json_encode(array(utf8_encode($value))) !== null ? utf8_encode($value) : null;
        $utf8_decode = @json_encode(array(utf8_decode($value))) !== null ? utf8_decode($value) : null;
//     	$this->getLogger()->info(__METHOD__, array($nothing_to_do , $utf8_encode , $utf8_decode));
        $value = $nothing_to_do ?: $utf8_encode ?: $utf8_decode;
        return $value;
    }
}
