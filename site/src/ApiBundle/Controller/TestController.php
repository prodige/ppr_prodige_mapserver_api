<?php

namespace Mapserver\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Link;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Unlink;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;

// GET params
use FOS\RestBundle\Controller\Annotations\RequestParam;

// POST params

use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

use Symfony\Component\HttpFoundation\JsonResponse;
use Mapserver\ApiBundle\Exception\MapserverException;

/**
 * Mapserver API.
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/mapserver")
 */
class TestController extends BaseController
{

//
//@OA\RequestBody(
//*         description="Input data format",
//*         @OA\MediaType(
//*             mediaType="application/x-www-form-urlencoded",
//*             @OA\Schema(
//*                 type="object",
//*                 @OA\Property(
//*                     property="directory",
//*                     description="MAP FILE DIRECTORY",
//*                     type="string",
//*                 ),
//*                 @OA\Property(
//*                     property="locators",
//*                     description="Locators params",
//*                     type="string"
//*                 )
//*             )
//*         )
//*     ),
//    
    /**
     * @Get("/map/get")
     *
     * @param string $file mapfile name without extension
     *
     * @example GET /api/map/environnement
     *
     * @Operation(
     *     tags={"Map"},
     *     summary="get Map information",
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getMap(Request $request, ParamFetcher $paramFetcher)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            return $this->formatJsonSuccess(Response::HTTP_CREATED, array('id' => 1));
            //return $this->formatJsonSuccess(Response::HTTP_OK, array('id'=>1));
            //return $this->formatJsonError(Response::HTTP_NOT_IMPLEMENTED, 'not yet implemented');

        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }


    /**
     * @Get("/test/putMap")
     *
     * @param string $file mapfile name without extension
     *
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     * @RequestParam(name="name"         , description="MAP NAME", nullable=false)
     * @RequestParam(name="fontset"      , description="MAP FONTSET", nullable=true, default=TestController::DEFAULT_FONTSET)
     * @RequestParam(name="symbolset"    , description="MAP SYMBOLSET", nullable=true, default=TestController::DEFAULT_SYMBOLSET)
     * @RequestParam(name="imagecolor"   , description="MAP IMAGECOLOR", nullable=false)
     * @RequestParam(name="imagetype"    , description="MAP IMAGETYPE", nullable=false)
     * @RequestParam(name="extent"       , description="MAP EXTENT", nullable=false)
     * @RequestParam(name="units"        , description="MAP UNITS", nullable=false)
     * @RequestParam(name="projection"   , description="PROJECTION", nullable=false)
     * @RequestParam(name="maxscaledenom", description="WEB MAXSCALEDENOM", nullable=true)
     * @RequestParam(name="minscaledenom", description="WEB MINSCALEDENOM", nullable=true)
     * @RequestParam(name="metadata"     , description="WEB METADATA (JSON OBJECT {name:value})", nullable=true)
     * @RequestParam(name="template"     , description="WEB TEMPLATE", nullable=true)
     *
     * @example PUT /api/map/environnement
     *
     * @Operation(
     *     tags={"Map"},
     *     summary="create/update Map",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="directory",
     *                     description="MAP FILE DIRECTORY",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     description="MAP NAME",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="fontset",
     *                     description="MAP FONTSET",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="symbolset",
     *                     description="MAP SYMBOLSET",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="imagecolor",
     *                     description="MAP IMAGECOLOR",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="imagetype",
     *                     description="MAP IMAGETYPE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="extent",
     *                     description="MAP EXTENT",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="units",
     *                     description="MAP UNITS",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="projection",
     *                     description="PROJECTION",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="maxscaledenom",
     *                     description="WEB MAXSCALEDENOM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="minscaledenom",
     *                     description="WEB MINSCALEDENOM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="metadata",
     *                     description="WEB METADATA (JSON OBJECT {name:value})",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="template",
     *                     description="WEB TEMPLATE",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function putMap(Request $request, ParamFetcher $paramFetcher)
    {
        $file = "success_map";

        $data = array(
            "name" => "cecinestpasunecarte", // string
            "fontset" => "etc/fonts.txt", // string = fontset file path
            "symbolset" => "etc/symboles/symbols.sym", // string = symbolset file path
            "imagecolor" => "#00FF00", // color in HEX that we'll later convert into RGB then into colorObj
            "imagetype" => "PNG", // string I guess, but can't find in the doc
            "extent" => "-10.20 -0.5 10 1.6", // 4 decimal values (minx, miny, maxx, maxy) in a string we'll later convert into rectObj
            "units" => "METERS", // string that should match a map units type
            "projection" => "proj=utm,ellps=GRS80,zone=15,north,no_defs", // projection string (= some "PROJ.4" strings separated with "," to then create a projectionObj
            "maxscaledenom" => "500", // (for the web) decimal
            "minscaledenom" => "499.99", // (for the web) decimal
            "metadata" => json_encode(array("key1" => "value1", "key7" => "value7", "key2" => "value2", "value5")), // metadata for the web, that we'll later convert into a hashTableObj
            "template" => "template.map", // (for the web) string = template file path
        );

        $request->request->replace($data);

        try {
            $api = new ApiController();
            $api->setContainer($this->container);

            $results = $api->putMap($paramFetcher, $file);
            $results instanceof JsonResponse;

            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }

        /*$file = "fail_map";
       $data = array(
               "name"          => "cecinestpasunecarte", // string
               "fontset"		=> "./etc/fonts.txt", // string = fontset file name
               "symbolset"		=> "path/to/fontset/symbolset.ext", // string = symbolset file name
               "imagecolor"    => "#FFFFFF", // color in HEX we'll later convert into RGB then into colorObj
               "imagetype"     => "PNG", // string I guess, but can't find in the doc (TODO)
               "extent"		=> "-10.20 -0.5 10 1.6", // 4 decimal values (minx, miny, maxx, maxy) in a string we'll later convert into rectObj
               "units"         => "METERS", // string that should match a map units type
               "projection"	=> "thisisaPROJECTIONSTYLEstring", //(epsg:26915) projection string (matching sthing ? TODO) to then create a projectionObj
               "maxscaledenom" => "500", // decimal TODO
               "minscaledenom" => "", // decimal TODO
               "metadata"      => json_encode(array("key1"=>"value1", "key7"=>"value7", "key2"=>"value2", "value5")), // metadata we'll later convert into a hashTableObj
               "template"    => "", // template ?? TODO
       );

       $request->request->replace($data);

       try{
           $api = new ApiController();
           $api->setContainer($this->container);

           $results = $api->putMap($paramFetcher, $file);
           $results instanceof JsonResponse;

           if ( $results->getStatusCode()!==200 ){
               throw new \Exception($results->getContent());
           }
       }catch(\Exception $ex){
           echo $ex->getMessage();
       }
       */

        return new Response("<br/><br/>************************<br/> End of method : " . __METHOD__);

    }

    /**
     * @Delete("/test/deleteMap")
     *
     * @param string $file mapfile name without extension
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     * @RequestParam(name="name"         , description="MAP NAME", nullable=false)
     *
     * @example DELETE /api/map/environnement
     *
     * @Operation(
     *     tags={"Map"},
     *     summary="delete map file",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="directory",
     *                     description="MAP FILE DIRECTORY",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     description="MAP NAME",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function deleteMap(ParamFetcher $paramFetcher)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $thisFile = "success_map";
            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->deleteMap($paramFetcher, $thisFile);

            $results instanceof JsonResponse;

            if ($results->getStatusCode() !== 200) {

                throw new \Exception($results->getContent());

            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);
    }

    /**
     * @Get("/map/{file}/reference")
     *
     * @param string $file mapfile name without extension
     *
     * @example GET /api/map/environnement/reference
     *
     * @Operation(
     *     tags={"Reference"},
     *     summary="get reference map information",
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getReference(Request $request, ParamFetcher $paramFetcher)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            return $this->formatJsonError(Response::HTTP_NOT_IMPLEMENTED, 'not yet implemented');

        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }


    // PUT REFERENCE

    /**
     * @PUT("/test/putReference")
     *
     * @param string $file mapfile name without extension
     *
     * @RequestParam(name="directory", description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     * @RequestParam(name="image", description="REFERENCE IMAGE", nullable=false)
     * @RequestParam(name="extent", description="REFERENCE EXTENT", nullable=false)
     * @RequestParam(name="size", description="REFERENCE SIZE", nullable=false)
     * @RequestParam(name="color", description="REFERENCE COLOR", nullable=true)
     * @RequestParam(name="outlinecolor", description="REFERENCE OUTLINECOLOR", nullable=true)
     *
     * @example PUT /api/environnement/reference/
     * @example only PUT since Instances of referenceMapObj are always embedded inside the mapObj.
     *
     * @Operation(
     *     tags={"Reference"},
     *     summary="update Reference Map",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="directory",
     *                      description="MAP FILE DIRECTORY",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="image",
     *                      description="REFERENCE IMAGE",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="extent",
     *                      description="REFERENCE EXTENT",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="size",
     *                      description="REFERENCE SIZE",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="color",
     *                      description="REFERENCE COLOR",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="outlinecolor",
     *                      description="REFERENCE OUTLINECOLOR",
     *                      type="string"
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function putReference(Request $request, ParamFetcher $paramFetcher)
    {
        /* we create the two parameters we will send via GET in the URL */
        $thisFile = "success_map";
        $thisReference = "0";

        /* we send the data as a request to test ApiController */
        $data = array(
            "color" => "#04B404", // string of color in HEX",
            "extent" => "0.2 0.5 20 50",
            "image" => "thisisaIMAGEfilepath",
            "outlinecolor" => "#04B404", // string of color in HEX
            "size" => "420 1024",
        );

        $request->request->replace($data);

        try {
            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->putReference($paramFetcher, $thisFile, $thisReference);

            $results instanceof JsonResponse;
            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);

    }


    /**
     * @Get("/test/getLayer")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     *
     * @example GET /api/map/environnement/layer/layer1
     *
     * @Operation(
     *     tags={"Layer"},
     *     summary="get Layer information",
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getLayer(Request $request, ParamFetcher $paramFetcher)
    {
        /* arguments given in the URL via PUT */
        $file = "map_test";
        $layer = "layer1";

        /* request values*/
        $data = array();

        $request->request->replace($data);

        try {
            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->getLayer($request, $paramFetcher, $file, $layer);

//            var_dump($results);

            $results instanceof JsonResponse;

            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }

        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);

        // always put your code in a try catch block to be sure to always return a JsonResponse
    }


    /**
     * @GET("/test/putLayer")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     *
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     * @RequestParam(name="name", description="LAYER NAME", nullable=false)
     * @RequestParam(name="type", description="LAYER TYPE", nullable=false)
     * @RequestParam(name="connectiontype", description="LAYER CONNECTIONTYPE", nullable=true)
     * @RequestParam(name="connection", description="LAYER CONNECTION", nullable=true)
     * @RequestParam(name="data", description="LAYER DATA", nullable=true)
     * @RequestParam(name="tileindex", description="LAYER TILEINDEX", nullable=true)
     * @RequestParam(name="tileitem", description="LAYER TILEITEM", nullable=true)
     * @RequestParam(name="projection", description="LAYER PROJECTION", nullable=false)
     * @RequestParam(name="status", description="LAYER STATUS", nullable=false)
     * @RequestParam(name="sizeunits", description="LAYER UNITS", nullable=false)
     * @RequestParam(name="maxscaledenom", description="LAYER MAXSCALEDENOM", nullable=true)
     * @RequestParam(name="minscaledenom", description="LAYER MINSCALEDENOM", nullable=true)
     * @RequestParam(name="labelitem", description="LAYER LABELITEM", nullable=true)
     * @RequestParam(name="offsite", description="LAYER OFFSITE", nullable=true)
     * @RequestParam(name="processing", description="LAYER PROCESSING", nullable=true)
     * @RequestParam(name="symbolscaledenom", description="LAYER SYMBOLSCALEDENOM", nullable=true)
     * @RequestParam(name="tolerance", description="LAYER TOLERANCE", nullable=true)
     * @RequestParam(name="toleranceunits", description="LAYER TOLERANCEUNITS", nullable=true)
     * @RequestParam(name="classitem", description="LAYER CLASSITEM", nullable=true)
     * @RequestParam(name="metadata", description="LAYER METADATA (JSON OBJECT {name:value})", nullable=true)
     *
     * @example PUT /api/environnement/layer/layer1
     *
     * @Operation(
     *     tags={"Layer"},
     *     summary="create/update Layer",
     *     @OA\Parameter(
     *         name="directory",
     *         in="body",
     *         description="MAP FILE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="body",
     *         description="LAYER NAME",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="body",
     *         description="LAYER TYPE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="connectiontype",
     *         in="body",
     *         description="LAYER CONNECTIONTYPE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="connection",
     *         in="body",
     *         description="LAYER CONNECTION",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="data",
     *         in="body",
     *         description="LAYER DATA",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="tileindex",
     *         in="body",
     *         description="LAYER TILEINDEX",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="tileitem",
     *         in="body",
     *         description="LAYER TILEITEM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="projection",
     *         in="body",
     *         description="LAYER PROJECTION",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="body",
     *         description="LAYER STATUS",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="sizeunits",
     *         in="body",
     *         description="LAYER UNITS",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="maxscaledenom",
     *         in="body",
     *         description="LAYER MAXSCALEDENOM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="minscaledenom",
     *         in="body",
     *         description="LAYER MINSCALEDENOM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="labelitem",
     *         in="body",
     *         description="LAYER LABELITEM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="offsite",
     *         in="body",
     *         description="LAYER OFFSITE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="processing",
     *         in="body",
     *         description="LAYER PROCESSING",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="symbolscaledenom",
     *         in="body",
     *         description="LAYER SYMBOLSCALEDENOM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="tolerance",
     *         in="body",
     *         description="LAYER TOLERANCE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="toleranceunits",
     *         in="body",
     *         description="LAYER TOLERANCEUNITS",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="classitem",
     *         in="body",
     *         description="LAYER CLASSITEM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="metadata",
     *         in="body",
     *         description="LAYER METADATA (JSON OBJECT {name:value})",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */


    public function putLayer(Request $request, ParamFetcher $paramFetcher)
    {
        /* we create the two parameters we will send via GET in the URL */
        $thisFile = "success_map";
        $thisLayer = "success_layer";
        $thisFile = "temp_admin_4_553"; //string
        $thisLayer = "testvecteur1"; //string


        /* we send the data as a request to test ApiController */
        $data = array(
            "name" => $thisLayer, // string
            "type" => "RASTER", // string matching sthing
            "connectiontype" => "TILED_SHAPEFILE",
            "connection" => "thisisaCONNECTIONstring", // string
            "data" => "thisisaDATAstring", // string
            "tileindex" => "thisisaTILEINDEXstring", //string
            "tileitem" => "thisisaTILEITEMstring", // string
            "projection" => "proj=utm,ellps=GRS80,zone=15,north,no_defs", // projection string (= some "PROJ.4" strings separated with "," to then create a projectionObj
            "status" => "MS_DEFAULT",
            "sizeunits" => "FEET",
            "maxscaledenom" => "22.03",
            "minscaledenom" => "03.22",
            "labelitem" => "thisisaLABELITEMstring", // string
            "offsite" => "#04B404", // string of color in HEX
            "processing" => "thisisaPROCESSINGstring",
            "symbolscaledenom" => "222.22", // decimal
            "tolerance" => "0.01", // decimal
            "toleranceunits" => "10", // decimal
            "classitem" => "thisisaCLASSITEMstring", // string
            "metadata" => json_encode(array("key1" => "value1", "key7" => "value7", "key2" => "value2", "value5")), // metadata for the web, that we'll later convert into a hashTableObj
        );

        $data = array
        (
            "directory" => "/home/devperso/martigny/BRGM/carmen_mapserver/web/data/ALKANTE/Publication/",
            "name" => "testvecteur01",
            "type" => "point",
            "projection" => "EPSG:2154",
            "status" => "ON",
            "sizeunits" => "meters",
            "maxscaledenom" => 12000000,
            "minscaledenom" => 500,
            "data" => "./testvecteur1.tab"
        );
        $request->request->replace($data);
        try {


            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->putLayer($paramFetcher, $thisFile, $thisLayer);

            $results instanceof JsonResponse;
            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw $ex;
        }
        return new Response("<br/><br/>Fin " . __METHOD__);

    }


    /**
     * @Get("/test/deleteLayer")
     *
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     *
     * @example DELETE /api/environnement/layer/layer1
     *
     * @Operation(
     *     tags={"Layer"},
     *     summary="delete layer from map file",
     *     @OA\Parameter(
     *         name="directory",
     *         in="body",
     *         description="MAP FILE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function deleteLayer(ParamFetcher $paramFetcher)
    {    // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $thisFile = "success_map";
            $thisLayer = "success_layer";

            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->deleteLayer($paramFetcher, $thisFile, $thisLayer);

            $results instanceof JsonResponse;

            if ($results->getStatusCode() !== 200) {

                throw new \Exception($results->getContent());

            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);
    }


    /**
     * @Get("/map/{file}/layer/{layer}/class/{class}")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     *
     * @example GET /api/map/environnement/layer/layer1/class/1
     *
     * @Operation(
     *     tags={"Class"},
     *     summary="get Class information",
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getClass(Request $request, ParamFetcher $paramFetcher)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            return $this->formatJsonError(Response::HTTP_NOT_IMPLEMENTED, 'not yet implemented');

        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }






    // PUT CLASS

    /**
     * @GET("/test/putClass")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     *
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     * @RequestParam(name="name", description="CLASS NAME", nullable=false)
     * @RequestParam(name="expression", description="CLASS EXPRESSION", nullable=false)
     * @RequestParam(name="status", description="CLASS STATUS", nullable=true)
     *
     * @example PUT /api/map/environnement/layer/layer1/class/1
     *
     * @Operation(
     *     tags={"Class"},
     *     summary="create/update Class",
     *     @OA\Parameter(
     *         name="directory",
     *         in="body",
     *         description="MAP FILE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="body",
     *         description="CLASS NAME",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="expression",
     *         in="body",
     *         description="CLASS EXPRESSION",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="body",
     *         description="CLASS STATUS",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function putClass(Request $request, ParamFetcher $paramFetcher)
    {
        /* we create the two parameters we will send via GET in the URL */
        $thisFile = "success_map";
        $thisLayer = "success_layer";
        $thisClass = "1";


        /* we send the data as a request to test ApiController */
        $data = array(
            "name" => "success_class", // string
            "expression" => "[POPULATION] > 50000 AND '[LANGUAGE]' eq 'FRENCH'", // string
            "status" => "MS_DEFAULT",
        );

        $request->request->replace($data);

        try {
            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->putClass($paramFetcher, $thisFile, $thisLayer, $thisClass);

            $results instanceof JsonResponse;
            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);

    }


    /**
     * @Get("/test/deleteClass")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     *
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     *
     * @example DELETE /api/map/environnement/layer/layer1/class/1
     *
     * @Operation(
     *     tags={"Class"},
     *     summary="delete class from a layer of map file",
     *     @OA\Parameter(
     *         name="directory",
     *         in="body",
     *         description="MAP FILE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function deleteClass(ParamFetcher $paramFetcher)
    {
        try {
            $thisFile = "success_map";
            $thisLayer = "success_layer";
            $thisClass = "0";

            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->deleteClass($paramFetcher, $thisFile, $thisLayer, $thisClass);

            $results instanceof JsonResponse;

            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);
    }


    /**
     * @Get("/map/{file}/layer/{layer}/class/{class}/style/{style}")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $style style index
     *
     * @example GET /api/map/environnement/layer/layer1/class/1/style/1
     *
     * @Operation(
     *     tags={"Style"},
     *     summary="get Style information",
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getStyle(Request $request, ParamFetcher $paramFetcher)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            return $this->formatJsonError(Response::HTTP_NOT_IMPLEMENTED, 'not yet implemented');

        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }



    // PUT STYLE


    /**
     * @GET("/test/putStyle")
     *
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $style style index
     *
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     * @RequestParam(name="color", description="STYLE COLOR", nullable=true)
     * @RequestParam(name="outlinecolor", description="STYLE OUTLINECOLOR", nullable=true)
     * @RequestParam(name="symbol", description="STYLE SYMBOL", nullable=true)
     * @RequestParam(name="width", description="STYLE WIDTH", nullable=true)
     * @RequestParam(name="outlinewidth", description="STYLE OUTLINEWIDTH", nullable=true)
     * @RequestParam(name="size", description="STYLE SIZE", nullable=true)
     * @RequestParam(name="minsize", description="STYLE MINSIZE", nullable=true)
     * @RequestParam(name="maxsize", description="STYLE MAXSIZE", nullable=true)
     * @RequestParam(name="geomtransform", description="STYLE GEOMTRANSFORM", nullable=true)
     *
     * @example PUT /api/map/environnement/layer/layer1/class/1/style/1
     *
     * @Operation(
     *     tags={"Style"},
     *     summary="create/update Style",
     *     @OA\Parameter(
     *         name="directory",
     *         in="body",
     *         description="MAP FILE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="color",
     *         in="body",
     *         description="STYLE COLOR",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="outlinecolor",
     *         in="body",
     *         description="STYLE OUTLINECOLOR",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="symbol",
     *         in="body",
     *         description="STYLE SYMBOL",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="width",
     *         in="body",
     *         description="STYLE WIDTH",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="outlinewidth",
     *         in="body",
     *         description="STYLE OUTLINEWIDTH",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="size",
     *         in="body",
     *         description="STYLE SIZE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="minsize",
     *         in="body",
     *         description="STYLE MINSIZE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="maxsize",
     *         in="body",
     *         description="STYLE MAXSIZE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="geomtransform",
     *         in="body",
     *         description="STYLE GEOMTRANSFORM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */

    public function putStyle(Request $request, ParamFetcher $paramFetcher)
    {
        /* we create the two parameters we will send via GET in the URL */
        $thisFile = "success_map";
        $thisLayer = "success_layer";
        $thisClass = "0";
        $thisStyle = "0";


        /* we send the data as a request to test ApiController */
        $data = array(
            "color" => "#04B404", // string of color in HEX",
            "geomtransform" => "thisisaGEOMTRANSFORMstring",
            "maxsize" => "17.89",
            "minsize" => "14.92",
            "outlinecolor" => "#04B404", // string of color in HEX
            "outlinewidth" => "0.2 1.5",
            "size" => "15.15",
            "symbol" => "99",
            "width" => "18.25"
        );

        $request->request->replace($data);

        try {
            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->putStyle($paramFetcher, $thisFile, $thisLayer, $thisClass, $thisStyle);

            $results instanceof JsonResponse;
            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);

    }









// DELETE STYLE


    /**
     * @Get("/test/deleteStyle")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $style style index
     *
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     *
     * @example DELETE /api/map/environnement/layer/layer1/class/1/style/1
     *
     * @Operation(
     *     tags={"Style"},
     *     summary="delete style from a class of a layer mapfile",
     *     @OA\Parameter(
     *         name="directory",
     *         in="body",
     *         description="MAP FILE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */


    public function deleteStyle(Request $request, ParamFetcher $paramFetcher)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $thisFile = "success_map";
            $thisLayer = "success_layer";
            $thisClass = "0";
            $thisStyle = "0";

            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->deleteStyle($paramFetcher, $thisFile, $thisLayer, $thisClass, $thisStyle);

            $results instanceof JsonResponse;

            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);
    }

    /**
     * @Get("/map/{file}/layer/{layer}/class/{class}/label/{label}")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $label label index
     *
     * @example GET /api/map/environnement/layer/layer1/class/1/label/1
     *
     * @Operation(
     *     tags={"Label"},
     *     summary="get Label information",
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getLabel(Request $request, ParamFetcher $paramFetcher)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            return $this->formatJsonError(Response::HTTP_NOT_IMPLEMENTED, 'not yet implemented');

        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }


    /**
     * @GET("/test/putLabel")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $label label index
     *
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     * @RequestParam(name="color", description="LABEL COLOR", nullable=true)
     * @RequestParam(name="shadowcolor", description="LABEL SHADOWCOLOR", nullable=true)
     * @RequestParam(name="shadowsize", description="LABEL SHADOWSIZE", nullable=true)
     * @RequestParam(name="font", description="LABEL FONT ", nullable=true)
     * @RequestParam(name="minscaledenom", description="LABEL MINSCALEDENOM", nullable=true)
     * @RequestParam(name="maxscaledenom", description="LABEL MAXSCALEDENOM", nullable=true)
     * @RequestParam(name="size", description="LABEL SIZE", nullable=true)
     * @RequestParam(name="angle", description="LABEL ANGLE", nullable=true)
     * @RequestParam(name="force", description="LABEL FORCE", nullable=true)
     * @RequestParam(name="offset", description="LABEL OFFSET", nullable=true)
     * @RequestParam(name="position", description="LABEL POSITION", nullable=true)
     * @RequestParam(name="geomtransform", description="LABEL STYLE GEOMTRANSFORM", nullable=true)
     *
     * @example PUT /api/map/environnement/layer/layer1/class/1/label/1
     *
     * @Operation(
     *     tags={"Label"},
     *     summary="create/update Label Information",
     *     @OA\Parameter(
     *         name="directory",
     *         in="body",
     *         description="MAP FILE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="color",
     *         in="body",
     *         description="LABEL COLOR",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="shadowcolor",
     *         in="body",
     *         description="LABEL SHADOWCOLOR",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="shadowsize",
     *         in="body",
     *         description="LABEL SHADOWSIZE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="font",
     *         in="body",
     *         description="LABEL FONT",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="minscaledenom",
     *         in="body",
     *         description="LABEL MINSCALEDENOM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="maxscaledenom",
     *         in="body",
     *         description="LABEL MAXSCALEDENOM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="size",
     *         in="body",
     *         description="LABEL SIZE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="angle",
     *         in="body",
     *         description="LABEL ANGLE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="force",
     *         in="body",
     *         description="LABEL FORCE",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="offset",
     *         in="body",
     *         description="LABEL OFFSET",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="position",
     *         in="body",
     *         description="LABEL POSITION",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Parameter(
     *         name="geomtransform",
     *         in="body",
     *         description="LABEL STYLE GEOMTRANSFORM",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function putLabel(Request $request, ParamFetcher $paramFetcher)
    {
        /* we create the parameters we will send via GET in the URL */
        $thisFile = "success_map"; //string
        $thisLayer = "success_layer"; //string
        $thisClass = "0"; //integer
        $thisLabel = "2"; //string


        /* we send the data as a request to test ApiController */
        $data = array(
            "color" => "#04B404", // string of color in HEX we'll need to convert to colorObj
            "shadowcolor" => "#04B404", // string of color in HEX we'll need to convert to colorObj
            "shadowsize" => "56 65", // integer
            "font" => "comic san MS", // string
            "maxscaledenom" => "22.03", // decimal
            "minscaledenom" => "03.22", // decimal
            "size" => "666", // integer
            "angle" => "22.222", // decimal
            "force" => TRUE, // boolean
            "offset" => "18 25", // two integers separated by a space
            "position" => "29", // integer
            "geomtransfom" => "1000",
        );

        $request->request->replace($data);
        try {


            $api = new ApiController();
            $api->setContainer($this->container);

            $results = $api->putLabel($paramFetcher, $thisFile, $thisLayer, $thisClass, $thisLabel);

            $results instanceof JsonResponse;
            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);

    }





    // DELETE LABEL

    /**
     * @Get("/test/deleteLabel")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $label label index
     *
     * @RequestParam(name="directory"    , description="MAP FILE DIRECTORY", nullable=true, default=TestController::DEFAULT_DIRECTORY)
     * @example DELETE /api/map/environnement/layer/layer1/class/1/label/1
     *
     * @Operation(
     *     tags={"Label"},
     *     summary="delete label from a layer of map file",
     *     @OA\Parameter(
     *         name="directory",
     *         in="body",
     *         description="MAP FILE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function deleteLabel(Request $request, ParamFetcher $paramFetcher)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $thisFile = "success_map";
            $thisLayer = "success_layer";
            $thisClass = "0";
            $thisLabel = "0";

            $api = new ApiController();
            $api->setContainer($this->container);
            $results = $api->deleteLabel($paramFetcher, $thisFile, $thisLayer, $thisClass, $thisLabel);

            $results instanceof JsonResponse;

            if ($results->getStatusCode() !== 200) {
                throw new \Exception($results->getContent());
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
        return new Response("<br/><br/>Fin " . __METHOD__);
    }
}
