<?php

namespace Mapserver\ApiBundle\Controller;

interface MapserverDefaults {

    /**
     * The data root directory (for any user directory)
     * @var int
     */
    const DEFAULT_CHMOD = 0770;
    /**
     * The data root directory (for any user directory)
     * @var string
     */
    const ROOT_DIRECTORY = "./data/";
    /**
     * The default user directory
     * @var string
     */
    const DEFAULT_DIRECTORY = "COMMON_MAPFILES/";
    /**
     * The default fontset path, relative to the user directory
     * @var string
     */
    const DEFAULT_FONTSET = "./etc/fonts.txt";
    /**
     * The default symbolset path, relative to the userdirectory
     * @var string
     */
    const DEFAULT_SYMBOLSET = "./etc/symbols.sym";
    
    
    const MS_LAYER_TYPES = "MS_LAYER_POINT|MS_LAYER_LINE|MS_LAYER_POLYGON|MS_LAYER_RASTER|MS_LAYER_ANNOTATION|MS_LAYER_QUERY|MS_LAYER_CIRCLE|MS_LAYER_TILEINDEX|MS_LAYER_CHART";
    
}