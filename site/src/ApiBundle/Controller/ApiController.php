<?php

namespace Mapserver\ApiBundle\Controller;

use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Link;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Unlink;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;

// GET params
use FOS\RestBundle\Controller\Annotations\RequestParam;

// POST params
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Mapserver\ApiBundle\Exception\MapserverException;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Mapserver API.
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/api")
 */
class ApiController extends MapfileController
{
    /**
     * @Get("/map/{file}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     *
     * @example GET /api/map/environnement
     *
     * @Operation(
     *     tags={"Map"},
     *     summary="get Map information",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     */
    public function getMap(Request $request, ParamFetcherInterface $paramFetcher, $file)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $directory = $paramFetcher->get('directory');
            $mapObj = $this->getMapObj($directory, $file);
            $msData = $this->convertMapObj($mapObj);

            return $this->formatJsonSuccess($msData);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage() . $ex->getTraceAsString());
        }
    }

    /**
     * @Post("/map/{file}")
     *
     * @param string $file mapfile name without extension
     *
     * @RequestParam(name="directory" , description="MAP FILE DIRECTORY", nullable=true, default=ApiController::DEFAULT_DIRECTORY)
     * @RequestParam(name="name" , description="MAP NAME", nullable=false)
     * @RequestParam(name="fontset" , description="MAP FONTSET", nullable=true, default=ApiController::DEFAULT_FONTSET)
     * @RequestParam(name="symbolset" , description="MAP SYMBOLSET", nullable=true, default=ApiController::DEFAULT_SYMBOLSET)
     * @RequestParam(name="imagecolor" , description="MAP IMAGECOLOR", nullable=false)
     * @RequestParam(name="imagetype" , description="MAP IMAGETYPE", nullable=false)
     * #RequestParam(name="transparent" , description="MAP TRANSPARENCY", nullable=false)
     * @RequestParam(name="extent" , description="MAP EXTENT", nullable=false)
     * @RequestParam(name="units" , description="MAP UNITS", nullable=false)
     * @RequestParam(name="projection" , description="PROJECTION", nullable=false)
     * @RequestParam(name="maxscaledenom", description="WEB MAXSCALEDENOM", nullable=true)
     * @RequestParam(name="minscaledenom", description="WEB MINSCALEDENOM", nullable=true)
     * @RequestParam(name="metadata" , description="WEB METADATA (JSON OBJECT {name:value})", nullable=true)
     * @RequestParam(name="template" , description="WEB TEMPLATE", nullable=true)
     * @RequestParam(name="reference" , description="REFERENCE MAP (JSON OBJECT)", nullable=true)
     *
     * @example PUT /api/map/environnement
     *
     * @Operation(
     *     tags={"Map"},
     *     summary="create/update Map",
     *      @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="directory",
     *                     description="MAP FILE DIRECTORY",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     description="MAP NAME",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="fontset",
     *                     description="MAP FONTSET",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="symbolset",
     *                     description="MAP SYMBOLSET",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="imagecolor",
     *                     description="MAP IMAGECOLOR",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="imagetype",
     *                     description="MAP IMAGETYPE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="extent",
     *                     description="MAP EXTENT",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="units",
     *                     description="MAP UNITS",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="projection",
     *                     description="PROJECTION",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="maxscaledenom",
     *                     description="WEB MAXSCALEDENOM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="minscaledenom",
     *                     description="WEB MINSCALEDENOM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="metadata",
     *                     description="WEB METADATA (JSON OBJECT {name:value})",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="template",
     *                     description="WEB TEMPLATE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="reference",
     *                     description="REFERENCE MAP (JSON OBJECT)",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     */
    public function putMap(Request $request, ParamFetcher $paramFetcher, $file)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            /*
             * Request values
             */
            $directory = $paramFetcher->get('directory'); // should be absolute path to directory
            $mapfile = $file;
            $mapObj = $this->getMapObj($directory, $file, false);

            $msData = parent::putMapInFile(new ParameterBag($paramFetcher->all()), $mapObj, $directory, $mapfile);

            $this->saveMap($mapObj, $file, "When edit map definition");

            return $this->formatJsonSuccess($msData);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage(), $paramFetcher->all(false));
        }
    }

    /**
     * @Post("/locator/{file}")
     *
     * @param string $file mapfile name without extension
     *
     * @RequestParam(name="directory" , description="MAP FILE DIRECTORY", nullable=true, default=ApiController::DEFAULT_DIRECTORY)
     * @RequestParam(name="locators" , description="Locators params", nullable=true)
     *
     * @example POST /api/locator
     *
     * @Operation(
     *     tags={"Locator"},
     *     summary="create/update Locators in mapfile",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="directory",
     *                     description="MAP FILE DIRECTORY",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="locators",
     *                     description="Locators params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function postLocator(Request $request, ParamFetcher $paramFetcher, $file)
    {

        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            /*
             * Request values
             */
            $directory = $paramFetcher->get('directory'); // should be absolute path to directory
            $locators = $paramFetcher->get('locators');

            $mapfile = $file;
            $mapObj = $this->getMapObj($directory, $file, false);
            //remove existing layers
            for ($i = 0; $i < $mapObj->numlayers; $i++) {
                if (preg_match('/_locator$/', $mapObj->getLayer($i)->name)) {
                    $mapObj->getLayer($i)->set("status", MS_DELETE);
                }
            }
            foreach ($locators as $key => $locator) {
                $locatorRank = $locator["locatorRank"];
                $layerName = $locator["layername"];
                $layerFields = $locator["layerFields"];
                $orderField = $locator["orderField"];
                $filterField = $locator["filterField"];

                $layerToCopy = @$mapObj->getLayerByName($layerName);

                //copy and adjust layer
                $msLocatorLayer = ms_newLayerObj($mapObj, $layerToCopy);
                $msLocatorLayer->set("name", $layerName . "_" . $locatorRank . "_locator");
                if ($msLocatorLayer->classitem && $msLocatorLayer->classitem != "") {
                    $layerFields[] = $msLocatorLayer->classitem;
                }
                if ($msLocatorLayer->labelitem && $msLocatorLayer->labelitem != "") {
                    $layerFields[] = $msLocatorLayer->labelitem;
                }
                //add gid
                $layerFields[] = "gid";
                //delete duplicate
                $layerFields = array_unique($layerFields);
                //remove order field
                $layerFields = array_diff($layerFields, array($orderField));

                if ($msLocatorLayer->type !== MS_LAYER_POINT) {
                    $msLocatorLayer->set("data",
                        str_replace(") as foo using unique", " order by " . $orderField . ") as foo using unique",
                            str_replace("the_geom from (select * from ",
                                "the_geom from (select distinct on (" . $orderField .
                                ($filterField != "" ? "," . $filterField : "") .
                                ") " . $orderField . ",  ST_Envelope(the_geom) as the_geom, " . implode(", ", $layerFields) . " from ", $msLocatorLayer->data)));

                } else {

                    //transform data in polygon and do it for all srs (4326 for instance)
                    if ($msLocatorLayer->getProjection() !== "+init=epsg:4326") {
                        $msLocatorLayer->set("data",
                            str_replace(") as foo using unique", " order by " . $orderField . ") as foo using unique",
                                str_replace("the_geom from (select * from ",
                                    "the_geom from (select distinct on (" . $orderField .
                                    ($filterField != "" ? "," . $filterField : "") .
                                    ") " . $orderField . ",  ST_Envelope(st_buffer(the_geom, 100)) as the_geom,  " . implode(", ", $layerFields) . " from ", $msLocatorLayer->data)));

                    } else {
                        $msLocatorLayer->set("data",
                            str_replace(") as foo using unique", " order by " . $orderField . ") as foo using unique",
                                str_replace("the_geom from (select * from ",
                                    "the_geom from (select distinct on (" . $orderField .
                                    ($filterField != "" ? "," . $filterField : "") .
                                    ") " . $orderField . ",  ST_Envelope(cast(  st_buffer(cast(the_geom as geography), 100) as geometry)) as the_geom, " . implode(", ", $layerFields) . " from ", $msLocatorLayer->data)));
                    }

                }

                $msLocatorLayer->set("type", MS_LAYER_POLYGON);

                $msLocatorLayer->setMetaData("ows_include_items", "all");
                $msLocatorLayer->setMetaData("gml_include_items", "all");
                $msLocatorLayer->setMetaData("gml_types", "auto");

            }


            $this->saveMap($mapObj, $file, "When adding locators");

            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage(), $paramFetcher->all(false));
        }
    }


    /**
     * @Delete("/map/{file}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     *
     * @example DELETE /api/map/environnement
     *
     * @Operation(
     *     tags={"Map"},
     *     summary="delete map file",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(@OA\Schema(type="string"))
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function deleteMap(Request $request, ParamFetcher $paramFetcher, $file)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            $directory = $paramFetcher->get('directory'); // should be absolute path to directory

            $file = $directory . str_replace(".map", "", $file) . '.map';

            if (file_exists($file)) {
                $delete_is_succesful = unlink($file);
                if (!$delete_is_succesful) {
                    throw new MapserverException($this->_t(sprintf("An error occured when trying to delete the map file at : '%s'.", $file)));
                }
            }

            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    /**
     * @Get("/map/{file}/reference")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     *
     * @example GET /api/map/environnement/reference
     *
     * @Operation(
     *     tags={"Reference"},
     *     summary="get reference map information",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getReference(Request $request, ParamFetcher $paramFetcher, $file)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $directory = $paramFetcher->get('directory');
            $mapObj = $this->getMapObj($directory, $file);

            $msData = $this->convertReferenceMapObj($mapObj, $mapObj->reference);

            return $this->formatJsonSuccess($msData);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    // PUT REFERENCE

    /**
     * @Put("/map/{file}/reference")
     *
     * Updates the reference map. If given image file is null or empty,
     * a new reference map image will be generated and returned.
     *
     * @param string $file mapfile name without extension
     *
     * @RequestParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     * @RequestParam(name="image", description="REFERENCE IMAGE", nullable=true, default=null)
     * @RequestParam(name="extent", description="REFERENCE EXTENT", nullable=false)
     * @RequestParam(name="size", description="REFERENCE SIZE", nullable=false)
     * @RequestParam(name="color", description="REFERENCE COLOR", nullable=true)
     * @RequestParam(name="outlinecolor", description="REFERENCE OUTLINECOLOR", nullable=true)
     * @RequestParam(name="status", description="REFERENCE STATUS", nullable=true, default="ON")
     *
     * @example PUT /api/environnement/reference/
     * @example only PUT since Instances of referenceMapObj are always embedded inside the mapObj.
     *
     * @Operation(
     *     tags={"Reference"},
     *     summary="update Reference Map",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="directory",
     *                     description="REFERENCE DIRECTORY",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="image",
     *                     description="REFERENCE IMAGE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="extent",
     *                     description="REFERENCE EXTENT",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="size",
     *                     description="REFERENCE SIZE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="color",
     *                     description="REFERENCE COLOR",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="outlinecolor",
     *                     description="REFERENCE OUTLINECOLOR",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="status",
     *                     description="REFERENCE STATUS",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function putReference(Request $request, ParamFetcher $paramFetcher, $file)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            // Get parameter values (alphabetical order)
            $directory = $paramFetcher->get('directory');
            $mapfile = $file;
            $mapObj = $this->getMapObj($directory, $file, false);

            $msData = $this->putReferenceInMap(new ParameterBag($paramFetcher->all()), $mapObj, $mapfile);

            // Updating and saving the new map object
            $this->saveMap($mapObj, $file, "When adding/editing a class");

            return $this->formatJsonSuccess($msData);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage(), $paramFetcher->all(false));
        }
    }

    /**
     * @Get("/map/{file}/reference/generate")
     *
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     * @QueryParam(name="extent" , description="REFERENCE EXTENT", nullable=true, default=null)
     * @QueryParam(name="size", description="REFERENCE SIZE", nullable=true, default=null)
     * @QueryParam(name="hiddens", description="STATE OF VISIBILITY FOR EACH LAYERS", nullable=true)
     *
     * @param string $file mapfile name without extension
     *
     * @Operation(
     *     tags={"Reference"},
     *     summary="Generate a Reference Map image and returns it base64 encoded",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Parameter(
     *         name="extent",
     *         in="query",
     *         description="REFERENCE EXTENT",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Parameter(
     *         name="size",
     *         in="query",
     *         description="REFERENCE SIZE",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Parameter(
     *         name="hiddens",
     *         in="query",
     *         description="STATE OF VISIBILITY FOR EACH LAYERS",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     *
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @param unknown $file
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function generateReferenceMap(Request $request, ParamFetcher $paramFetcher, $file)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $extent = $paramFetcher->get('extent', null);
            $size = $paramFetcher->get('size', "200 200");
            $hiddens = $paramFetcher->get('hiddens', array());

            $directory = $paramFetcher->get('directory');
            $initFile = $file;
            $radicalname = pathinfo($file, PATHINFO_FILENAME);
            $this->getLogger()->info(__METHOD__, array($radicalname));
            $filetemp = uniqid($radicalname . '_ref_') . '.map';

            $mapObj = $this->getMapObj($directory, $file);

            // change the visibility of layers before generation
            foreach ($hiddens as $layerId) {
                $oLayer = $mapObj->getLayerByName($layerId);
                $mapObj->removeLayer($oLayer->index);
            }

            for ($iLayer = 0; $iLayer < $mapObj->numlayers; $iLayer++) {
                $oLayer = $mapObj->getLayer($iLayer);
                if (empty($hiddens)) {
                    try {
                        $oLayer->open();
                        $oLayer->close();
                    } catch (\Exception $exception) {
                        $mapObj->removeLayer($oLayer->index);
                        continue;
                    }
                }
                $oLayer->set('maxscaledenom', null);
                $oLayer->set('minscaledenom', null);
                $oLayer->set('status', MS_ON);
                /*if ( $oLayer->isVisible() ){
                    $oLayer->set('maxscaledenom', null);
                    $oLayer->set('minscaledenom', null);
                    //$oLayer->set('status', MS_ON);
                }else {
                    $mapObj->removeLayer($oLayer->index);
                }*/
            }
            $this->saveMap($mapObj, $directory . '/' . $filetemp, 'Reference map generation', true);

            list($imageObj, $extent) = array_values($this->getReferenceImageObj($directory, $filetemp, $extent, $size));

            @unlink($directory . '/' . $filetemp);
            // get reference map image format
            $ext = $mapObj->outputformat->extension;
            $filename = $directory . '/reference/reference_' . $radicalname . '.' . $ext;
            //var_dump($imageObj, $filename);
            $imageObj && $imageObj->saveImage($filename);
            $type = pathinfo($filename, PATHINFO_EXTENSION);

            if ($request->query->get("debug", "0")) {
                header("Content-type: image/$type");
                readfile($filename);
                exit();
            }
            $data = file_get_contents($filename);
            $image = 'data:image/' . $type . ';base64,' . base64_encode($data);

            $imageObj = null;
            $imagesize = getimagesize($filename);

            return $this->formatJsonSuccess(array(
                'data' => $image,
                "extent" => $extent,
                "image" => './' . ltrim(str_replace($directory, "", $filename), '/'),
                "width" => $imagesize[0],
                "height" => $imagesize[1],
            ));
        } catch (\Exception $ex) {
            throw $ex;
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage() . $ex->getTraceAsString(), $paramFetcher->all(false));
        }
    }

    /**
     * @Get("/map/{file}/layer/{layer}")
     *
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     *
     * @example GET /api/map/environnement/layer/layer1
     *
     * @Operation(
     *     tags={"Layer"},
     *     summary="get Layer information",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getLayer(Request $request, ParamFetcher $paramFetcher, $file, $layer)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $directory = $paramFetcher->get('directory');

            $mapObj = $this->getMapObj($directory, $file);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            $msData = $this->convertLayerObj($mapObj, $layerObj);
            return $this->formatJsonSuccess($msData);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    /**
     * @Get("/map/{file}/layer/{layer}/fields")
     *
     * @QueryParam(name="directory" , description="MAP FILE DIRECTORY", nullable=true, default=ApiController::DEFAULT_DIRECTORY)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     *
     * @example GET /api/map/environnement/layer/layer1/fields
     *
     * @Operation(
     *     tags={"Layer"},
     *     summary="get Layer Fields information",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="MAP FILE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getLayerFields(Request $request, ParamFetcher $paramFetcher, $file, $layer)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            $directory = $paramFetcher->get("directory");
            $mapObj = $this->getMapObj($directory, $file);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            list($layerData, $layerName) = self::_getDataForPhpOGR($mapObj, $layerObj, true, true);

            $result = $this->_getLayerFields($layerData, $layerName);

            return $this->formatJsonSuccess($result);

        } catch (\Exception $ex) {
            throw $ex;
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    /**
     * Returns the types of the fields for the layer given
     * @param  $strfilename source file
     * @return $FieldsTypes : array (field_name=>type(ogr) of the field)
     */

    protected function getOgrFieldTypes($strfilename, $layerIndex = 0)
    {

        $FieldTypes = array();
        $layerType = -1;
        // Register all drivers
        OGRRegisterAll();

        // Open data source
        $hSFDriver = NULL;
        $hDatasource = OGROpen($strfilename, 0, $hSFDriver);

        if (!$hDatasource) {
            return -1;
        }

        $hLayer = OGR_DS_GetLayer($hDatasource, $layerIndex);
        // Get Layer Definition
        $hLayerDefn = OGR_L_GetLayerDefn($hLayer);
        $nb_field = 0;
        $nb_field = OGR_FD_GetFieldCount($hLayerDefn);
        $this->getLogger()->debug(__METHOD__, compact("strfilename", "layerIndex", "hLayerDefn", "nb_field"));
        for ($i = 0; $i < $nb_field; $i++) {
            $hFieldDefn = OGR_FD_GetFieldDefn($hLayerDefn, $i);
            $hFieldName = OGR_Fld_GetNameRef($hFieldDefn);
            //detect source encoding and transform it
            $encoding = mb_detect_encoding($hFieldName);
            $hFieldName = iconv($encoding, 'UTF-8', $hFieldName);

            $hFieldType = OGR_Fld_GetType($hFieldDefn);
            $hFieldTypeName = OGR_GetFieldTypeName($hFieldType);
            $FieldTypes[$hFieldName] = $hFieldTypeName;
        }

        return $FieldTypes;
    }

//     PUT LAYER //

    /**
     * @Put("/map/{file}/layer/{layer}")
     *
     * @RequestParam(name="directory", description="LAYER DIRECTORY", nullable=false)
     * @RequestParam(name="name", description="LAYER NAME", nullable=false)
     * @RequestParam(name="type", description="LAYER TYPE", nullable=false)
     * @RequestParam(name="projection", description="LAYER PROJECTION", nullable=false)
     * @RequestParam(name="status", description="LAYER STATUS", nullable=true, default="ON")
     * @RequestParam(name="sizeunits", description="LAYER SIZE UNITS", nullable=true, default="PIXELS")
     *
     * @RequestParam(name="connectiontype", description="LAYER CONNECTIONTYPE", nullable=true, default="INLINE")
     * @RequestParam(name="connection", description="LAYER CONNECTION", nullable=true)
     * @RequestParam(name="data", description="LAYER DATA", nullable=true)
     * @RequestParam(name="tileindex", description="LAYER TILEINDEX", nullable=true)
     * @RequestParam(name="tileitem", description="LAYER TILEITEM", nullable=true)
     * @RequestParam(name="maxscaledenom", description="LAYER MAXSCALEDENOM", nullable=true)
     * @RequestParam(name="minscaledenom", description="LAYER MINSCALEDENOM", nullable=true)
     * @RequestParam(name="labelitem", description="LAYER LABELITEM", nullable=true, default=null)
     * @RequestParam(name="offsite", description="LAYER OFFSITE", nullable=true)
     * @RequestParam(name="processing", description="LAYER PROCESSING", nullable=true)
     * @RequestParam(name="symbolscaledenom", description="LAYER SYMBOLSCALEDENOM", nullable=true)
     * @RequestParam(name="symbolscaledenompixels", description="LAYER SYMBOLSCALEDENOM", nullable=true)
     * @RequestParam(name="tolerance", description="LAYER TOLERANCE", nullable=true)
     * @RequestParam(name="template", description="LAYER TEMPLATE", nullable=true)
     * @RequestParam(name="toleranceunits", description="LAYER TOLERANCEUNITS", nullable=true)
     * @RequestParam(name="classitem", description="LAYER CLASSITEM", nullable=true)
     * @RequestParam(name="labelitem", description="LAYER LABELITEM", nullable=true)
     * @RequestParam(name="encoding", description="LAYER DATA ENCODING", nullable=true)
     * @RequestParam(name="metadata", description="LAYER METADATA", nullable=true)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     *
     * @example PUT /api/environnement/layer/layer1
     *
     * @Operation(
     *     tags={"Layer"},
     *     summary="create/update Layer",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="directory",
     *                     description="LAYER DIRECTORY",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     description="LAYER NAME",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="type",
     *                     description="LAYER TYPE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="projection",
     *                     description="LAYER PROJECTION",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="status",
     *                     description="LAYER STATUS",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="sizeunits",
     *                     description="LAYER SIZE UNITS",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="connectiontype",
     *                     description="LAYER CONNECTIONTYPE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="connection",
     *                     description="LAYER CONNECTION",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     description="LAYER DATA",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="tileindex",
     *                     description="LAYER TILEINDEX",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="tileitem",
     *                     description="LAYER TILEITEM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="maxscaledenom",
     *                     description="LAYER MAXSCALEDENOM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="minscaledenom",
     *                     description="LAYER MINSCALEDENOM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="labelitem",
     *                     description="LAYER LABELITEM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="offsite",
     *                     description="LAYER OFFSITE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="processing",
     *                     description="LAYER PROCESSING",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="symbolscaledenom",
     *                     description="LAYER SYMBOLSCALEDENOM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="symbolscaledenompixels",
     *                     description="LAYER SYMBOLSCALEDENOMPIXELS",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="tolerance",
     *                     description="LAYER TOLERANCE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="template",
     *                     description="LAYER TEMPLATE",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="toleranceunits",
     *                     description="LAYER TOLERANCEUNITS",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="classitem",
     *                     description="LAYER CLASSITEM",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="encoding",
     *                     description="LAYER DATA ENCODING",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="metadata",
     *                     description="LAYER METADATA",
     *                     type="string"
     *                 ),
     *             )
     *         )
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function putLayer(Request $request, ParamFetcher $paramFetcher, $file, $layer)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        $this->getLogger()->debug(__METHOD__, $request->request->all());
        $this->getLogger()->debug(__METHOD__, $paramFetcher->all());
        try {
            // Get parameter values
            $directory = $paramFetcher->get('directory');

            $mapObj = $this->getMapObj($directory, $file, false, true);

            // Save/create layerObj
            $this->putLayerInMap(new ParameterBag($paramFetcher->all()), $mapObj, $layer);

            // Updating and saving the new map object
            $this->saveMap($mapObj, $file, "When adding/editing a layer");

            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage(), array("params" => $paramFetcher->all(false), "stacktrace" => $ex->getTraceAsString()));
        }
    }

    // DELETE LAYER

    /**
     * @Delete("/map/{file}/layer/{layer}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     *
     * @example DELETE /api/environnement/layer/layer1
     *
     * @Operation(
     *     tags={"Layer"},
     *     summary="delete layer from map file",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function deleteLayer(Request $request, ParamFetcher $paramFetcher, $file, $layer)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $directory = $paramFetcher->get('directory'); // should be absolute path to directory

            $mapObj = $this->getMapObj($directory, $file);
            $layerObj = $this->getLayerObj($mapObj, $layer);

            $delete_is_succesful = $mapObj->removeLayer($layerObj->index);
            $layerObj->free();

            if (!($delete_is_succesful && $this->saveMap($mapObj, $file, "When delete layer"))) {
                throw new MapserverException($this->_t(sprintf("An error occured when trying to delete a layer from the map file at : '%s'.", $file)));
            }
            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    /**
     * @Get("/map/{file}/layer/{layer}/class/{class}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     *
     * @example GET /api/map/environnement/layer/layer1/class/1
     *
     * @Operation(
     *     tags={"Class"},
     *     summary="get Class information",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getClass(Request $request, ParamFetcher $paramFetcher, $file, $layer, $class)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            // Get parameter values (alphabetical order)
            $directory = $paramFetcher->get('directory');

            $mapObj = $this->getMapObj($directory, $file);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            $classObj = $this->getClassObj($mapObj, $layerObj, $class);

            //Get a class in layerObj
            $msData = $this->convertClassObj($classObj);

            return $this->formatJsonSuccess($msData);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    // PUT CLASS

    /**
     * @Put("/map/{file}/layer/{layer}/class/{class}")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     *
     * @RequestParam(name="directory", description="CLASS DIRECTORY", nullable=false)
     * @RequestParam(name="name", description="CLASS NAME", nullable=false)
     * @RequestParam(name="title", description="CLASS TITLE", nullable=true)
     * @RequestParam(name="expression", description="CLASS EXPRESSION", nullable=false)
     * @RequestParam(name="status", description="CLASS STATUS", nullable=true, default="ON")
     *
     * @example PUT /api/map/environnement/layer/layer1/class/1
     *
     * @Operation(
     *     tags={"Class"},
     *     summary="create/update Class",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="directory",
     *                      description="CLASS DIRECTORY",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="name",
     *                      description="CLASS NAME",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="title",
     *                      description="CLASS TITLE",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="expression",
     *                      description="CLASS EXPRESSION",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      description="CLASS STATUS",
     *                      type="string"
     *                  )
     *             )
     *         )
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function putClass(Request $request, ParamFetcher $paramFetcher, $file, $layer, $class)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            // Get parameter values (alphabetical order)
            $directory = $paramFetcher->get('directory');
            $mapObj = $this->getMapObj($directory, $file, false);
            $layerObj = $this->getLayerObj($mapObj, $layer, false);

            // Save/create classObj
            $this->putClassInLayer(new ParameterBag($paramFetcher->all()), $mapObj, $layerObj, $class);

            // Updating and saving the new map object
            $this->saveMap($mapObj, $file, "When adding/editing a class");

            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage(), $paramFetcher->all(false));
        }
    }
    // DELETE CLASS

    /**
     * @Delete("/map/{file}/layer/{layer}/class/{class}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     *
     * @example DELETE /api/map/environnement/layer/layer1/class/1
     *
     * @Operation(
     *     tags={"Class"},
     *     summary="delete class from a layer of map file",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function deleteClass(Request $request, ParamFetcher $paramFetcher, $file, $layer, $class)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $directory = $paramFetcher->get('directory'); // should be absolute path to directory

            $mapObj = $this->getMapObj($directory, $file);
            $layerObj = $this->getLayerObj($mapObj, $layer);

            $delete_is_succesful = !is_null($layerObj->removeClass($class));

            if (!($delete_is_succesful && $this->saveMap($mapObj, $file, "When delete class"))) {
                throw new MapserverException($this->_t(sprintf("An error occured when trying to delete a class from the map file at : '%s'.", $file)));
            }

            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    /**
     * @Get("/map/{file}/layer/{layer}/class/{class}/style/{style}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $style style index
     *
     * @example GET /api/map/environnement/layer/layer1/class/1/style/1
     *
     * @Operation(
     *     tags={"Style"},
     *     summary="get Style information",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getStyle(Request $request, ParamFetcher $paramFetcher, $file, $layer, $class, $style)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $directory = $paramFetcher->get('directory');
            $mapObj = $this->getMapObj($directory, $file);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            $classObj = $this->getClassObj($mapObj, $layerObj, $class);
            $styleObj = $this->getStyleObj($classObj, $style);

            $msData = $this->convertStyleObj($styleObj);
            return $this->formatJsonSuccess($msData);
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    // PUT STYLE

    /**
     * @Put ("/map/{file}/layer/{layer}/class/{class}/style/{style}")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $style style index
     *
     * @RequestParam(name="directory", description="LAYER DIRECTORY", nullable=false)
     * @RequestParam(name="color", description="STYLE COLOR", nullable=true)
     * @RequestParam(name="outlinecolor", description="STYLE OUTLINECOLOR", nullable=true)
     * @RequestParam(name="symbol", description="STYLE SYMBOL", nullable=true)
     * @RequestParam(name="width", description="STYLE WIDTH", nullable=true)
     * @RequestParam(name="outlinewidth", description="STYLE OUTLINEWIDTH", nullable=true)
     * @RequestParam(name="size", description="STYLE SIZE", nullable=true)
     * @RequestParam(name="angle", description="STYLE ANGLE", nullable=true)
     * @RequestParam(name="minsize", description="STYLE MINSIZE", nullable=true)
     * @RequestParam(name="maxsize", description="STYLE MAXSIZE", nullable=true)
     * @RequestParam(name="pattern", description="STYLE PATTERN", nullable=true)
     * @RequestParam(name="geomtransform", description="STYLE GEOMTRANSFORM", nullable=true)
     *
     * @example PUT /api/map/environnement/layer/layer1/class/1/style/1
     *
     * @Operation(
     *     tags={"Style"},
     *     summary="create/update Style",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                   @OA\Property(
     *                      property="directory",
     *                      description="LAYER DIRECTORY",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="color",
     *                      description="STYLE COLOR",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="outlinecolor",
     *                      description="STYLE OUTLINECOLOR",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="symbol",
     *                      description="STYLE SYMBOL",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="width",
     *                      description="STYLE WIDTH",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="outlinewidth",
     *                      description="STYLE OUTLINEWIDTH",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="size",
     *                      description="STYLE SIZE",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="angle",
     *                      description="STYLE ANGLE",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="minsize",
     *                      description="STYLE MINSIZE",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="maxsize",
     *                      description="STYLE MAXSIZE",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="pattern",
     *                      description="STYLE PATTERN",
     *                      type="string"
     *                ),
     *                 @OA\Property(
     *                      property="geomtransform",
     *                      description="STYLE GEOMTRANSFORM",
     *                      type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function putStyle(Request $request, ParamFetcher $paramFetcher, $file, $layer, $class, $style)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            // Get parameter values (alphabetical order)
            $directory = $paramFetcher->get('directory');

            $mapObj = $this->getMapObj($directory, $file, false);
            $layerObj = $this->getLayerObj($mapObj, $layer, false);
            $classObj = $this->getClassObj($mapObj, $layerObj, $class, false);

            // Save/create styleObj
            $this->putStyleInClass(new ParameterBag($paramFetcher->all()), $classObj, $style);

            // Updating and saving the new map object
            $this->saveMap($mapObj, $file, "When adding/editing a style");

            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage(), $paramFetcher->all(false));
        }
    }

    // DELETE STYLE

    /**
     * @Delete("/map/{file}/layer/{layer}/class/{class}/style/{style}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $style style index
     *
     * @example DELETE /api/map/environnement/layer/layer1/class/1/style/1
     *
     * @Operation(
     *     tags={"Style"},
     *     summary="delete style from a class of a layer mapfile",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function deleteStyle(Request $request, ParamFetcher $paramFetcher, $file, $layer, $class, $style)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $directory = $paramFetcher->get('directory'); // should be absolute path to directory

            $mapObj = $this->getMapObj($directory, $file);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            $classObj = $this->getClassObj($mapObj, $layerObj, $class);

            $delete_is_succesful = !is_null($classObj->removeStyle($class));

            if (!($delete_is_succesful && $this->saveMap($mapObj, $file, "When delete style"))) {
                throw new MapserverException($this->_t(sprintf("An error occured when trying to delete a style from the map file at : '%s'.", $file)));
            }
            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    /**
     * @Get("/map/{file}/layer/{layer}/class/{class}/label/{label}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $label label index
     *
     * @example GET /api/map/environnement/layer/layer1/class/1/label/1
     *
     * @Operation(
     *     tags={"Label"},
     *     summary="get Label information",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function getLabel(Request $request, ParamFetcher $paramFetcher, $file, $layer, $class, $label)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $directory = $paramFetcher->get('directory');

            $mapObj = $this->getMapObj($directory, $file);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            $classObj = $this->getClassObj($mapObj, $layerObj, $class);
            $labelObj = $this->getLabelObj($classObj, $label);

            $msData = $this->convertLabelObj($labelObj);
            return $this->formatJsonSuccess($msData);

        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    // PUT LABEL

    /**
     * @Put("/map/{file}/layer/{layer}/class/{class}/label/{label}")
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $label label index
     *
     * @RequestParam(name="directory", description="LABEL DIRECTORY", nullable=false)
     * @RequestParam(name="color", description="LABEL COLOR", nullable=true)
     * @RequestParam(name="shadowcolor", description="LABEL SHADOWCOLOR", nullable=true)
     * @RequestParam(name="shadowsizex", description="LABEL SHADOWSIZE X", nullable=true, default=-1)
     * @RequestParam(name="shadowsizey", description="LABEL SHADOWSIZE Y", nullable=true, default=-1)
     * @RequestParam(name="font", description="LABEL FONT ", nullable=true)
     * @RequestParam(name="minscaledenom", description="LABEL MINSCALEDENOM", nullable=true, default=-1)
     * @RequestParam(name="maxscaledenom", description="LABEL MAXSCALEDENOM", nullable=true, default=-1)
     * @RequestParam(name="size", description="LABEL SIZE", nullable=true)
     * @RequestParam(name="angle", description="LABEL ANGLE", nullable=true)
     * @RequestParam(name="force", description="LABEL FORCE", nullable=true)
     * @RequestParam(name="offsetx", description="LABEL OFFSETX", nullable=true, default=-1)
     * @RequestParam(name="offsety", description="LABEL OFFSETY", nullable=true, default=-1)
     * @RequestParam(name="position", description="LABEL POSITION", nullable=true)
     * @RequestParam(name="encoding", description="LABEL DATA ENCODING", nullable=true)
     * @RequestParam(name="geomtransform", description="LABEL STYLE GEOMTRANSFORM", nullable=true)
     *
     * @example PUT /api/map/environnement/layer/layer1/class/1/label/1
     *
     * @Operation(
     *     tags={"Label"},
     *     summary="create/update Label Information",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="directory",
     *                      description="LABEL DIRECTORY",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="color",
     *                      description="LABEL COLOR",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="shadowcolor",
     *                      description="LABEL SHADOWCOLOR",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="shadowsizex",
     *                      description="LABEL SHADOWSIZE X",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="shadowsizey",
     *                      description="LABEL SHADOWSIZE Y",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="font",
     *                      description="LABEL FONT",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="minscaledenom",
     *                      description="LABEL MINSCALEDENOM",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="maxscaledenom",
     *                      description="LABEL MAXSCALEDENOM",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="size",
     *                      description="LABEL SIZE",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="angle",
     *                      description="LABEL ANGLE",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="force",
     *                      description="LABEL FORCE",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="offsetx",
     *                      description="LABEL OFFSETX",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="offsety",
     *                      description="LABEL OFFSETY",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="position",
     *                      description="LABEL POSITION",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="encoding",
     *                      description="LABEL DATA ENCODING",
     *                      type="string"
     *                  ),
     *                 @OA\Property(
     *                      property="geomtransform",
     *                      description="LABEL STYLE GEOMTRANSFORM",
     *                      type="string"
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function putLabel(Request $request, ParamFetcher $paramFetcher, $file, $layer, $class, $label)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            // Get parameter values (alphabetical order)
            $directory = $paramFetcher->get('directory');

            $mapObj = $this->getMapObj($directory, $file, false);
            $layerObj = $this->getLayerObj($mapObj, $layer, false);
            $classObj = $this->getClassObj($mapObj, $layerObj, $class, false);

            $this->putLabelInClass(new ParameterBag($paramFetcher->all()), $classObj, $label);

            // Updating and saving the new map object
            $this->saveMap($mapObj, $file, "When adding/editing a label");

            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage(), $paramFetcher->all(false));
        }
    }

    // DELETE LABEL

    /**
     * @Delete("/map/{file}/layer/{layer}/class/{class}/label/{label}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     *
     * @param string $file mapfile name without extension
     * @param string $layer layer name
     * @param string $class class index
     * @param string $label label index
     *
     * @example DELETE /api/map/environnement/layer/layer1/class/1/label/1
     *
     * @Operation(
     *     tags={"Label"},
     *     summary="delete label from a layer of map file",
     *     @OA\Parameter(
     *         name="directory",
     *         in="query",
     *         description="REFERENCE DIRECTORY",
     *         required=false,
     *         @OA\Schema(type="string")
     *                ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function deleteLabel(Request $request, ParamFetcher $paramFetcher, $file, $layer, $class, $label)
    {
        // always put your code in a try catch block to be sure to always return a JsonResponse
        try {
            $directory = $paramFetcher->get('directory'); // should be absolute path to directory

            $mapObj = $this->getMapObj($directory, $file);
            $layerObj = $this->getLayerObj($mapObj, $layer);
            $classObj = $this->getClassObj($mapObj, $layerObj, $class);

            $delete_is_succesful = !is_null($classObj->removeLabel($class));

            if (!($delete_is_succesful && $this->saveMap($mapObj, $file, "When delete label"))) {
                throw new MapserverException($this->_t(sprintf("An error occured when trying to delete a label from the map file at : '%s'.", $file)));
            }
            return $this->formatJsonSuccess();
        } catch (\Exception $ex) {
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

    /**
     * @Get("/copylayers/{mapfile_to_write}/{mode}")
     * @QueryParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     * @QueryParam(name="layers_ms_new_names_to_add", description="Layers new names to copy", nullable=false)
     * @QueryParam(name="map_name_to_add", description="map name from which to coppy", nullable=false)
     * @QueryParam(name="mapname", description="map name writen", nullable=false)
     * @QueryParam(name="layers_msnames_to_delete", description="Layers names to delete", nullable=true)
     */
    public function get_copyLayers(Request $request, ParamFetcher $paramFetcher, $mapfile_to_write, $mode)
    {
        return $this->copyLayers($request, $paramFetcher, $mapfile_to_write, $mode);
    }

    /**
     * @Post("/copylayers/{mapfile_to_write}/{mode}")
     * @RequestParam(name="directory", description="REFERENCE DIRECTORY", nullable=false)
     * @RequestParam(name="layers_ms_new_names_to_add", description="Layers new names to copy", nullable=false)
     * @RequestParam(name="map_name_to_add", description="map name from which to coppy", nullable=false)
     * @RequestParam(name="mapname", description="map name writen", nullable=false)
     * @RequestParam(name="layers_msnames_to_delete", description="Layers names to delete", nullable=true)
     *
     * @param string $mapfile_to_write mapfile source name without extension
     * @param string $destination mapfile destination name without extension
     *
     * @example app_dev.php/api/copylayers/temp_admin_2_638/mode/Add
     *
     * @Operation(
     *     tags={"Copy"},
     *     summary="copy layers of map file source to map file destination",
     *      @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="directory",
     *                      description="REFERENCE DIRECTORY",
     *                      type="string"
     *                 ),
     *                  @OA\Property(
     *                      property="layers_ms_new_names_to_add",
     *                      description="Layers new names to copy",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="map_name_to_add",
     *                      description="map name from which to coppy",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="mapname",
     *                      description="map name writen",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="layers_msnames_to_delete",
     *                      description="Layers names to delete",
     *                      type="string"
     *                  ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     */
    public function copyLayers(Request $request, ParamFetcher $paramFetcher, $mapfile_to_write, $mode)
    {
        //Always put your code in a try catch block to be sure to always return a JsonResponse
        try {

            $directory = $paramFetcher->get('directory'); // should be absolute path to directory
            $mapname = $paramFetcher->get('mapname');
            $layers_ms_new_names_to_add = $paramFetcher->get('layers_ms_new_names_to_add');

            $dest = $paramFetcher->get('map_name_to_add');
            //Ouvrir mapfile origine MO
            $mapObj_to_write = $this->getMapObj($directory, $mapfile_to_write);

            //Cas - Remplacer
            if ($mode == "Replace") {
                for ($i = 0; $i < $mapObj_to_write->numlayers; $i++) {
                    $oLayer = $mapObj_to_write->getLayer($i);
                    $oLayer->set('status', MS_DELETE);
                }

            }
            /*$this->saveMap($mapObj_o, $source, "When replacing layers");
            $mapObj_o = $this->getMapObj($directory, $source);*/

            //Cas - Ajouter et Remplacer

            //Ouvrir mapfile à copier MC
            $mapObj_from_copy = $this->getMapObj($directory, $dest, true, false);

            //Trouver les layers de MO à partir des noms de layers passés en param
            $hasExtraSymbol = false;
            $numsymbolsTo = $mapObj_to_write->getNumSymbols();
            $symbols = array();
            foreach ($layers_ms_new_names_to_add as $old_name => $new_name) {
                // instancie un LayerObj correspondant
                $layerObj_to_add = $this->getLayerObj($mapObj_from_copy, $old_name);
                //change name
                if ($layerObj_to_add != NULL) {

                    for ($i = 0; $i < $layerObj_to_add->numclasses; $i++) {
                        $class = $layerObj_to_add->getClass($i);
                        for ($j = 0; $j < $class->numstyles; $j++) {
                            $style = $class->getStyle($j);
                            if ($style->symbolname && $mapObj_to_write->getSymbolByName($style->symbolname) >= $numsymbolsTo) {
                                $hasExtraSymbol = $hasExtraSymbol || ($style->symbolname != self::HATCHSYMBOL);
                                $symbols[$style->symbolname] = $style->symbolname;
                            }
                        }
                    }
                }
            }

            $symbolset = $mapObj_to_write->symbolsetfilename;
            if (!file_exists($symbolset) && file_exists($mapObj_to_write->mappath . $symbolset)) {
                $symbolset = $mapObj_to_write->mappath . $symbolset;
            }
            if ($hasExtraSymbol) {

                $new_symbolset = dirname($symbolset) . "/symbols-" . $mapname . ".sym";
                if (realpath($new_symbolset) != realpath($symbolset)) {
                    copy($symbolset, $new_symbolset);
                    $this->setSymbolset($mapObj_to_write, $new_symbolset);
                }
                $symbolset = $new_symbolset;

            }

            foreach ($layers_ms_new_names_to_add as $old_name => $new_name) {
                // instancie un LayerObj correspondant
                $layerObj_to_add = $this->getLayerObj($mapObj_from_copy, $old_name);
                //change name
                if ($layerObj_to_add != NULL) {
                    $layerObj_to_add->set("name", "tmp_" . $new_name);
                    $nLayerObj = ms_newLayerObj($mapObj_to_write);
                    $nLayerObj->updateFromString($layerObj_to_add->convertToString());
                    $layerObj_to_add->set("name", $old_name);
                }
            }

            foreach ($layers_ms_new_names_to_add as $old_name => $new_name) {
                $layerObj_added = $this->getLayerObj($mapObj_to_write, "tmp_" . $new_name);
                if ($layerObj_added) {
                    $layerObj_added->set("name", $new_name);
                }
            }

            //Updating and saving the new map object
            if ($mapObj_to_write = $this->saveMap($mapObj_to_write, $mapfile_to_write, "When adding a layer", false, true)) {
                if (!empty($symbols)) {
                    $oMap = ms_newMapObj($mapfile_to_write);
                    for ($i = 0; $i < $oMap->numlayers; $i++) {
                        $oMap->getLayer($i)->set("status", MS_DELETE);
                    }
                    $nbInTemp = 0;
                    foreach ($symbols as $symbolname) {
                        $from_symbol = $mapObj_from_copy->getSymbolByName($symbolname);
                        $from_symbol = $mapObj_from_copy->getSymbolObjectById($from_symbol);

                        $in_map = ($symbolname == self::HATCHSYMBOL ? $mapObj_to_write : $oMap);

                        $to_symbol_idx = ms_newSymbolObj($in_map, $symbolname);
                        $to_symbol = $in_map->getSymbolObjectById($to_symbol_idx);
                        if ($symbolname == self::HATCHSYMBOL) {
                            $to_symbol->set('inmapfile', MS_TRUE);
                            $to_symbol->set('type', MS_SYMBOL_HATCH);
                            continue;
                        }

                        $nbInTemp++;

                        $fields = array("type", "character", "filled", "font", "sizex", "sizey", "transparent", "transparentcolor");
                        foreach ($fields as $field) {
                            $to_symbol->set($field, $from_symbol->$field);
                        }
                        $to_symbol->set("inmapfile", MS_TRUE);
                        $to_symbol->setImagePath($from_symbol->imagepath);
                        $from_symbol->numpoints > 0 && $to_symbol->setPoints($from_symbol->getPointsArray());
                    }
                    if ($nbInTemp) {
                        $tmpfile = str_replace(".map", ".tmp.map", $mapfile_to_write);
                        $tmpfile2 = str_replace(".tmp", ".tmp2", $tmpfile);
                        $oMap->save($tmpfile);

                        $contents = file_get_contents($tmpfile);
                        $contents = preg_replace('/.+(SYMBOL\s.+\sEND(?! #)).+/is', "$1", $contents);
                        file_put_contents($tmpfile, $contents);
                        file_put_contents($tmpfile2, "SYMBOLSET\n" . $contents . "\nEND");
                        try {
                            $this->setSymbolSet($oMap, $tmpfile2);
                            file_put_contents($symbolset, preg_replace("!^(.+)(\sEND\s*)$!is", "$1" . $contents . "$2", file_get_contents($symbolset)));
                            @unlink($tmpfile);
                            @unlink($tmpfile2);
                        } catch (\Exception $exception) {
                        }
                    }
                }
            }

            //Updating and saving the new map object
            if ($this->saveMap($mapObj_to_write, $mapfile_to_write, "When adding a layer")) {

                return $this->formatJsonSuccess(array("symbolset" => str_replace($mapObj_to_write->mappath, "", $symbolset)));
            }

        } catch (\Exception $ex) {
            //$this->get('logger')->error($ex->getTraceAsString());
            return $this->formatJsonError(Response::HTTP_INTERNAL_SERVER_ERROR, 'Internal Server Error', $ex->getMessage());
        }
    }

}
