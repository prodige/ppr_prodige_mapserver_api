FROM docker.alkante.com/alk-apache:1-php7.3
ENV APP_TARGET sf4

# Install php7.3-pgsql
RUN apt-get update ;\
    apt-get install -y --no-install-recommends php7.3-pgsql postgresql-client

# PHP module
RUN apt-get update ;\
    apt-get install -y --no-install-recommends php7.3-yaml

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer ;\
    chmod 777 /var/www

# Configure fcgid apache module
RUN apt-get install -y --no-install-recommends libapache2-mod-fcgid
COPY --chown=root:root fcgid.conf /etc/apache2/mods-available/fcgid.conf
RUN a2enmod fcgid

# Install gdal
RUN cd /tmp && \
  wget -nv https://ftp.alkante.com/gitlab/alkante/prodige/buster/libecwj2_3.3-1buster_amd64.deb && \
  wget -nv https://ftp.alkante.com/gitlab/alkante/prodige/buster/libgdal20_2.4.0+dfsg-1_amd64.deb && \
  wget -nv https://ftp.alkante.com/gitlab/alkante/prodige/buster/gdal-bin_2.4.0+dfsg-1_amd64.deb && \
  wget -nv https://ftp.alkante.com/gitlab/alkante/prodige/buster/gdal-data_2.4.0+dfsg-1_all.deb && \
  apt-get update && \
  apt install -y --no-install-recommends ./gdal-bin_2.4.0+dfsg-1_amd64.deb ./gdal-data_2.4.0+dfsg-1_all.deb ./libecwj2_3.3-1buster_amd64.deb ./libgdal20_2.4.0+dfsg-1_amd64.deb && \
  apt-mark hold gdal-data gdal-bin libecwj2 libgdal20 &&\
  rm -f ./gdal-bin_2.4.0+dfsg-1_amd64.deb ./gdal-data_2.4.0+dfsg-1_all.deb ./libecwj2_3.3-1buster_amd64.deb ./libgdal20_2.4.0+dfsg-1_amd64.deb ;

# Install mapserv
RUN echo 'deb http://archive.debian.org/debian buster-backports main contrib non-free' > /etc/apt/sources.list.d/debian_backports.list &&\
    echo 'Package: *\n\
Pin: release o=Debian Backports,a=buster-backports,n=buster-backports,l=Debian Backports \n\
Pin-Priority: 400' > /etc/apt/preferences.d/debian_backports.pref &&\
    apt-get update &&\
    apt install -y --no-install-recommends -t buster-backports mapserver-bin php-mapscript &&\
    apt-mark hold mapserver-bin php-mapscript

RUN cp /usr/bin/mapserv /usr/lib/cgi-bin/mapserv; \
  cp /usr/bin/mapserv /usr/lib/cgi-bin/mapservwfs; \
  cp /usr/bin/mapserv /usr/lib/cgi-bin/mapserv.fcgi;

COPY --chown=root:root epsg /usr/share/proj/epsg

RUN chmod o+r /usr/share/proj/epsg

RUN a2enmod cgi; \
  a2enmod cgid
