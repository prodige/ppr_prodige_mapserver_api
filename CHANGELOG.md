# Changelog

All notable changes to [ppr_prodige_mapserver_api](https://gitlab.adullact.net/prodige/ppr_prodige_mapserver_api) project will be documented in this file.

## [4.4.3](https://gitlab.adullact.net/prodige/ppr_prodige_mapserver_api/compare/4.4.2...4.4.3) - 2024-07-02

### Fixed

- wfs adding layer in map composer

## [4.4.2](https://gitlab.adullact.net/prodige/ppr_prodige_mapserver_api/compare/4.4.1...4.4.2) - 2023-03-30

## [4.4.1](https://gitlab.adullact.net/prodige/ppr_prodige_mapserver_api/compare/4.4.0-rc8...4.4.1) - 2023-03-17

