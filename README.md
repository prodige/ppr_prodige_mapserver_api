# Module PRODIGE API pour Mapserver

Module développé en PHP/Symfony, basé sur l'extension PHP/Mapscript et servant d'API de lecture/écriture de mapfiles.
Module non ouvert sur le web, servant à des appels inter modules.

### configuration

Modifier le fichier site/app/config/global_parameters.yml

### Installation

[documentation d'insatllation en développement](cicd/dev/README.md).

## Accès à l'application

- pas d'accès direct
